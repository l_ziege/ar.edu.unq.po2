package tp11.ej3;

import java.util.List;
import java.util.Map;

public interface WikipediaPage {

	/*Retorna el t�tulo de la p�gina.*/
	public String getTitle();
	
	/*Retorna una Lista de las p�ginas de Wikipedia con las que se conecta.*/
	public List<WikipediaPage> getLinks();
	
	/*
	 * Retorna un Map con un valor en texto y la pagina que describe ese valor que aparecen en los infobox 
	 * de la p�gina de Wikipedia.
	 */
	public Map<String, WikipediaPage> getInfobox();
	
}
