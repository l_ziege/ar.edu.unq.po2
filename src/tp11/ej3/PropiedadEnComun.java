package tp11.ej3;

// PATRON TEMPLATE METHOD
// ROL: ConcreteClassC

public class PropiedadEnComun extends FiltroWikipediaPage {

	// OPERACIÓN PRIMITIVA
	@Override
	protected boolean sonPaginasSimilares(WikipediaPage primeraPagina, WikipediaPage segundaPagina) {
		return this.tienenAlgunaPropiedadDelInfoboxEnComun(primeraPagina, segundaPagina);
	}

	private boolean tienenAlgunaPropiedadDelInfoboxEnComun(WikipediaPage primeraPagina, WikipediaPage segundaPagina) {
		return primeraPagina.getInfobox().keySet().stream().anyMatch(propiedad -> segundaPagina.getInfobox().containsKey(propiedad));
	}

}
