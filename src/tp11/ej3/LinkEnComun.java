package tp11.ej3;

// PATRON TEMPLATE METHOD
// ROL: ConcreteClassB

public class LinkEnComun extends FiltroWikipediaPage {

	// OPERACIÓN PRIMITIVA
	@Override
	protected boolean sonPaginasSimilares(WikipediaPage primeraPagina, WikipediaPage segundaPagina) {
		return this.tienenAlgunLinkEnComun(primeraPagina, segundaPagina);
	}

	private boolean tienenAlgunLinkEnComun(WikipediaPage primeraPagina, WikipediaPage segundaPagina) {
		return primeraPagina.getLinks().stream().anyMatch(link -> segundaPagina.getLinks().contains(link));
	}

}
