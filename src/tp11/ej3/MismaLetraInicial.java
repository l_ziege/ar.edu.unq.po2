package tp11.ej3;

// PATRON TEMPLATE METHOD
// ROL: ConcreteClassA

public class MismaLetraInicial extends FiltroWikipediaPage {

	// OPERACIÓN PRIMITIVA
	@Override
	protected boolean sonPaginasSimilares(WikipediaPage primeraPagina, WikipediaPage segundaPagina) {
		return this.primeraLetraDelTituloDeLaPagina(primeraPagina) == this.primeraLetraDelTituloDeLaPagina(segundaPagina);
	}

	private char primeraLetraDelTituloDeLaPagina(WikipediaPage page) {
		return page.getTitle().charAt(0);
	}

}
