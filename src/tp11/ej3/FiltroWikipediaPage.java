package tp11.ej3;

import java.util.List;
import java.util.stream.Collectors;

// PATRON TEMPLATE METHOD
// ROL: AbstractClass

public abstract class FiltroWikipediaPage {

	// TEMPLATE METHOD
	public final List<WikipediaPage> getSimilarPages(WikipediaPage page, List<WikipediaPage> wikipedia) {
		List<WikipediaPage> paginasSimilares = wikipedia.stream().filter(pagina -> this.sonPaginasSimilares(page, pagina)).collect(Collectors.toList());
		return paginasSimilares;
	}

	// OPERACIÓN PRIMITIVA
	protected abstract boolean sonPaginasSimilares(WikipediaPage primeraPagina, WikipediaPage segundaPagina);
	
}
