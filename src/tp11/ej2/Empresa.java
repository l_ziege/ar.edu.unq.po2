package tp11.ej2;

import java.util.ArrayList;
import java.util.List;

public class Empresa {

	private List<Empleado> empleados;
	
	public Empresa() {
		super();
		this.empleados = new ArrayList<Empleado>();
	}
	
	public void cargarEmpleado(Empleado nuevoEmpleado) {
		this.empleados.add(nuevoEmpleado);
	}
	
	public void pagarSueldos() {
		this.empleados.stream().forEach(empleado -> empleado.recibirPagoDeSueldo(this.sueldoDelEmpleadoConDescuentoDeAportesYObraSocial(empleado)));
	}

	private double sueldoDelEmpleadoConDescuentoDeAportesYObraSocial(Empleado empleado) {
		return (empleado.sueldo() - (empleado.sueldo() * 13 / 100));
	}
}
