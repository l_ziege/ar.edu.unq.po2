package tp11.ej2;

// PATRON TEMPLATE METHOD
// ROL: ConcreteClassB

public class EmpleadoPasante extends Empleado {

	public EmpleadoPasante(int horasDeTrabajoPorMes, int hijos, boolean estaCasado) {
		super();
		this.dinero = 0;
		this.cantHorasTrabajadasPorMes = horasDeTrabajoPorMes;
		this.cantHijos = hijos;
		this.estaCasado = estaCasado;
	}
	
	// OPERACIÓN PRIMITIVA
	@Override
	protected double sueldoBásico() {
		// No tiene un sueldo básico
		return 0;
	}

	// OPERACIÓN PRIMITIVA
	@Override
	protected double pagoPorHorasTrabajadasEnElMes() {
		return 40 * this.cantHorasTrabajadasPorMes;
	}
	
	// OPERACIÓN PRIMITIVA
	@Override
	protected double pagoPorHijosOEstarCasado() {
		// No recibe un plus por hijos o pareja
		return 0;
	}

}
