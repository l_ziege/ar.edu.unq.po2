package tp11.ej2;

// PATRON TEMPLATE METHOD
// ROL: AbstractClass

public abstract class Empleado {
	
	protected double dinero;
	protected int cantHorasTrabajadasPorMes;
	protected int cantHijos;
	protected boolean estaCasado;
	
	// TEMPLATE METHOD
	public final double sueldo() {
		return (this.sueldoBásico() + this.pagoPorHorasTrabajadasEnElMes() + this.pagoPorHijosOEstarCasado());
	}

	// OPERACIÓN PRIMITIVA
	protected abstract double sueldoBásico();

	// OPERACIÓN PRIMITIVA
	protected abstract double pagoPorHorasTrabajadasEnElMes();

	// OPERACIÓN PRIMITIVA
	protected abstract double pagoPorHijosOEstarCasado();

	public void recibirPagoDeSueldo(double sueldo) {
		this.dinero += sueldo;
	}
	
	public double getDinero() {
		return this.dinero;
	}
	
}
