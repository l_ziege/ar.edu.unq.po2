package tp11.ej2;

// PATRON TEMPLATE METHOD
// ROL: ConcreteClassC

public class EmpleadoDePlanta extends Empleado {
	
	public EmpleadoDePlanta(int horasDeTrabajoPorMes, int hijos, boolean estaCasado) {
		super();
		this.dinero = 0;
		this.cantHorasTrabajadasPorMes = horasDeTrabajoPorMes;
		this.cantHijos = hijos;
		this.estaCasado = estaCasado;
	}

	// OPERACIÓN PRIMITIVA
	@Override
	protected double sueldoBásico() {
		return 3000;
	}

	// OPERACIÓN PRIMITIVA
	@Override
	protected double pagoPorHorasTrabajadasEnElMes() {
		// No recibe un pago extra por horas trabajadas
		return 0;
	}

	// OPERACIÓN PRIMITIVA
	@Override
	protected double pagoPorHijosOEstarCasado() {
		return this.cantHijos * 150;
	}

	

}
