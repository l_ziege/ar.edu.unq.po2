package tp11.ej2;

// PATRON TEMPLATE METHOD
// ROL: ConcreteClassA

public class EmpleadoTemporario extends Empleado {
	
	public EmpleadoTemporario(int horasDeTrabajoPorMes, int hijos, boolean estaCasado) {
		super();
		this.dinero = 0;
		this.cantHorasTrabajadasPorMes = horasDeTrabajoPorMes;
		this.cantHijos = hijos;
		this.estaCasado = estaCasado;
	}

	// OPERACIÓN PRIMITIVA
	@Override
	protected double sueldoBásico() {
		return 1000;
	}

	// OPERACIÓN PRIMITIVA
	@Override
	protected double pagoPorHorasTrabajadasEnElMes() {
		return 5 * this.cantHorasTrabajadasPorMes;
	}
	
	// OPERACIÓN PRIMITIVA
	@Override
	protected double pagoPorHijosOEstarCasado() {
		if (this.tieneHijos() || this.estaCasado) {
			return 100;
		} else {
			return 0;
		}
	}

	private boolean tieneHijos() {
		return this.cantHijos > 0;
	}

}
