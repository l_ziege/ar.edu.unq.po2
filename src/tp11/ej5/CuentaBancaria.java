package tp11.ej5;

import java.util.ArrayList;
import java.util.List;

// PATRON TEMPLATE METHOD
// ROL: AbstractClass

public abstract class CuentaBancaria {
	
	private String titular;
	private int saldo;
	private List<String> movimientos;
	
	public CuentaBancaria(String titular) {
		this.titular=titular;
		this.saldo=0;
		this.movimientos=new ArrayList<String>();
	}
	
	public String getTitular() {
		return this.titular;
	}
	
	public int getSaldo() {
		return this.saldo;
	}
	
	// OPERACIÓN CONCRETA
	private void setSaldo(int monto) {
		this.saldo=monto;
	}
	
	// OPERACIÓN CONCRETA
	private void agregarMovimiento(String movimiento) {
		this.movimientos.add(movimiento);
	}
	
	// TEMPLATE METHOD
	public final void extraer(int monto) {
		if (this.esUnaExtracciónValida(monto)) {
			this.setSaldo(this.getSaldo()-monto);
			this.agregarMovimiento("Extraccion");
		}
	}

	// OPERACIÓN PRIMITIVA
	protected abstract boolean esUnaExtracciónValida(int monto);
}
