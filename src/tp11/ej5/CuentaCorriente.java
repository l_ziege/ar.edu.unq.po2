package tp11.ej5;

// PATRON TEMPLATE METHOD
// ROL: ConcreteClassA

public class CuentaCorriente extends CuentaBancaria {

	private int descubierto;
	
	public CuentaCorriente(String titular, int descubierto) {
		super(titular);
		this.descubierto=descubierto;
	}
	
	public int getDescubierto() {
		return this.descubierto;
	}

	// OPERACIÓN PRIMITIVA
	@Override
	protected boolean esUnaExtracciónValida(int monto) {
		return this.getSaldo()+this.getDescubierto()>=monto;
	}

}
