package tp11.ej5;

// PATRON TEMPLATE METHOD
// ROL: ConcreteClassB

public class CajaDeAhorro extends CuentaBancaria {

	private int limite;
	
	public CajaDeAhorro(String titular, int limite) {
		super(titular);
		this.limite=limite;
	}
	
	public int getLimite() {
		return this.limite;
	}

	// OPERACIÓN PRIMITIVA
	@Override
	protected boolean esUnaExtracciónValida(int monto) {
		return this.getSaldo()>=monto && this.getLimite()>=monto;
	}

}
