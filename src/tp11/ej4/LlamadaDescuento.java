package tp11.ej4;

// PATRON TEMPLATE METHOD
// ROL: ConcreteClassA

public class LlamadaDescuento extends LlamadaTelefonica {

	public LlamadaDescuento(int tiempo, int horaDelDia) {
		super(tiempo, horaDelDia);
	}
	
	// OPERACIÓN PRIMITIVA
	@Override
	public boolean esHoraPico() {
		return false;
	}
	
	@Override
	public float costoNeto() {
		return this.getTiempo()*0.95f;
	}

}
