package tp11.ej4;

// PATRON TEMPLATE METHOD
// ROL: AbstractClass

public abstract class LlamadaTelefonica {
	
	private int tiempo;
	private int horaDelDia;
	
	public LlamadaTelefonica(int tiempo, int horaDelDia) {
		this.tiempo=tiempo;
		this.horaDelDia=horaDelDia;
	}
	
	// OPERACIÓN CONCRETA
	public int getTiempo() {
		return this.tiempo;
	}
	
	public int getHoraDelDia() {
		return this.horaDelDia;
	}
	
	// OPERACIÓN PRIMITIVA
	public abstract boolean esHoraPico();
	
	// TEMPLATE METHOD
	public final float costoFinal() {
		if (this.esHoraPico()) {
			return this.costoNeto()*1.2f*this.getTiempo();
		} else {
			return this.costoNeto()*this.getTiempo();
		}
	}
	
	// OPERACIÓN CONCRETA
	public float costoNeto() {
		return this.getTiempo()*1;
	}
	
}
