package tp9.ej5;

public class Product {

	private Float price;
	private String name;
	
	public Product(Float price, String name) {
		super();
		this.price = price;
		this.name = name;
	}

	public Float getPrice() {
		return this.price;
	}
	
	public String getName() {
		return this.name;
	}
}
