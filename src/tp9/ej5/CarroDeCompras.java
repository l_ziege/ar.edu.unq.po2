package tp9.ej5;

import java.util.List;

public class CarroDeCompras {

	private List<Product> productos;

	public CarroDeCompras(List<Product> productos) {
		super();
		setElements(productos);
	}

	private void setElements(List<Product> productos) {
		this.productos = productos;
	}
	
	public List<Product> getElements() {
		return this.productos;
	}
	
	public int totalRounded() {
		int totalRedondeado = this.productos.stream().mapToInt(p->p.getPrice().intValue()).sum();
		return totalRedondeado;
	}
	
	public float total() {
		float total = 0;
		for (Product p: this.productos) {
			total += p.getPrice();
		}
		return total;
	}
}
