package tp9.ej3;

// PATRON COMPOSITE
// ROL: Component

public abstract class Caracter {

	/*
	 * El punto debe estar dentro del mapa y los pasos son cortos.
	 */
	public abstract void caminarPorElMapa(Punto puntoAIr, Mapa mapa);
	
	protected abstract void moverAPuntoDelMapa(Punto puntoAIr);
	
	public void agregarCaracter(Caracter caracter) {
		// Metodo hook
	}
	
	public void eliminarCaracter(Caracter caracter) {
		// Metodo hook
	}
	
	public Caracter getCaracter(int posici�n) {
		return null;
	}
}
