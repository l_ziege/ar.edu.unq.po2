package tp9.ej3;

import java.util.ArrayList;
import java.util.List;

// PATRON COMPOSITE
// ROL: Composite

public class Ejercito extends Caracter {
	
	private List<Caracter> caracteres;

	public Ejercito() {
		super();
		this.caracteres = new ArrayList<Caracter>();
	}
	
	@Override
	public void caminarPorElMapa(Punto puntoAIr, Mapa mapa) {
		this.caracteres.stream().forEach(caracter -> caracter.caminarPorElMapa(puntoAIr, mapa));
	}
	
	@Override
	protected void moverAPuntoDelMapa(Punto puntoAIr) {
		// metodo hook, no hace nada (cada tipo de caracter se encarga de mover al punto a su forma).
	}
	
	@Override
	public void agregarCaracter(Caracter caracter) {
		this.caracteres.add(caracter);
	}
	
	@Override
	public void eliminarCaracter(Caracter caracter) {
		this.caracteres.remove(caracter);
	}
	
	@Override
	public Caracter getCaracter(int posici�n) {
		return this.caracteres.get(posici�n);
	}

}
