package tp9.ej3;

// PATRON COMPOSITE
// ROL: Leaf

public class Ingeniero extends Caracter {
	
	private Punto posici�n;
	private int cantLajas;

	public Ingeniero(int cantLajas) {
		super();
		this.posici�n = new Punto();
		this.cantLajas = cantLajas;
	}

	@Override
	public void caminarPorElMapa(Punto puntoAIr, Mapa mapa) {
		this.moverAPuntoDelMapa(mapa.getPunto(puntoAIr));
	}

	@Override
	protected void moverAPuntoDelMapa(Punto puntoAIr) {
		this.posici�n = puntoAIr;
		this.ponerLajaSiTiene();
	}

	private void ponerLajaSiTiene() {
		if (this.tieneLajas()) {
			this.ponerLajaSiPuede();
		}
	}

	private void ponerLajaSiPuede() {
		if (! this.posici�n.tieneLaja()) {
			this.ponerLaja();
		}
	}

	private void ponerLaja() {
		this.posici�n.ponerLaja();
		this.cantLajas -= 1;
	}

	private boolean tieneLajas() {
		return this.cantLajas > 0;
	}

}
