package tp9.ej3;

public class Punto {

	private int x;
	private int y;
	private boolean tieneLaja;
	
	public Punto(int posX, int posY) {
		super();
		this.x = posX;
		this.y = posY;
		this.tieneLaja = false;
	}
	
	public Punto() {
		super();
		this.x = 0;
		this.y = 0;
		this.tieneLaja = false;
	}
	
	public void moverAPunto(int posX, int posY) {
		this.x = posX;
		this.y = posY;
	}

	public boolean esMismaPosición(Punto puntoAIr) {
		return ((this.x == puntoAIr.getPosX())
				&&
				(this.y == puntoAIr.getPosY()));
	}
	
	public int getPosX() {
		return this.x;
	}
	
	public int getPosY() {
		return this.y;
	}
	
	public boolean tieneLaja() {
		return this.tieneLaja;
	}

	public void ponerLaja() {
		this.tieneLaja = true;
	}
}