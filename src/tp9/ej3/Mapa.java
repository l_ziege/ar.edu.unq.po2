package tp9.ej3;

import java.util.Arrays;
import java.util.List;

/**
 * El mapa actualmente esta compuesto por 25 cuadrados en un mapa de 2 dimensiones, a futuro se podria hacer
 * mapas mas extensos incluso de 3 dimensiones.
 */

public class Mapa {

	private List<Punto> puntosEnElMapa;

	public Mapa() {
		super();
		this.puntosEnElMapa = this.puntosDeUnMapa5x5();
	}

	private List<Punto> puntosDeUnMapa5x5() {
		return Arrays.asList(new Punto(0,0), new Punto(1,0), new Punto(2,0), new Punto(3,0), new Punto(4,0),
							 new Punto(0,1), new Punto(1,1), new Punto(2,1), new Punto(3,1), new Punto(4,1),
							 new Punto(0,2), new Punto(1,2), new Punto(2,2), new Punto(3,2), new Punto(4,2),
							 new Punto(0,3), new Punto(1,3), new Punto(2,3), new Punto(3,3), new Punto(4,3),
							 new Punto(0,4), new Punto(1,4), new Punto(2,4), new Punto(3,4), new Punto(4,4));
	}

	public Punto getPunto(Punto puntoAIr) {
		int posActual = 0;
		Punto puntoActual = this.puntosEnElMapa.get(posActual);
		while (! puntoActual.esMismaPosición(puntoAIr)) {
			posActual += 1;
			puntoActual = this.puntosEnElMapa.get(posActual);
		}
		return puntoActual;
	}
	
}
