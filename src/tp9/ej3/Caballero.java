package tp9.ej3;

// PATRON COMPOSITE
// ROL: Leaf

public class Caballero extends Caracter {
	
	private Punto posici�n;
	
	public Caballero() {
		super();
		this.posici�n = new Punto();
	}

	@Override
	public void caminarPorElMapa(Punto puntoAIr, Mapa mapa) {
		this.moverAPuntoDelMapa(mapa.getPunto(puntoAIr));
	}

	@Override
	protected void moverAPuntoDelMapa(Punto puntoAIr) {
		this.posici�n = puntoAIr;
		this.vigilar();
	}

	private void vigilar() {
		// Vigila la zona mientras se mueve de un punto a otro.
	}

}
