package tp9.ej6;

import java.util.Arrays;
import java.util.List;

// PATRON COMPOSITE
// ROL: Leaf

public class ShapeShifterHoja extends ElementoShapeShifter {
	
	private int valor;

	public ShapeShifterHoja(int valor) {
		super();
		this.valor = valor;
	}

	@Override
	public IShapeShifter compose(IShapeShifter iss) {
		ShapeShifter compuesto = new ShapeShifter(Arrays.asList(this, iss));
		return compuesto;
	}

	@Override
	public int deepest() {
		return 0;
	}

	@Override
	public IShapeShifter flat() {
		return this;
	}

	@Override
	public List<Integer> values() {
		return Arrays.asList(this.valor);
	}
	
	@Override
	public String toString() {
		return ("" + this.valor);
	}

}
