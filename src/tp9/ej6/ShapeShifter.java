package tp9.ej6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// PATRON COMPOSITE
// ROL: Composite

public class ShapeShifter extends ElementoShapeShifter {

	private List<IShapeShifter> valores;
	
	public ShapeShifter(List<IShapeShifter> valores) {
		super();
		this.valores = valores;
	}

	@Override
	public IShapeShifter compose(IShapeShifter iss) {
		ShapeShifter compuesto = new ShapeShifter(Arrays.asList(this, iss));
		return compuesto;
	}

	@Override
	public int deepest() {
		int profundidadMáxima = 0;
		for (int i = 0; i < this.valores.size()-1; i++) {
			profundidadMáxima = this.profundidadMáximaEntre(this.valores.get(i).deepest(), this.valores.get(i+1).deepest());
		}
		return profundidadMáxima + 1;
	}

	private int profundidadMáximaEntre(int profundidadActual, int profundidadAComparar) {
		if (profundidadActual > profundidadAComparar) {
			return profundidadActual;
		} else {
			return profundidadAComparar;
		}
	}

	@Override
	public IShapeShifter flat() {
		// lista de values()
		List<Integer> valores = this.values();
		List<IShapeShifter> hojasDeShapeShifter = new ArrayList<IShapeShifter>();
		for (Integer valor: valores) {
			hojasDeShapeShifter.add(new ShapeShifterHoja(valor));
		}
		return new ShapeShifter(hojasDeShapeShifter);
	}

	@Override
	public List<Integer> values() {
		List<Integer> valores = new ArrayList<Integer>();
		for (IShapeShifter iss: this.valores) {
			valores.addAll(iss.values());
		}
		return valores;
	}
	
	@Override
	public void agregarElementoSS(IShapeShifter ess) {
		this.valores.add(ess);
	}
	
	@Override
	public void eliminarElementoSS(IShapeShifter ess) {
		this.valores.remove(ess);
	}
	
	@Override
	public IShapeShifter getElementoSS(int posición) {
		return this.valores.get(posición);
	}
	
	@Override
	public String toString() {
		String valores = "[ ";
		for (IShapeShifter iss: this.valores) {
			valores += iss.toString() + " ";
		}
		return (valores + "]");
	}

}
