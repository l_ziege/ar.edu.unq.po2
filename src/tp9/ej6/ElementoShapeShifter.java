package tp9.ej6;

import java.util.List;

// PATRON COMPOSITE
// ROL: Component

public abstract class ElementoShapeShifter implements IShapeShifter {

	public abstract IShapeShifter compose(IShapeShifter iss);

	public abstract int deepest();

	public abstract IShapeShifter flat();

	public abstract List<Integer> values();
	
	public void agregarElementoSS(IShapeShifter ess) {
		// Metodo hook
	}
	
	public void eliminarElementoSS(IShapeShifter ess) {
		// Metodo hook
	}
	
	public IShapeShifter getElementoSS(int posici�n) {
		return null;
	}

}
