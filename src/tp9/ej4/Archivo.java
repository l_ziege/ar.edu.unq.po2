package tp9.ej4;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// PATRON COMPOSITE
// ROL: Leaf

public class Archivo extends ElementoFileSystem {
	
	private int size;
	private Date fechaUltMod;

	public Archivo(String nombre, int tama�o, Date fechaMod) {
		super();
		this.nombre = nombre;
		this.size = tama�o;
		this.fechaUltMod = fechaMod;
	}
	
	@Override
	public int totalSize() {
		return this.size;
	}

	@Override
	public Date getFecha() {
		return fechaUltMod;
	}
	
	@Override
	public void printStructure() {
		System.out.println(this.nombre);
	}

	@Override
	protected List<ElementoFileSystem> getContenido() {
		return new ArrayList<ElementoFileSystem>();
	}

	@Override
	public ElementoFileSystem lastModified() {
		return this;
	}

	@Override
	public ElementoFileSystem oldestElement() {
		return this;
	}

}
