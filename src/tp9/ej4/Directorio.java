package tp9.ej4;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// PATRON COMPOSITE
// ROL: Composite

public class Directorio extends ElementoFileSystem {
	
	private List<ElementoFileSystem> contenido;
	private Date fechaCreación;

	public Directorio(String nombre, List<ElementoFileSystem> contenido, Date fechaCreación) {
		super();
		this.nombre = nombre;
		this.contenido = contenido;
		this.fechaCreación = fechaCreación;
	}
	
	public Directorio(String nombre, Date fechaCreación) {
		super();
		this.nombre = nombre;
		this.contenido = new ArrayList<ElementoFileSystem>();
		this.fechaCreación = fechaCreación;
	}
	
	@Override
	public void agregarElementoFS(ElementoFileSystem efs) {
		this.contenido.add(efs);
	}
	
	@Override
	public void eliminarElementoFS(ElementoFileSystem efs) {
		this.contenido.remove(efs);
	}
	
	@Override
	public ElementoFileSystem getElementoFS(int posición) {
		return this.contenido.get(posición);
	}

	@Override
	public void printStructure() {
		System.out.println(this.nombre);
		for (ElementoFileSystem efs : this.contenido) {
			this.imprimirEstructuraConCantidadDeEspacios(efs, 1);
		}
	}

	private void imprimirEstructuraConCantidadDeEspacios(ElementoFileSystem efs, int cantEspacios) {
		this.imprimirEspacios(cantEspacios);
		System.out.println(efs.nombre);
		for (ElementoFileSystem efsActual : efs.getContenido()) {
			this.imprimirEstructuraConCantidadDeEspacios(efsActual, cantEspacios + 1);
		}
	}

	private void imprimirEspacios(int cantEspacios) {
		for (int i=0; i < cantEspacios; i++) {
			System.out.print(" ");
		}
	}

	@Override
	public ElementoFileSystem lastModified() {
		ElementoFileSystem efsMasNuevo = this;
		for (ElementoFileSystem efs: contenido) {
			efsMasNuevo = efsMasNuevoEntre(efsMasNuevo, efs);
		}
		return efsMasNuevo;
	}

	private ElementoFileSystem efsMasNuevoEntre(ElementoFileSystem efs1, ElementoFileSystem efs2) {
		if (efs1.getFecha().after(efs2.getFecha())) {
			return efs1;
		} else {
			return efs2;
		}
	}

	@Override
	public ElementoFileSystem oldestElement() {
		ElementoFileSystem efsMasAntiguo = this;
		for (ElementoFileSystem efs: contenido) {
			efsMasAntiguo = efsMasAntiguoEntre(efsMasAntiguo, efs);
		}
		return efsMasAntiguo;
	}

	private ElementoFileSystem efsMasAntiguoEntre(ElementoFileSystem efs1, ElementoFileSystem efs2) {
		if (efs1.getFecha().before(efs2.getFecha())) {
			return efs1;
		} else {
			return efs2;
		}
	}

	@Override
	public int totalSize() {
		int tamañoTotal = this.contenido.stream().mapToInt(efs->efs.totalSize()).sum();
		return tamañoTotal;
	}
	
	@Override
	public Date getFecha() {
		return fechaCreación;
	}

	@Override
	protected List<ElementoFileSystem> getContenido() {
		return this.contenido;
	}

}
