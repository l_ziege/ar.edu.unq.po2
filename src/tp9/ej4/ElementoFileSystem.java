package tp9.ej4;

import java.util.Date;
import java.util.List;

// PATRON COMPOSITE
// ROL: Component

public abstract class ElementoFileSystem implements FileSystem {

	protected String nombre;
	
	public String getNombre() {
		return this.nombre;
	}
	
	public abstract int totalSize();
	
	public abstract void printStructure();
	
	public abstract Date getFecha();
	
	public void agregarElementoFS(ElementoFileSystem efs) {
		// Metodo hook
	}
	
	public void eliminarElementoFS(ElementoFileSystem efs) {
		// Metodo hook
	}
	
	public ElementoFileSystem getElementoFS(int posici�n) {
		return null;
	}

	protected abstract List<ElementoFileSystem> getContenido();

}
