package tp9.ej4;

public interface FileSystem {

	/**
	* Retorna el total ocupado en disco del receptor. Expresado en
	*cantidad de bytes.
	*/
	public int totalSize();
	
	/**
	* Imprime en consola el contenido indicando el nombre del elemento
	*e indentandolo con tantos espacios como profundidad en la
	estructura.
	 */
	public void printStructure();
	
	/**
	 * Elemento mas nuevo
	 */
	public ElementoFileSystem lastModified();
	
	/** Elemento mas antiguo
	 */
	public ElementoFileSystem oldestElement();
}
