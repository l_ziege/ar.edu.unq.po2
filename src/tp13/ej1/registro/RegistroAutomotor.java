package tp13.ej1.registro;

import java.time.LocalDate;

public class RegistroAutomotor {

	public Boolean debeRealizarVtv(Vehiculo vehiculo, LocalDate fecha) {
		return vehiculo.debeRealizarVtvALaFecha(fecha);
	}
	
}
