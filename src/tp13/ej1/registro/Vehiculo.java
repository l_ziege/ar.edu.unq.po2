package tp13.ej1.registro;

import java.time.LocalDate;

public class Vehiculo {
	
	private Boolean esVehiculoPolicial;
	private LocalDate fechaFabricacion;	
	private String ciudadRadicacion; 
	
	public Vehiculo(Boolean esVehiculoPolicial, LocalDate fechaFabricacion, String ciudadRadicacion) {
		this.esVehiculoPolicial = esVehiculoPolicial;
		this.fechaFabricacion = fechaFabricacion;
		this.ciudadRadicacion = ciudadRadicacion;
	}

	public Boolean debeRealizarVtvALaFecha(LocalDate fecha) {
		// vehiculos policiales no realizan vtv, ya que tienen otro tipo de control
		// sólo realizan vtv los vehículos con más de 1 año de antiguedad y radicados
		// en 'Buenos Aires'
		return (!this.esVehiculoPolicial && this.tieneMasDeUnAñoDeAntigüedadALaFecha(fecha)
				&& this.estáRadicadoEnBuenosAires());
	}

	private boolean tieneMasDeUnAñoDeAntigüedadALaFecha(LocalDate fecha) {
		return fecha.minusYears(1).isAfter(fechaFabricacion);
	}
	
	private boolean estáRadicadoEnBuenosAires() {
		return ciudadRadicacion.equalsIgnoreCase("Buenos Aires");
	}
	
}
