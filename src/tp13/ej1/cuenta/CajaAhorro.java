package tp13.ej1.cuenta;

public class CajaAhorro extends CuentaBancaria{

	public CajaAhorro(HistorialMovimientos historialDeMovimientos, Notificador notificador, Integer saldo) {
		super(historialDeMovimientos, notificador, saldo);
	}

	@Override
	protected boolean esExtracciónValida(Integer monto) {
		return this.saldo >= monto;
	}
	
}
