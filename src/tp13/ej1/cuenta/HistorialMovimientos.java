package tp13.ej1.cuenta;

public interface HistorialMovimientos {

	void registrarMovimiento(String descripcion, Integer monto);

}
