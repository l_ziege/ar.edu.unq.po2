package tp13.ej1.cuenta;

public interface Notificador {
	
	void notificarNuevoSaldoACliente(CuentaBancaria cuentaBancaria);

}
