package tp8.ej5;

import java.util.Arrays;
import java.util.List;

public class PokerStatus {

	public Jugada verificar(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		Jugada jugada;
		if (hayPoquer(carta1, carta2, carta3, carta4, carta5)) {
			jugada = this.jugadaDePoquer(carta1, carta2, carta3, carta4, carta5);
			return jugada;
		}
		if (hayColor(carta1, carta2, carta3, carta4, carta5)) {
			List<Carta> cartasDeColor = Arrays.asList(carta1, carta2, carta3, carta4, carta5);
			jugada = new Jugada(cartasDeColor, "Color");
			return jugada;
		}
		if (hayTrio(carta1, carta2, carta3, carta4, carta5)) {
			jugada = this.jugadaDeTrio(carta1, carta2, carta3, carta4, carta5);
			return jugada;
		}
		else {
			List<Carta> cartasDeNada = Arrays.asList(carta1, carta2, carta3, carta4, carta5);
			jugada = new Jugada(cartasDeNada, "Nada");
			return jugada;
		}
	}

	private boolean hayPoquer(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		return ((tienePoquer(carta1, carta2, carta3, carta4)) ||
				(tienePoquer(carta1, carta2, carta3, carta5)) ||
				(tienePoquer(carta1, carta2, carta4, carta5)) ||
				(tienePoquer(carta1, carta3, carta4, carta5)) ||
				(tienePoquer(carta2, carta3, carta4, carta5)));
	}

	private boolean tienePoquer(Carta carta1, Carta carta2, Carta carta3, Carta carta4) {
		int valorCarta1 = carta1.getValor();
		int valorCarta2 = carta2.getValor();
		int valorCarta3 = carta3.getValor();
		int valorCarta4 = carta4.getValor();
		return ((valorCarta1 == valorCarta2) &&
				(valorCarta2 == valorCarta3) &&
				(valorCarta3 == valorCarta4));
	}
	
	private Jugada jugadaDePoquer(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		Jugada jugadaDePoquer;
		List<Carta> cartasDePoquer;
		if (tienePoquer(carta1, carta2, carta3, carta4)) {
			cartasDePoquer = Arrays.asList(carta1, carta2, carta3, carta4);
			jugadaDePoquer = new Jugada(cartasDePoquer, "Poquer");
		}
		if (tienePoquer(carta1, carta2, carta3, carta5)) {
			cartasDePoquer = Arrays.asList(carta1, carta2, carta3, carta5);
			jugadaDePoquer = new Jugada(cartasDePoquer, "Poquer");
		}
		if (tienePoquer(carta1, carta2, carta4, carta5)) {
			cartasDePoquer = Arrays.asList(carta1, carta2, carta4, carta5);
			jugadaDePoquer = new Jugada(cartasDePoquer, "Poquer");
		}
		if (tienePoquer(carta1, carta3, carta4, carta5)) {
			cartasDePoquer = Arrays.asList(carta1, carta3, carta4, carta5);
			jugadaDePoquer = new Jugada(cartasDePoquer, "Poquer");
		}
		else {
			cartasDePoquer = Arrays.asList(carta2, carta3, carta4, carta5);
			jugadaDePoquer = new Jugada(cartasDePoquer, "Poquer");
		}
		return jugadaDePoquer;
	}

	private boolean hayColor(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		return ((carta1.esMismoPaloQue(carta2)) &&
				(carta2.esMismoPaloQue(carta3)) &&
				(carta3.esMismoPaloQue(carta4)) &&
				(carta4.esMismoPaloQue(carta5)));
	}

	private boolean hayTrio(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		return ((tieneTrio(carta1, carta2, carta3)) ||
				(tieneTrio(carta1, carta2, carta4)) ||
				(tieneTrio(carta1, carta2, carta5)) ||
				(tieneTrio(carta1, carta3, carta4)) ||
				(tieneTrio(carta1, carta3, carta5)) ||
				(tieneTrio(carta1, carta4, carta5)) ||
				(tieneTrio(carta2, carta3, carta4)) ||
				(tieneTrio(carta2, carta3, carta5)) ||
				(tieneTrio(carta2, carta4, carta5)) ||
				(tieneTrio(carta3, carta4, carta5)));
	}

	private boolean tieneTrio(Carta carta1, Carta carta2, Carta carta3) {
		int valorCarta1 = carta1.getValor();
		int valorCarta2 = carta2.getValor();
		int valorCarta3 = carta3.getValor();
		return ((valorCarta1 == valorCarta2) &&
				(valorCarta2 == valorCarta3));
	}
	
	private Jugada jugadaDeTrio(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		Jugada jugadaDeTrio;
		List<Carta> cartasDeTrio;
		if (tieneTrio(carta1, carta2, carta3)) {
			cartasDeTrio = Arrays.asList(carta1, carta2, carta3);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		if (tieneTrio(carta1, carta2, carta4)) {
			cartasDeTrio = Arrays.asList(carta1, carta2, carta4);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		if (tieneTrio(carta1, carta2, carta5)) {
			cartasDeTrio = Arrays.asList(carta1, carta2, carta5);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		if (tieneTrio(carta1, carta3, carta4)) {
			cartasDeTrio = Arrays.asList(carta1, carta3, carta4);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		if (tieneTrio(carta1, carta3, carta5)) {
			cartasDeTrio = Arrays.asList(carta1, carta3, carta5);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		if (tieneTrio(carta1, carta4, carta5)) {
			cartasDeTrio = Arrays.asList(carta1, carta4, carta5);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		if (tieneTrio(carta2, carta3, carta4)) {
			cartasDeTrio = Arrays.asList(carta2, carta3, carta4);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		if (tieneTrio(carta2, carta3, carta5)) {
			cartasDeTrio = Arrays.asList(carta2, carta3, carta5);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		if (tieneTrio(carta2, carta4, carta5)) {
			cartasDeTrio = Arrays.asList(carta2, carta4, carta5);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		else {
			cartasDeTrio = Arrays.asList(carta3, carta4, carta5);
			jugadaDeTrio = new Jugada(cartasDeTrio, "Trio");
		}
		return jugadaDeTrio;
	}
}
