package tp8.ej5;

import java.util.List;

public class Jugada {
	
	private List<Carta> cartasDeJugada;
	private String tipoDeJugada;

	public Jugada(List<Carta> cartasJugada, String tipoJugada) {
		super();
		this.cartasDeJugada = cartasJugada;
		this.tipoDeJugada = tipoJugada;
	}

	public String getTipoDeJugada() {
		return this.tipoDeJugada;
	}

	public boolean leGanaA(Jugada jugada) {
		if (this.valorJugada() == jugada.valorJugada()) {
			return this.valorDeLasCartas() > jugada.valorDeLasCartas();
		}
		else {
			return this.valorJugada() > jugada.valorJugada();
		}
	}

	private int valorJugada() {
		if (this.tipoDeJugada == "Poquer") {
			return 3;
		}
		if (this.tipoDeJugada == "Color") {
			return 2;
		}
		if (this.tipoDeJugada == "Trio") {
			return 1;
		}
		else {
			return 0;
		}
	}
	
	private int valorDeLasCartas() {
		int valorTotal = this.cartasDeJugada.stream().
				mapToInt(carta -> carta.getValor()).sum();
		return valorTotal;
	}

}
