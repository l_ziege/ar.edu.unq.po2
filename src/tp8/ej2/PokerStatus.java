package tp8.ej2;

public class PokerStatus {

	public String verificar(String carta1, String carta2, String carta3, String carta4, String carta5) {
		if (hayPoquer(carta1, carta2, carta3, carta4, carta5)) {
			return "Poquer";
		}
		if (hayColor(carta1, carta2, carta3, carta4, carta5)) {
			return "Color";
		}
		if (hayTrio(carta1, carta2, carta3, carta4, carta5)) {
			return "Trio";
		}
		return "Nada";
	}

	private boolean hayPoquer(String carta1, String carta2, String carta3, String carta4, String carta5) {
		return ((tienePoquer(carta1, carta2, carta3, carta4)) ||
				(tienePoquer(carta1, carta2, carta3, carta5)) ||
				(tienePoquer(carta1, carta2, carta4, carta5)) ||
				(tienePoquer(carta1, carta3, carta4, carta5)) ||
				(tienePoquer(carta2, carta3, carta4, carta5)));
	}

	private boolean tienePoquer(String carta1, String carta2, String carta3, String carta4) {
		String numCarta1 = carta1.substring(0, carta1.length() - 1);
		String numCarta2 = carta2.substring(0, carta1.length() - 1);
		String numCarta3 = carta3.substring(0, carta1.length() - 1);
		String numCarta4 = carta4.substring(0, carta1.length() - 1);
		return ((numCarta1.equals(numCarta2)) &&
				(numCarta2.equals(numCarta3)) &&
				(numCarta3.equals(numCarta4)));
	}

	private boolean hayColor(String carta1, String carta2, String carta3, String carta4, String carta5) {
		String paloCarta1 = carta1.substring(carta1.length() - 1);
		String paloCarta2 = carta2.substring(carta2.length() - 1);
		String paloCarta3 = carta3.substring(carta3.length() - 1);
		String paloCarta4 = carta4.substring(carta4.length() - 1);
		String paloCarta5 = carta5.substring(carta5.length() - 1);
		return ((paloCarta1.equals(paloCarta2)) &&
				(paloCarta2.equals(paloCarta3)) &&
				(paloCarta3.equals(paloCarta4)) &&
				(paloCarta4.equals(paloCarta5)));
	}

	private boolean hayTrio(String carta1, String carta2, String carta3, String carta4, String carta5) {
		return ((tieneTrio(carta1, carta2, carta3)) ||
				(tieneTrio(carta1, carta2, carta4)) ||
				(tieneTrio(carta1, carta2, carta5)) ||
				(tieneTrio(carta1, carta3, carta4)) ||
				(tieneTrio(carta1, carta3, carta5)) ||
				(tieneTrio(carta1, carta4, carta5)) ||
				(tieneTrio(carta2, carta3, carta4)) ||
				(tieneTrio(carta2, carta3, carta5)) ||
				(tieneTrio(carta2, carta4, carta5)) ||
				(tieneTrio(carta3, carta4, carta5)));
	}

	private boolean tieneTrio(String carta1, String carta2, String carta3) {
		String numCarta1 = carta1.substring(0, carta1.length() - 1);
		String numCarta2 = carta2.substring(0, carta1.length() - 1);
		String numCarta3 = carta3.substring(0, carta1.length() - 1);
		return ((numCarta1.equals(numCarta2)) &&
				(numCarta2.equals(numCarta3)));
	}
}