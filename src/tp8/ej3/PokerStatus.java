package tp8.ej3;

public class PokerStatus {

	public String verificar(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		if (hayPoquer(carta1, carta2, carta3, carta4, carta5)) {
			return "Poquer";
		}
		if (hayColor(carta1, carta2, carta3, carta4, carta5)) {
			return "Color";
		}
		if (hayTrio(carta1, carta2, carta3, carta4, carta5)) {
			return "Trio";
		}
		return "Nada";
	}

	private boolean hayPoquer(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		return ((tienePoquer(carta1, carta2, carta3, carta4)) ||
				(tienePoquer(carta1, carta2, carta3, carta5)) ||
				(tienePoquer(carta1, carta2, carta4, carta5)) ||
				(tienePoquer(carta1, carta3, carta4, carta5)) ||
				(tienePoquer(carta2, carta3, carta4, carta5)));
	}

	private boolean tienePoquer(Carta carta1, Carta carta2, Carta carta3, Carta carta4) {
		int valorCarta1 = carta1.getValor();
		int valorCarta2 = carta2.getValor();
		int valorCarta3 = carta3.getValor();
		int valorCarta4 = carta4.getValor();
		return ((valorCarta1 == valorCarta2) &&
				(valorCarta2 == valorCarta3) &&
				(valorCarta3 == valorCarta4));
	}

	private boolean hayColor(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		return ((carta1.esMismoPaloQue(carta2)) &&
				(carta2.esMismoPaloQue(carta3)) &&
				(carta3.esMismoPaloQue(carta4)) &&
				(carta4.esMismoPaloQue(carta5)));
	}

	private boolean hayTrio(Carta carta1, Carta carta2, Carta carta3, Carta carta4, Carta carta5) {
		return ((tieneTrio(carta1, carta2, carta3)) ||
				(tieneTrio(carta1, carta2, carta4)) ||
				(tieneTrio(carta1, carta2, carta5)) ||
				(tieneTrio(carta1, carta3, carta4)) ||
				(tieneTrio(carta1, carta3, carta5)) ||
				(tieneTrio(carta1, carta4, carta5)) ||
				(tieneTrio(carta2, carta3, carta4)) ||
				(tieneTrio(carta2, carta3, carta5)) ||
				(tieneTrio(carta2, carta4, carta5)) ||
				(tieneTrio(carta3, carta4, carta5)));
	}

	private boolean tieneTrio(Carta carta1, Carta carta2, Carta carta3) {
		int valorCarta1 = carta1.getValor();
		int valorCarta2 = carta2.getValor();
		int valorCarta3 = carta3.getValor();
		return ((valorCarta1 == valorCarta2) &&
				(valorCarta2 == valorCarta3));
	}

}
