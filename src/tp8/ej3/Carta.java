package tp8.ej3;

public class Carta {
	
	private int valor;
	private String palo;

	public Carta(int valor, String palo) {
		super();
		this.valor = valor;
		this.palo = palo;
	}

	public int getValor() {
		return this.valor;
	}

	public String getPalo() {
		return this.palo;
	}

	public boolean esSuperiorA(Carta carta) {
		return this.valor > carta.getValor();
	}

	public boolean esMismoPaloQue(Carta carta) {
		return this.palo.equals(carta.getPalo());
	}

}
