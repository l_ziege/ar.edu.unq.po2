package tp8.ej1;

public class PokerStatus {

	public boolean verificar(String carta1, String carta2, String carta3, String carta4, String carta5) {
		return ((tienePoquer(carta1, carta2, carta3, carta4)) ||
				(tienePoquer(carta1, carta2, carta3, carta5)) ||
				(tienePoquer(carta1, carta2, carta4, carta5)) ||
				(tienePoquer(carta1, carta3, carta4, carta5)) ||
				(tienePoquer(carta2, carta3, carta4, carta5)));
	}

	private boolean tienePoquer(String carta1, String carta2, String carta3, String carta4) {
		String numCarta1 = carta1.substring(0, carta1.length() - 1);
		String numCarta2 = carta2.substring(0, carta1.length() - 1);
		String numCarta3 = carta3.substring(0, carta1.length() - 1);
		String numCarta4 = carta4.substring(0, carta1.length() - 1);
		return ((numCarta1.equals(numCarta2)) &&
				(numCarta2.equals(numCarta3)) &&
				(numCarta3.equals(numCarta4)));
	}

}
