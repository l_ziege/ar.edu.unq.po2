package tp6.ej2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SecretariaDeDeportes {

	private List<ActividadSemanal> actividadesSemanales;

	public SecretariaDeDeportes(List<ActividadSemanal> actividadesSemanales) {
		super();
		this.actividadesSemanales = actividadesSemanales;
	}

	public SecretariaDeDeportes() {
		super();
		this.actividadesSemanales = new ArrayList<ActividadSemanal>();
	}
	
	public void agregarActividadSemanal(ActividadSemanal as) {
		this.actividadesSemanales.add(as);
	}
	
	public List<ActividadSemanal> actividadesDeFutbol() {
		List<ActividadSemanal> actividadesDeFutbol = this.actividadesSemanales.
				stream().
				filter(as -> as.getDeporte() == Deporte.FUTBOL).
				collect(Collectors.toList());
		return actividadesDeFutbol;
	}
	
	public List<ActividadSemanal> actividadesDeComplejidad(Integer complejidad) {
		List<ActividadSemanal> actividadesDeComplejidad = this.actividadesSemanales.
				stream().
				filter(as -> as.getDeporte().complejidad == complejidad).
				collect(Collectors.toList());
		return actividadesDeComplejidad;
	}
	
	public Integer cantHorasTotalesDeActividadesSemanales() {
		Integer horasTotales = this.actividadesSemanales.stream().
				mapToInt(as->as.getDuraci�n() / 60).sum();
		return horasTotales;
	}
	
	public ActividadSemanal laDeMenorCostoDeUnDeporte(Deporte deporte) {
		List<ActividadSemanal> actividadesDeDeporte = this.actividadesSemanales.
				stream().
				filter(as -> as.getDeporte() == deporte).
				collect(Collectors.toList());
		ActividadSemanal actividadMenorCosto = actividadesDeDeporte.
				stream().
				min(Comparator.comparing(ActividadSemanal::getCosto)).get();
		return actividadMenorCosto;
	}
	
	public Map<Deporte, ActividadSemanal> laDeMenorCostoDeCadaDeporte() {
		Map<Deporte, ActividadSemanal> laActividadDeMenorCostoPorDeporte =
				this.actividadesSemanales.stream().
				collect(Collectors.collectingAndThen(Collectors.groupingBy(ActividadSemanal::getDeporte,
						Collectors.minBy(Comparator.comparing(ActividadSemanal::getCosto))), 
						map -> map.entrySet().stream().
						collect(Collectors.toMap(Map.Entry::getKey,  e -> e.getValue().get()))));
		return laActividadDeMenorCostoPorDeporte;
		
		//Map<Deporte, List<ActividadSemanal>> actividadesPorDeporte = 
				//this.actividadesSemanales.stream().
				//collect(Collectors.groupingBy(ActividadSemanal::getDeporte));
		//Map<Deporte, ActividadSemanal> laActividadDeMenorCostoPorDeporte = 
				//actividadesPorDeporte.entrySet().
		
		//this.actividadesSemanales.stream().
		//collect(Collectors.collectingAndThen(Collectors.groupingBy(ActividadSemanal::getDeporte,
				//Collectors.minBy(Comparator.comparing(ActividadSemanal::getCosto))), 
				//map -> map.entrySet().stream().
				//collect(Collectors.toMap(Map.Entry::getKey,  e -> e.getValue().get()))));
		
		//		new HashMap<Deporte, ActividadSemanal>();
		//laActividadDeMenorCostoPorDeporte.put(Deporte.RUNNING, this.laDeMenorCostoDeUnDeporte(Deporte.RUNNING));
		//laActividadDeMenorCostoPorDeporte.put(Deporte.FUTBOL, this.laDeMenorCostoDeUnDeporte(Deporte.FUTBOL));
		//laActividadDeMenorCostoPorDeporte.put(Deporte.BASKET, this.laDeMenorCostoDeUnDeporte(Deporte.BASKET));
		//laActividadDeMenorCostoPorDeporte.put(Deporte.TENNIS, this.laDeMenorCostoDeUnDeporte(Deporte.TENNIS));
		//laActividadDeMenorCostoPorDeporte.put(Deporte.JABALINA, this.laDeMenorCostoDeUnDeporte(Deporte.JABALINA));
		//return laActividadDeMenorCostoPorDeporte;
		
				//this.actividadesSemanales.stream().
				//collect(Collectors.toMap(ActividadSemanal::getDeporte,
						//Function.identity(), BinaryOperator.
						//minBy(Comparator.comparing(ActividadSemanal::getCosto))));
		
				//this.actividadesSemanales.stream().
						//collect(Collectors.groupingBy(ActividadSemanal::getDeporte,
						//Collectors.minBy(Comparator.comparing(ActividadSemanal::getCosto))));
	}
	
	public void imprimirTodasLasActividades() {
		this.actividadesSemanales.stream().forEach(as -> System.out.println(as.toString()));
	}
}
