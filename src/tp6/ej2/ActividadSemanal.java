package tp6.ej2;

public class ActividadSemanal {
	
	private DiaDeLaSemana dia;
	private Integer horaInicio; // representada en formato 1500, 2200, 900, etc.
	private Integer duraci�n;
	private Deporte deporte;
	private Integer costo;
	
	public ActividadSemanal(DiaDeLaSemana dia, Integer horaInicio, Integer duraci�n, Deporte deporte) {
		super();
		this.dia = dia;
		this.horaInicio = horaInicio;
		this.duraci�n = duraci�n;
		this.deporte = deporte;
		this.costo = costoDeLaActividadSegunDiaYDeporte(dia, deporte);
	}

	private Integer costoDeLaActividadSegunDiaYDeporte(DiaDeLaSemana dia, Deporte deporte) {
		return costoDia(dia) + (deporte.complejidad * 200);
	}

	private int costoDia(DiaDeLaSemana dia) {
		if ((dia == DiaDeLaSemana.LUNES) 
				|| (dia == DiaDeLaSemana.MARTES)
				|| (dia == DiaDeLaSemana.MIERCOLES)) {
			return 500 * this.cantidadDeHoras();
		} else {
			return 1000 * this.cantidadDeHoras();
		}
	}
	
	@Override
	public String toString() {
		return "Deporte: " + this.deporte.toString() + ". Dia: " + this.dia.toString() +
				" A LAS: " + (this.horaInicio / 100) + " . Duraci�n: " + (this.duraci�n / 60) +
				" hora(s).";
	}

	private int cantidadDeHoras() {
		return this.duraci�n / 60;
	}

	public DiaDeLaSemana getDia() {
		return dia;
	}

	public Integer getHoraInicio() {
		return horaInicio;
	}

	public Integer getDuraci�n() {
		return duraci�n;
	}

	public Deporte getDeporte() {
		return deporte;
	}

	public Integer getCosto() {
		return costo;
	}
}
