package tp6.ej2;

public enum Deporte {

	RUNNING(1), FUTBOL(2), BASKET(2), TENNIS(3), JABALINA(4);
	
	Integer complejidad;
	
	Deporte(Integer complejidad) {
		this.complejidad = complejidad;
	}
}
