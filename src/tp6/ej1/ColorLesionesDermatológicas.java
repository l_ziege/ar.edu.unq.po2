package tp6.ej1;

public enum ColorLesionesDermatolˇgicas {
	
	ROJO {

		@Override
		public String descripciˇnPredefinida() {
			return "Describe de la forma en que se debe describir el color Rojo.";
		}

		@Override
		public int nivelDeRiesgo() {
			return 1;
		}

		@Override
		public ColorLesionesDermatolˇgicas prˇximoColorEnProcesoMadurativo() {
			return GRIS;
		}
		
	}, GRIS {

		@Override
		public String descripciˇnPredefinida() {
			return "Describe de la forma en que se debe describir el color Gris.";
		}

		@Override
		public int nivelDeRiesgo() {
			return 2;
		}

		@Override
		public ColorLesionesDermatolˇgicas prˇximoColorEnProcesoMadurativo() {
			return AMARILLO;
		}
		
	}, AMARILLO {

		@Override
		public String descripciˇnPredefinida() {
			return "Describe de la forma en que se debe describir el color Amarillo.";
		}

		@Override
		public int nivelDeRiesgo() {
			return 3;
		}

		@Override
		public ColorLesionesDermatolˇgicas prˇximoColorEnProcesoMadurativo() {
			return MIEL;
		}
		
	}, MIEL {

		@Override
		public String descripciˇnPredefinida() {
			return "Describe de la forma en que se debe describir el color Miel.";
		}

		@Override
		public int nivelDeRiesgo() {
			return 4;
		}

		@Override
		public ColorLesionesDermatolˇgicas prˇximoColorEnProcesoMadurativo() {
			return ROJO;
		}
		
	};

	public abstract String descripciˇnPredefinida();
	
	public abstract int nivelDeRiesgo();
	
	public abstract ColorLesionesDermatolˇgicas prˇximoColorEnProcesoMadurativo();
}
