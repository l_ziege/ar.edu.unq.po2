package tp10.ej2;

import java.util.List;

// PATRON OBSERVER
// ROL: ConcreteObserver

public class AppMóvil {
	
	private Servidor servidorObservado;
	private Deporte deporteDeInteres;
	private String contrincanteDeInteres;
	private boolean hayActualización;

	public AppMóvil(Servidor servidor, Deporte deporte, String contrincante) {
		super();
		this.servidorObservado = servidor;
		this.deporteDeInteres = deporte;
		this.contrincanteDeInteres = contrincante;
		this.hayActualización = false;
		
		servidor.registrarAppMóvilObservadora(this);
	}
	
	public void nuevoContrincanteDeInteres(String nuevoContrincante) {
		this.contrincanteDeInteres = nuevoContrincante;
	}
	
	public void nuevoDeporteDeInteres(Deporte nuevoDeporte) {
		this.deporteDeInteres = nuevoDeporte;
	}
	
	public void nuevoServidorAObservar(Servidor nuevoServidor) {
		this.servidorObservado = nuevoServidor;
		this.hayActualización = false;
		nuevoServidor.registrarAppMóvilObservadora(this);
	}
	
	public void setActualización(Servidor servidor) {
		if (this.servidorObservado == servidor) {
			this.hayActualización = true;
		}
	}
	
	public boolean hayActualización() {
		return this.hayActualización;
	}

	public String getContrincanteDeInterés() {
		return this.contrincanteDeInteres;
	}
	
	public Deporte getDeporteDeInterés() {
		return this.deporteDeInteres;
	}

	public List<ResultadoDePartido> pedirResultadosDeInteresAlServidor() {
		this.hayActualización = false;
		return this.servidorObservado.getResultadosDeInteres(this.deporteDeInteres, this.contrincanteDeInteres);
	}

}
