package tp10.ej2;

import java.util.List;

public class ResultadoDePartido {
	
	private String resultado;
	private List<String> contrincantes;
	private Deporte deporte;

	public ResultadoDePartido(String resultado, List<String> contrincantes, Deporte deporte) {
		super();
		this.resultado = resultado;
		this.contrincantes = contrincantes;
		this.deporte = deporte;
	}

	public Deporte getDeporte() {
		return this.deporte;
	}

	public List<String> getContrincantes() {
		return this.contrincantes;
	}

	
}
