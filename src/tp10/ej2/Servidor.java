package tp10.ej2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// PATRON OBSERVER (AppDeResultadosDePartidos - Servidor)
// ROL: ConcreteObserver

// PATRON OBSERVER (Servidor - AppMóvil)
// ROL: ConcreteSubject

public class Servidor {

	private List<Deporte> deportesSuscripto;
	private AppDeResultadosDePartidos appObservada;
	private List<ResultadoDePartido> resultados;
	private List<AppMóvil> appsMóvilesObservadoras;
	
	public Servidor(List<Deporte> deportes) {
		super();
		this.deportesSuscripto = deportes;
		this.resultados = new ArrayList<ResultadoDePartido>();
		this.appsMóvilesObservadoras = new ArrayList<AppMóvil>();
	}

	public void registrarAppObservada(AppDeResultadosDePartidos appObservada) {
		this.appObservada = appObservada;
	}

	public List<ResultadoDePartido> getResultados() {
		return this.resultados;
	}

	public List<Deporte> getDeportesSuscripto() {
		return this.deportesSuscripto;
	}

	public void recibirResultadoDeLaApp(ResultadoDePartido resultado, AppDeResultadosDePartidos app) {
		if (this.appObservada == app) {
			this.resultados.add(resultado);
			this.notificarActualizaciónAAppsMóvilesInteresadas(resultado);
		}
	}

	private void notificarActualizaciónAAppsMóvilesInteresadas(ResultadoDePartido resultado) {
		this.appsMóvilesObservadoras.stream().forEach(app -> this.notificarActualizaciónSiEstáInteresada(app, resultado));
	}

	private void notificarActualizaciónSiEstáInteresada(AppMóvil app, ResultadoDePartido resultado) {
		if (resultado.getContrincantes().contains(app.getContrincanteDeInterés())
			||
			resultado.getDeporte() == app.getDeporteDeInterés()) {
			app.setActualización(this);
		}
	}

	public void registrarAppMóvilObservadora(AppMóvil appObservadora) {
		this.appsMóvilesObservadoras.add(appObservadora);
	}
	
	public void eliminarAppMóvilObservadora(AppMóvil appObservadora) {
		this.appsMóvilesObservadoras.remove(appObservadora);
	}

	public List<ResultadoDePartido> getResultadosDeInteres(Deporte deporteDeInteres, String contrincanteDeInteres) {
		return this.resultados.stream().filter(result -> result.getDeporte() == deporteDeInteres || result.getContrincantes().contains(contrincanteDeInteres)).collect(Collectors.toList());
	}
	
}