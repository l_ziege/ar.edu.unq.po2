package tp10.ej2;

import java.util.ArrayList;
import java.util.List;

// PATRON OBSERVER
// ROL: ConcreteSubject

public class AppDeResultadosDePartidos {

	private List<ResultadoDePartido> resultados;
	private List<Servidor> servidoresObservadores;
	
	public AppDeResultadosDePartidos() {
		super();
		this.resultados = new ArrayList<ResultadoDePartido>();
		this.servidoresObservadores = new ArrayList<Servidor>();
	}

	public void ingresarResultadoDePartido(ResultadoDePartido resultado) {
		this.resultados.add(resultado);
		this.notificarResultadoAServidoresInteresados(resultado);
	}
	
	private void notificarResultadoAServidoresInteresados(ResultadoDePartido resultado) {
		this.servidoresObservadores.stream().forEach(servidor -> this.notificarSiEstáInteresadoEnElResultado(servidor, resultado));
	}

	private void notificarSiEstáInteresadoEnElResultado(Servidor servidor, ResultadoDePartido resultado) {
		if (servidor.getDeportesSuscripto().contains(resultado.getDeporte())) {
			servidor.recibirResultadoDeLaApp(resultado, this);
		}
	}

	public void registrarServidorObservador(Servidor servidor) {
		this.servidoresObservadores.add(servidor);
		servidor.registrarAppObservada(this);
	}

	public void eliminarServidorObservador(Servidor servidor) {
		this.servidoresObservadores.remove(servidor);
	}
	
	public List<ResultadoDePartido> getResultados() {
		return this.resultados;
	}

	public List<Servidor> getServidoresObservadores() {
		return this.servidoresObservadores;
	}

}
