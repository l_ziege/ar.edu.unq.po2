package tp10.ej2;

import java.util.List;

public class M�vil {

	private AppM�vil app;
	
	public M�vil(AppM�vil appM�vil) {
		this.app = appM�vil;
	}

	public boolean hayActualizaci�nEnLaApp() {
		return this.app.hayActualizaci�n();
	}

	public List<ResultadoDePartido> pedirResultadosDeInteresALaApp() {
		return this.app.pedirResultadosDeInteresAlServidor();
	}

	public void nuevoDeporteDeInteresParaLaApp(Deporte nuevoDeporte) {
		this.app.nuevoDeporteDeInteres(nuevoDeporte);
	}

	public void nuevoContrincanteDeInteresParaLaApp(String nuevoContrincante) {
		this.app.nuevoContrincanteDeInteres(nuevoContrincante);
	}

}
