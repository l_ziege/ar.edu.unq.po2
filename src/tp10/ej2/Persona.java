package tp10.ej2;

import java.util.Arrays;
import java.util.List;

public class Persona {
	
	private String nombre;
	private M�vil m�vil;

	public Persona(String nombre, M�vil m�vil) {
		super();
		this.nombre = nombre;
		this.m�vil = m�vil;
	}
	
	public void nuevoDeporteDeInteresParticular(Deporte nuevoDeporte) {
		this.m�vil.nuevoDeporteDeInteresParaLaApp(nuevoDeporte);
	}
	
	public void nuevoContrincanteDeInteresParticular(String nuevoContrincante) {
		this.m�vil.nuevoContrincanteDeInteresParaLaApp(nuevoContrincante);
	}

	public List<ResultadoDePartido> pedirResultadosDeInteres() {
		if (this.m�vil.hayActualizaci�nEnLaApp()) {
			return this.m�vil.pedirResultadosDeInteresALaApp();
		} else {
			return Arrays.asList();
		}
	}

}
