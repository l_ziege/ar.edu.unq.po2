package tp10.ej1;

public class Autor {

	private String nombre;

	public Autor(String nombre) {
		super();
		this.nombre = nombre;
	}

	public String getNombre() {
		return this.nombre;
	}
	
}
