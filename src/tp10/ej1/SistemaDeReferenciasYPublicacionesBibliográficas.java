package tp10.ej1;

import java.util.ArrayList;
import java.util.List;

// PATRON OBSERVER
// ROL: ConcreteSubject

public class SistemaDeReferenciasYPublicacionesBibliográficas {

	private List<ArtículoCientífico> articulos;
	private List<MiembroDelLaboratorio> miembrosObservadores;
	
	public SistemaDeReferenciasYPublicacionesBibliográficas() {
		super();
		this.articulos = new ArrayList<ArtículoCientífico>();
		this.miembrosObservadores = new ArrayList<MiembroDelLaboratorio>();
	}
	
	public void registrarMiembroObservador(MiembroDelLaboratorio miembro) {
		this.miembrosObservadores.add(miembro);
		miembro.registrarSistemaObservado(this);
	}
	
	public void eliminarMiembroObservador(MiembroDelLaboratorio miembro) {
		this.miembrosObservadores.remove(miembro);
	}
	
	public void cargarArtículoCientífico(String título, List<Autor> autores, List<Filiación> filiaciones, 
			String tipo, String lugar, List<String> palabrasClave) {
		ArtículoCientífico artículo = new ArtículoCientífico(título, autores, filiaciones, tipo, lugar, palabrasClave);
		this.articulos.add(artículo);
		this.notificarArtículoAMiembrosInteresados(artículo);
	}

	private void notificarArtículoAMiembrosInteresados(ArtículoCientífico artículo) {
		this.miembrosObservadores.forEach(miembro -> this.notificarSiEstáInteresadoEnElArtículo(miembro, artículo));
	}

	private void notificarSiEstáInteresadoEnElArtículo(MiembroDelLaboratorio miembro, ArtículoCientífico artículo) {
		if (this.estáInteresadoElMiembroDelLaboratorioEnElArtículo(miembro, artículo)) {
			miembro.recibirNotificaciónDelSistema(this);
		}
	}

	private boolean estáInteresadoElMiembroDelLaboratorioEnElArtículo(MiembroDelLaboratorio miembro, ArtículoCientífico artículo) {
		return (this.estáEntreLosTemasDeInvestigaciónElTítulo(miembro.getTemasDeInvestigación(), artículo.getTítulo())
			    ||
			    this.estáEntreLosTemasDeInvestigaciónAlgunoDeLosAutores(miembro.getTemasDeInvestigación(), artículo.getAutores())
			    ||
			    this.estáEntreLosTemasDeInvestigaciónAlgunaDeLasFiliaciones(miembro.getTemasDeInvestigación(), artículo.getFiliaciones())
			    ||
			    this.estáEntreLosTemasDeInvestigaciónElTipoDeArtículo(miembro.getTemasDeInvestigación(), artículo.getTipo())
			    ||
			    this.estáEntreLosTemasDeInvestigaciónElLugarDondeFuePublicado(miembro.getTemasDeInvestigación(), artículo.getLugarDondeSePublicó())
			    ||
			    this.estáEntreLosTemasDeInvestigaciónAlgunaPalabraClave(miembro.getTemasDeInvestigación(), artículo.getPalabrasClave()));
	}
	
	private boolean estáEntreLosTemasDeInvestigaciónElTítulo(List<String> temasDeInvestigación, String título) {
		return temasDeInvestigación.contains(título);
	}
	
	private boolean estáEntreLosTemasDeInvestigaciónAlgunoDeLosAutores(List<String> temasDeInvestigación, List<Autor> autores) {
		int i = 0;
		while (!this.estáElAutorEntreLosTemasDeInvestigación(autores.get(i), temasDeInvestigación) && i < autores.size()-1) {
			i++;
		}
		return this.estáElAutorEntreLosTemasDeInvestigación(autores.get(i), temasDeInvestigación);
	}

	private boolean estáElAutorEntreLosTemasDeInvestigación(Autor autor, List<String> temasDeInvestigación) {
		return temasDeInvestigación.contains(autor.getNombre());
	}

	private boolean estáEntreLosTemasDeInvestigaciónAlgunaDeLasFiliaciones(List<String> temasDeInvestigación, List<Filiación> filiaciones) {
		int i = 0;
		while (!this.estáLaFiliaciónEntreLosTemasDeInvestigación(filiaciones.get(0), temasDeInvestigación) && i < filiaciones.size()-1) {
			i++;
		}
		return this.estáLaFiliaciónEntreLosTemasDeInvestigación(filiaciones.get(i), temasDeInvestigación);
	}

	private boolean estáLaFiliaciónEntreLosTemasDeInvestigación(Filiación filiación, List<String> temasDeInvestigación) {
		return temasDeInvestigación.contains(filiación.getNombre());
	}

	private boolean estáEntreLosTemasDeInvestigaciónElTipoDeArtículo(List<String> temasDeInvestigación, String tipo) {
		return temasDeInvestigación.contains(tipo);
	}

	private boolean estáEntreLosTemasDeInvestigaciónElLugarDondeFuePublicado(List<String> temasDeInvestigación, String lugarDondeSePublicó) {
		return temasDeInvestigación.contains(lugarDondeSePublicó);
	}

	private boolean estáEntreLosTemasDeInvestigaciónAlgunaPalabraClave(List<String> temasDeInvestigación, List<String> palabrasClave) {
		int i = 0;
		while (!this.estáLaPalabraClaveEntreSusTemasDeInvestigación(palabrasClave.get(i), temasDeInvestigación) && i < palabrasClave.size()-1) {
			i++;
		}
		return this.estáLaPalabraClaveEntreSusTemasDeInvestigación(palabrasClave.get(i), temasDeInvestigación);
	}

	private boolean estáLaPalabraClaveEntreSusTemasDeInvestigación(String palabraClave, List<String> temasDeInvestigación) {
		return temasDeInvestigación.contains(palabraClave);
	}

	public List<ArtículoCientífico> getArticulos() {
		return this.articulos;
	}
}
