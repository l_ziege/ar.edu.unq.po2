package tp10.ej1;

import java.util.List;

public class ArtículoCientífico {

	private String título;
	private List<Autor> autores;
	private List<Filiación> filiaciones;
	private String tipo;
	private String lugarDondeSePublicó;
	private List<String> palabrasClave;
	
	public ArtículoCientífico(String título, List<Autor> autores, List<Filiación> filiaciones, String tipo,
			String lugarDondeSePublicó, List<String> palabrasClave) {
		super();
		this.título = título;
		this.autores = autores;
		this.filiaciones = filiaciones;
		this.tipo = tipo;
		this.lugarDondeSePublicó = lugarDondeSePublicó;
		this.palabrasClave = palabrasClave;
	}

	public String getTítulo() {
		return this.título;
	}

	public List<Autor> getAutores() {
		return this.autores;
	}

	public List<Filiación> getFiliaciones() {
		return this.filiaciones;
	}

	public String getTipo() {
		return this.tipo;
	}

	public String getLugarDondeSePublicó() {
		return this.lugarDondeSePublicó;
	}

	public List<String> getPalabrasClave() {
		return this.palabrasClave;
	}
	
}
