package tp10.ej1;

import java.util.ArrayList;
import java.util.List;

// PATRON OBSERVER
// ROL: ConcreteObserver

public class MiembroDelLaboratorio {

	private String nombre;
	private List<String> temasDeInvestigación;
	private SistemaDeReferenciasYPublicacionesBibliográficas sistemaObservado;
	
	public MiembroDelLaboratorio(String nombre) {
		super();
		this.nombre = nombre;
		this.temasDeInvestigación = new ArrayList<String>();
	}
	
	public void agregarTemaDeInvestigación(String tema) {
		this.temasDeInvestigación.add(tema);
	}
	
	public void registrarSistemaObservado(SistemaDeReferenciasYPublicacionesBibliográficas sistemaObservado) {
		this.sistemaObservado = sistemaObservado;
	}
	
	public void recibirNotificaciónDelSistema(SistemaDeReferenciasYPublicacionesBibliográficas sistemaObservado) {
		if (this.sistemaObservado == sistemaObservado) {
			System.out.println("Hay una nueva publicación de un tema de su interés en el sistema.");
		} else {
			System.out.println("El sistema emisor de la notificación no es el observado.");
		}
	}

	public String getNombre() {
		return this.nombre;
	}

	public List<String> getTemasDeInvestigación() {
		return this.temasDeInvestigación;
	}
	
}
