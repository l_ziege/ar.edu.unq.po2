package tp10.ej0.complexObserver;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Importante: esta clase fue desarrollada a modo de ejemplo y disparador
 * para ser criticados y mejorados en la practica de Observer de la materia
 * Objetos 2 de la UNQ.
 */

public class RelojHumedad implements Observer{
	
	private List<ComplexSensor> misSensores;
	private int humedadActual;
	
	
	public RelojHumedad(List<ComplexSensor> misSensores){
		this.misSensores= misSensores;
		this.agregarObserver();
	}
	
	private void agregarObserver() {
		for (ComplexSensor sensor: this.misSensores) {
			sensor.addObserver(this);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		String aspecto = (String) arg;
		if(aspecto.equals("Humedad") && this.misSensores.contains(o)){
			this.getHumedadObserver(o);
		}
		
	}

	private void getHumedadObserver(Observable o) {
		for (ComplexSensor sensor: this.misSensores) {
			if (sensor == o) {
				this.humedadActual=sensor.getHumedad();
			}
		}
	}

}
