package tp10.ej0.complexObserver;

import java.util.List;

/**
 * Importante: esta clase fue desarrollada a modo de ejemplo y disparador
 * para ser criticados y mejorados en la practica de Observer de la materia
 * Objetos 2 de la UNQ.
 */

import java.util.Observable;
import java.util.Observer;

public class AireAcondicionado implements Observer{
	
	private List<ComplexSensor> misSensores;
	
	
	public AireAcondicionado(List<ComplexSensor> misSensores){
		this.misSensores=misSensores;
		this.agregarObserver();
	}

	private void agregarObserver() {
		for (ComplexSensor sensor: this.misSensores) {
			sensor.addObserver(this);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		String aspecto = (String) arg;
		if(aspecto.equalsIgnoreCase("TemperaturaInterior") && this.misSensores.contains(o)){
			this.verificarContextoObserver(o);
		}
	}

	private void verificarContextoObserver(Observable o) {
		for (ComplexSensor sensor: this.misSensores) {
			if (sensor == o) {
				this.verificarContexto(sensor.getTemperaturaInterior());
			}
		}
	}

	private void verificarContexto(int temperaturaInterior) {
		if(temperaturaInterior>25){
			//encender motor aire acondicionado 15 segundos
		}
		
	}

}
