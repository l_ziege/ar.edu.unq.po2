package tp10.ej0.simpleObserver;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Importante: esta clase fue desarrollada a modo de ejemplo y disparador
 * para ser criticados y mejorados en la practica de Observer de la materia
 * Objetos 2 de la UNQ.
 */

public class LlamadorBomberos implements Observer{
	
	private List<Sensor> misSensores;
	
	public LlamadorBomberos(List<Sensor> sensores){
		this.misSensores=sensores;
		this.agregarObserver();
	}

	private void agregarObserver() {
		for (Sensor sensor: this.misSensores) {
			sensor.addObserver(this); //Se agrega como observador
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		//sabe que recibe solamente este mensaje cuando cambio la temperatura, 
		//Y la recibe solamente de uno de los sensores con el que fue iniciado.
		
		this.verificarSituaciónObserver(o);
		
	}

	private void verificarSituaciónObserver(Observable o) {
		for (Sensor sensor: this.misSensores) {
			if (sensor == o) {
				this.verificarSituacion(sensor.getTemperatura());
			}
		}
	}

	private void verificarSituacion(int temperatura) {
		if(temperatura > 60){
			//Llamar a los bomberos
		}
	}

}
