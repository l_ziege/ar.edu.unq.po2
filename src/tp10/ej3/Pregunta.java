package tp10.ej3;

public class Pregunta {

	private int n�mero;
	private String pregunta;

	public Pregunta(int n�mero, String pregunta) {
		super();
		this.n�mero = n�mero;
		this.pregunta = pregunta;
	}

	public int getN�mero() {
		return this.n�mero;
	}

	public String getPregunta() {
		return this.pregunta;
	}
	
}
