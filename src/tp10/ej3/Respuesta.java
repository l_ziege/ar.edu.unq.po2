package tp10.ej3;

public class Respuesta {
	
	private int n�mero;
	private String respuesta;

	public Respuesta(int n�mero, String respuesta) {
		super();
		this.n�mero = n�mero;
		this.respuesta = respuesta;
	}

	public int getN�mero() {
		return this.n�mero;
	}

	public String getRespuesta() {
		return this.respuesta;
	}

}
