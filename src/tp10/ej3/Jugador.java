package tp10.ej3;

import java.util.ArrayList;
import java.util.List;

// PATRON OBSERVER
// ROL: ConcreteObserver

public class Jugador {
	
	private String nombre;
	private SistemaDePreguntasYRespuestas sistemaObservado;
	private List<Pregunta> preguntasAResponder;

	public Jugador(String nombre) {
		super();
		this.nombre = nombre;
		this.preguntasAResponder = new ArrayList<Pregunta>();
	}
	
	public void sumarseASiguientePartidaDelSistema(SistemaDePreguntasYRespuestas sistema) {
		sistema.sumarParticipanteASiguientePartida(this);
	}

	public void recibirNotificaciónDeAceptación(SistemaDePreguntasYRespuestas sistema, List<Pregunta> preguntas) {
		this.sistemaObservado = sistema;
		this.preguntasAResponder = preguntas;
		System.out.println("Estas listo para jugar");
	}
	
	public void enviarRespuesta(Respuesta respuestaAEnviar) {
		this.sistemaObservado.recibirRespuesta(this, respuestaAEnviar, this.preguntasAResponder.get(respuestaAEnviar.getNúmero() - 1));
	}
	
	public void recibirNotificaciónDeRespuestaIncorrecta(SistemaDePreguntasYRespuestas sistema) {
		if (this.sistemaObservado == sistema) {
			System.out.println("Pregunta respondida incorrectamente");
		}
	}
	
	public void recibirNotificaciónDeRespuestaCorrecta(SistemaDePreguntasYRespuestas sistema) {
		if (this.sistemaObservado == sistema) {
			System.out.println("Pregunta respondida correctamente");
		}
	}

	public void recibirNotificaciónDePreguntaRespondidaPorElParticipante(SistemaDePreguntasYRespuestas sistema, String nombre, String enunciado) {
		if (this.sistemaObservado == sistema) {
			System.out.println("El/La jugador/a " + nombre + " respondió correctamente a la pregunta: " + enunciado);
		}
	}
	
	public void recibirNotificaciónDeFinalizaciónDelConcursoYGanador(SistemaDePreguntasYRespuestas sistema, String nombre) {
		if (this.sistemaObservado == sistema) {
			System.out.println("Partida finalizada! El/La ganador/a es " + nombre + "!!!");
		}
	}

	public String getNombre() {
		return this.nombre;
	}

}
