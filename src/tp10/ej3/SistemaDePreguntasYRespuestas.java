package tp10.ej3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

// PATRON OBSERVER
// ROL: ConcreteSubject

public class SistemaDePreguntasYRespuestas {

	private Map<Jugador, List<Respuesta>> participantesConRespuestas;
	private List<Pregunta> preguntas;
	private List<Respuesta> respuestas;
	private boolean hayJuegoIniciado;
	
	// Las lista de preguntas y respuestas deben contener 5 elementos cada una.
	// Las respuestas están en el orden de las preguntas.
	public SistemaDePreguntasYRespuestas(List<Pregunta> preguntas, List<Respuesta> respuestas) {
		super();
		this.participantesConRespuestas = new HashMap<Jugador, List<Respuesta>>();
		this.preguntas = preguntas;
		this.respuestas = respuestas;
		this.hayJuegoIniciado = false;
	}
	
	public void sumarParticipanteASiguientePartida(Jugador participante) {
		if (this.participantesConRespuestas.size() < 5) {
			this.participantesConRespuestas.put(participante, new ArrayList<Respuesta>());
		} else {
			System.out.println("No se puede sumar a la partida porque ya tiene 5 participantes o ya hay una partida iniciada");
		}
	}
	
	public void iniciarPartida() {
		if (!this.hayJuegoIniciado && this.participantesConRespuestas.size() == 5) {
			this.participantesConRespuestas.keySet().stream().forEach(participante -> participante.recibirNotificaciónDeAceptación(this, this.preguntas));
			this.hayJuegoIniciado = true;
		} else {
			System.out.println("Ya hay un juego iniciado actualmente o no hay suficientes participantes para iniciar uno");
		}
	}
	
	public void recibirRespuesta(Jugador participante, Respuesta respuesta, Pregunta pregunta) {
		if (this.hayJuegoIniciado) {
			this.verificarRespuestaSiAúnNoFueRespondida(participante, respuesta, pregunta);
		} else {
			System.out.println("Acción no permitida! No hay un juego iniciado actualmente");
		}
	}

	private void verificarRespuestaSiAúnNoFueRespondida(Jugador participante, Respuesta respuesta, Pregunta pregunta) {
		if (!this.laRespuestaDelParticipanteYaFueRespondida(respuesta, participante)) {
			this.verificarSiLaRespuestaEsCorrecta(participante, respuesta, pregunta);
		} else {
			System.out.println("La pregunta ya fue respondida");
		}
	}
	
	private void verificarSiLaRespuestaEsCorrecta(Jugador participante, Respuesta respuesta, Pregunta pregunta) {
		if (this.esRespuestaCorrecta(respuesta, pregunta)) {
			this.contabilizarRespuestaDelParticipante(respuesta, participante);
			this.notificarNuevaPreguntaRespondida(pregunta, participante);
		} else {
			participante.recibirNotificaciónDeRespuestaIncorrecta(this);
		}
	}

	private boolean esRespuestaCorrecta(Respuesta respuesta, Pregunta pregunta) {
		return this.respuestas.get(pregunta.getNúmero() - 1).getRespuesta().equals(respuesta.getRespuesta());
	}
	
	private void contabilizarRespuestaDelParticipante(Respuesta respuesta, Jugador participante) {
		List<Respuesta> respuestasActuales = this.participantesConRespuestas.get(participante);
		respuestasActuales.add(respuesta);
		this.participantesConRespuestas.put(participante, respuestasActuales);
	}
	
	private void notificarNuevaPreguntaRespondida(Pregunta pregunta, Jugador participante) {
		if (this.participantesConRespuestas.get(participante).size() < 5) {
			participante.recibirNotificaciónDeRespuestaCorrecta(this);
			this.participantesConRespuestas.keySet().stream().forEach(part -> part.recibirNotificaciónDePreguntaRespondidaPorElParticipante(this, participante.getNombre(), pregunta.getPregunta()));
		} else {
			this.participantesConRespuestas.keySet().stream().forEach(part -> part.recibirNotificaciónDeFinalizaciónDelConcursoYGanador(this, participante.getNombre()));
			this.finalizarPartida();
		}
	}

	public void finalizarPartida() {
		if (this.hayJuegoIniciado) {
			this.participantesConRespuestas.clear();
			this.hayJuegoIniciado = false;
		} else {
			System.out.println("No hay un juego iniciado actualmente para finalizar");
		}
	}

	private boolean laRespuestaDelParticipanteYaFueRespondida(Respuesta respuesta, Jugador participante) {
		return this.respuestasDelParticipante(participante).stream().anyMatch(res -> res.getNúmero() == respuesta.getNúmero());
	}
	
	public Set<Jugador> getParticipantes() {
		return this.participantesConRespuestas.keySet();
	}
	
	public List<Respuesta> respuestasDelParticipante(Jugador participante) {
		return this.participantesConRespuestas.get(participante);
	}
	
}
