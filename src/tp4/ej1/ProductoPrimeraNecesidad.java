package tp4.ej1;

public class ProductoPrimeraNecesidad {

	private String nombre;
	private double precio;
	private boolean esPrecioCuidado;
	
	public ProductoPrimeraNecesidad(String nom, double pre, boolean precioCuidado) {
		super();
		this.nombre = nom;
		this.precio = pre;
		this.esPrecioCuidado = precioCuidado;
	}

	public double getPrecio() {
		return this.precio * 0.9;
	}

}
