package tp4.ej1;

public class Producto {

	private String nombre;
	private double precio;
	private boolean esPrecioCuidado;
	
	public Producto(String nom, double pre, boolean precioCuidado) {
		super();
		this.nombre = nom;
		this.precio = pre;
		this.esPrecioCuidado = precioCuidado;
	}

	public Producto(String nom, double pre) {
		super();
		this.nombre = nom;
		this.precio = pre;
		this.esPrecioCuidado = false;
	}
	
	public void aumentarPrecio(double aumento) {
		this.precio += aumento;
	}

	public String getNombre() {
		return nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public boolean esPrecioCuidado() {
		return esPrecioCuidado;
	}
}