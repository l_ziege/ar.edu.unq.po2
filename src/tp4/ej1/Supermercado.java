package tp4.ej1;

import java.util.ArrayList;
import java.util.List;

public class Supermercado {

	private String nombre;
	private String direcci�n;
	private List<Producto> productos;
	
	public Supermercado(String nom, String dir) {
		super();
		this.nombre = nom;
		this.direcci�n = dir;
		this.productos = new ArrayList<Producto>();
	}

	public int getCantidadDeProductos() {
		return this.productos.size();
	}

	public void agregarProducto(Producto producto) {
		this.productos.add(producto);
	}

	public Object getPrecioTotal() {
		double precioTotal = 0;
		for (Producto p: this.productos) {
			precioTotal += p.getPrecio();
		}
		return precioTotal;
	}
}
