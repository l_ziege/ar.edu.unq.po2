package tp4.ej3;

public class Ingreso extends IngresoPercibido {

	public Ingreso(String mesDePercepción, String concepto, double montoPercibido) {
		super();
		this.mesDePercepción = mesDePercepción;
		this.concepto = concepto;
		this.montoPercibido = montoPercibido;
	}

	@Override
	public double getMontoImponible() {
		return this.getMontoPercibido();
	}

}
