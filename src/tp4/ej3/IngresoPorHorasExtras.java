package tp4.ej3;

public class IngresoPorHorasExtras extends IngresoPercibido {
	
	private int cantHorasExtras;

	public IngresoPorHorasExtras(String mesDePercepción, String concepto, double montoPercibido, 
			int cantHorasExtras) {
		super();
		this.mesDePercepción = mesDePercepción;
		this.concepto = concepto;
		this.montoPercibido = montoPercibido;
		this.cantHorasExtras = cantHorasExtras;
	}

	@Override
	public double getMontoImponible() {
		return 0;
	}

	
}
