package tp4.ej3;

public abstract class IngresoPercibido {
	
	protected String mesDePercepción;
	protected String concepto;
	protected double montoPercibido;

	public abstract double getMontoImponible();

	public double getMontoPercibido() {
		return montoPercibido;
	}
}
