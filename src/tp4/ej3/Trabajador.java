package tp4.ej3;

import java.util.ArrayList;
import java.util.List;

public class Trabajador {

	private List<IngresoPercibido> ingresosPercibidos;
	
	public Trabajador() {
		super();
		this.ingresosPercibidos = new ArrayList<IngresoPercibido>();
	}
	
	public void registrarIngresoPercibido(IngresoPercibido ingreso) {
		this.ingresosPercibidos.add(ingreso);
	}
	
	public double getTotalPercibido() {
		double montoTotalPercibido = 0;
		
		for (IngresoPercibido ingreso: this.ingresosPercibidos) {
			montoTotalPercibido += ingreso.getMontoPercibido();
		}
		
		return montoTotalPercibido;
	}
	
	public double getMontoImponible() {
		double montoTotalImponible = 0;
		
		for (IngresoPercibido ingreso: this.ingresosPercibidos) {
			montoTotalImponible += ingreso.getMontoImponible();
		}
		
		return montoTotalImponible;
	}
	
	public double getImpuestoAPagar() {
		return this.getMontoImponible() * 0.02;
	}
}
