package tp4.ej2;

public class ProductoPrimeraNecesidad {

	private String nombre;
	private double precio;
	private boolean esPrecioCuidado;
	private int porcentaje;
	
	public ProductoPrimeraNecesidad(String nom, double pre, boolean precioCuidado, int porcentaje) {
		super();
		this.nombre = nom;
		this.precio = pre;
		this.esPrecioCuidado = precioCuidado;
		this.porcentaje = porcentaje;
	}

	public double getPrecio() {
		return (this.precio - (this.precio * this.porcentaje / 100));
	}

}
