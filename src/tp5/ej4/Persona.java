package tp5.ej4;

import java.util.Date;

public class Persona extends SerVivo {
	
	private Date fechaNac;

	public Persona(String nombre, Date fechaNac) {
		super();
		this.nombre = nombre;
		this.fechaNac = fechaNac;
	}

	public Date getFechaNac() {
		return fechaNac;
	}
	
	public int getEdad() {
		Date diaDeHoy = new Date(2021, 8, 23);
		int edad = diaDeHoy.getYear() - fechaNac.getYear();
		if ( (esAnteriorMes(fechaNac.getMonth(), diaDeHoy.getMonth())) || 
				((mismoMes(fechaNac.getMonth(), diaDeHoy.getMonth())) && 
						esDiaAnteriorOIgual(fechaNac.getDay(), diaDeHoy.getDay()))) {
			return edad;
		} else {
			return edad - 1;
		}
	}
	
	private boolean esAnteriorMes(int month1, int month2) {
		return month1 < month2;
	}
	
	private boolean mismoMes(int month1, int month2) {
		return month1 == month2;
	}
	
	private boolean esDiaAnteriorOIgual(int day1, int day2) {
		return day1 <= day2;
	}
	
	public boolean menorQue(Persona persona) {
		return this.getEdad() < persona.getEdad();
	}
}
