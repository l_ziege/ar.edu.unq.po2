package tp5.ej4;

public class Mascota extends SerVivo {
	
	private String raza;

	public Mascota(String nombre, String raza) {
		super();
		this.nombre = nombre;
		this.raza = raza;
	}

	public String getRaza() {
		return raza;
	}
}
