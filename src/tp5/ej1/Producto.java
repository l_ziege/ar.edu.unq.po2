package tp5.ej1;

public abstract class Producto {

	protected double precio;
	protected int stock;
	
	public int getStock() {
		return stock;
	}
	
	public double getPrecio() {
		return precio;
	}
	
	public void decrementarStock() {
		if (this.stock > 0) {
			this.stock -= 1;
		}
	}
	
	public abstract double consultarPrecio();
}