package tp5.ej1;

public class CajaDelMercadoCentral {
	
	private double montoAPagar;

	public CajaDelMercadoCentral() {
		super();
		this.montoAPagar = 0;
	}
	
	public void registrarUnProducto(Producto p) {
		this.montoAPagar += p.consultarPrecio();
		p.decrementarStock();
	}
	
	public double informarMontoTotalAPagar() {
		return this.montoAPagar;
	}
}
