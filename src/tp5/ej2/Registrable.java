package tp5.ej2;

public interface Registrable {

	public boolean esFactura();
	
	public double consultarPrecio();
}
