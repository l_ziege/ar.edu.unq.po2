package tp5.ej2;

public abstract class Factura implements Registrable {
	
	public boolean esFactura() {
		return true;
	}

	public abstract double consultarPrecio();
}
