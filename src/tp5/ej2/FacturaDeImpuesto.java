package tp5.ej2;

public class FacturaDeImpuesto extends Factura {

	private double tasaDelServicio;
	
	public FacturaDeImpuesto(double tasaDelServicio) {
		super();
		this.tasaDelServicio = tasaDelServicio;
	}

	@Override
	public double consultarPrecio() {
		return this.tasaDelServicio;
	}
}
