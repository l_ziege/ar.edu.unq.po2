package tp5.ej2;

public class CajaDelMercadoCentral implements Agencia {
	
	private double montoAPagar;

	public CajaDelMercadoCentral() {
		super();
		this.montoAPagar = 0;
	}
	
	public void registrar(Registrable r) {
		this.montoAPagar += r.consultarPrecio();
		if (r.esFactura()) {
			this.registrarPago((Factura) r);
		} else {
			((Producto) r).decrementarStock();
		}
	}
	
	public double informarMontoTotalAPagar() {
		return this.montoAPagar;
	}

	@Override
	public void registrarPago(Factura factura) {
		// Notificar a la agencia recaudadora.
	}
}
