package tp5.ej2;

public class FacturaDeServicio extends Factura {
	
	private double costoPorUnidadConsumida;
	private int cantUnidadesConsumidas;

	public FacturaDeServicio(double costoPorUnidadConsumida, int cantUnidadesConsumidas) {
		super();
		this.costoPorUnidadConsumida = costoPorUnidadConsumida;
		this.cantUnidadesConsumidas = cantUnidadesConsumidas;
	}

	@Override
	public double consultarPrecio() {
		return this.costoPorUnidadConsumida * this.cantUnidadesConsumidas;
	}
}
