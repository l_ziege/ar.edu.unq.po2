package tp5.ej2;

public class ProductoDeEmpresaTradicional extends Producto {
	
	/**
	 * El Stock debe ser un n�mero mayor a 0.
	 */
	
	public ProductoDeEmpresaTradicional(double precio, int stock) {
		super();
		this.precio = precio;
		this.stock = stock;
	}

	@Override
	public double consultarPrecio() {
		if (this.getStock() > 0) {
			return this.getPrecio();
		} else {
			return 0;
		}
	}
}
