package tp5.ej2;

public abstract class Producto implements Registrable {

	protected double precio;
	protected int stock;
	
	public int getStock() {
		return stock;
	}
	
	public double getPrecio() {
		return precio;
	}
	
	public void decrementarStock() {
		if (this.stock > 0) {
			this.stock -= 1;
		}
	}
	
	public boolean esFactura() {
		return false;
	}
	
	public abstract double consultarPrecio();
}