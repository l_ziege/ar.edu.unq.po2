package tp5.ej2;

public class ProductoDeCooperativa extends Producto {
	
	/**
	 * El Stock debe ser un n�mero mayor a 0.
	 */

	public ProductoDeCooperativa(double precio, int stock) {
		super();
		this.precio = precio;
		this.stock = stock;
	}

	@Override
	public double consultarPrecio() {
		if (this.getStock() > 0) {
			return this.getPrecio() * 0.9;
		} else {
			return 0;
		}
	}
}
