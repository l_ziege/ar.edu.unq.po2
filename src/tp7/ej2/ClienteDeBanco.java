package tp7.ej2;

public class ClienteDeBanco extends Trabajador {

	public ClienteDeBanco(String nombre, String apellido, String dirección, int edad, float sueldoNetoMensual) {
		super(nombre, apellido, dirección, edad, sueldoNetoMensual);
	}

	public void solicitarCréditoPersonalAlBancoPorSumaDeterminada(Banco banco, int dinero, int plazoEnMeses) {
		SolicitudDeCréditoPersonal sdc = new SolicitudDeCréditoPersonal(this, dinero, plazoEnMeses);
		banco.registrarSolicitudDeCrédito(sdc);
	}
	
	public void solicitarCréditoHipotecarioAlBancoPorSumaDeterminada(Banco banco, int dinero, int plazoEnMeses, PropiedadInmobiliaria propiedad) {
		SolicitudDeCréditoHipotecario sdc = new SolicitudDeCréditoHipotecario(this, dinero, plazoEnMeses, propiedad);
		banco.registrarSolicitudDeCrédito(sdc);
	}

	public void recibirMonto(int monto) {
		// El cliente recibe el monto solicitado al banco.
	}
}
