package tp7.ej2;

public class SolicitudDeCréditoPersonal extends SolicitudDeCrédito {

	public SolicitudDeCréditoPersonal(ClienteDeBanco clienteDeBanco, int dinero, int plazoEnMeses) {
		super();
		this.cliente = clienteDeBanco;
		this.monto = dinero;
		this.plazoEnMeses = plazoEnMeses;
		this.montoCuotaMensual = dinero / plazoEnMeses;
	}

	@Override
	public boolean chequearSolicitud() {
		return ingresoAnualDelSolicitanteEsDeAlMenos15000() && 
				montoDeLaCuotaNoSupera70PorcientoDelIngresoMensual();
	}

	private boolean ingresoAnualDelSolicitanteEsDeAlMenos15000() {
		return this.getCliente().getSueldoNetoAnual() >= 15000;
	}

	private boolean montoDeLaCuotaNoSupera70PorcientoDelIngresoMensual() {
		return this.getMontoCuotaMensual() <= (this.getCliente().getSueldoNetoMensual() * 0.7);
	}
}
