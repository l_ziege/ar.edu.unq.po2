package tp7.ej2;

import java.util.ArrayList;
import java.util.List;

public class Banco {
	
	private List<ClienteDeBanco> clientes;

	public Banco(List<ClienteDeBanco> clientes) {
		super();
		this.clientes = clientes;
	}

	public Banco() {
		super();
		this.clientes = new ArrayList<ClienteDeBanco>();
	}

	public void agregarCliente(ClienteDeBanco cliente) {
		this.clientes.add(cliente);
	}

	public void registrarSolicitudDeCrédito(SolicitudDeCrédito sdc) {
		if (this.clientes.contains(sdc.getCliente())) {
			this.evaluarSolicitudDeCrédito(sdc);
		}
	}

	private void evaluarSolicitudDeCrédito(SolicitudDeCrédito sdc) {
		if (sdc.chequearSolicitud()) {
			sdc.getCliente().recibirMonto(sdc.getMonto());
		}
	}
}
