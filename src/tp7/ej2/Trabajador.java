package tp7.ej2;

public class Trabajador extends Persona {

	private float sueldoNetoMensual;

	public Trabajador(String nombre, String apellido, String direcci�n, int edad, float sueldoNetoMensual) {
		super(nombre, apellido, direcci�n, edad);
		this.sueldoNetoMensual = sueldoNetoMensual;
	}

	public float getSueldoNetoMensual() {
		return sueldoNetoMensual;
	}
	
	public float getSueldoNetoAnual() {
		return this.sueldoNetoMensual * 12;
	}
}
