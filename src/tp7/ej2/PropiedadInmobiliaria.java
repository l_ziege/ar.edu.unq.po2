package tp7.ej2;

public class PropiedadInmobiliaria {

	private String descripci�n;
	private String direcci�n;
	private int valorFiscal;
	
	public PropiedadInmobiliaria(String descripci�n, String direcci�n, int valorFiscal) {
		super();
		this.descripci�n = descripci�n;
		this.direcci�n = direcci�n;
		this.valorFiscal = valorFiscal;
	}
	
	public String getDescripci�n() {
		return descripci�n;
	}
	
	public String getDirecci�n() {
		return direcci�n;
	}
	
	public int getValorFiscal() {
		return valorFiscal;
	}
}
