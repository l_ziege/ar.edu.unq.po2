package tp7.ej3;

public class SolicitudDeCréditoHipotecario extends SolicitudDeCrédito {

	private PropiedadInmobiliaria propiedad;
	
	public SolicitudDeCréditoHipotecario(ClienteDeBanco clienteDeBanco, int dinero, int plazoEnMeses,
			PropiedadInmobiliaria propiedad) {
		super();
		this.cliente = clienteDeBanco;
		this.monto = dinero;
		this.plazoEnMeses = plazoEnMeses;
		this.montoCuotaMensual = dinero / plazoEnMeses;
		this.propiedad = propiedad;
	}

	public PropiedadInmobiliaria getPropiedad() {
		return propiedad;
	}

	@Override
	public boolean chequearSolicitud() {
		return montoCuotaNoSuperaEl50PorcientoDelIngresoMensual() &&
				montoSolicitadoNoSupera70PorCientoDelValorFiscalDeLaPropiedad() &&
				clienteNoSuperaráLos65AñosAntesDeTerminarDePagarElCredito();
	}

	private boolean montoCuotaNoSuperaEl50PorcientoDelIngresoMensual() {
		return this.getMontoCuotaMensual() <= (this.getCliente().getSueldoNetoMensual() * 0.5);
	}

	private boolean montoSolicitadoNoSupera70PorCientoDelValorFiscalDeLaPropiedad() {
		return this.getMonto() <= (this.propiedad.getValorFiscal() * 0.7);
	}

	private boolean clienteNoSuperaráLos65AñosAntesDeTerminarDePagarElCredito() {
		return (this.getCliente().getEdad() + (this.getPlazoEnMeses() / 12)) < 65;
	}
}
