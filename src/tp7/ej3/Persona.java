package tp7.ej3;

public class Persona {

	private String nombre;
	private String apellido;
	private String direcci�n;
	private int edad;
	
	public Persona(String nombre, String apellido, String direcci�n, int edad) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.direcci�n = direcci�n;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getDirecci�n() {
		return direcci�n;
	}

	public int getEdad() {
		return edad;
	}
}
