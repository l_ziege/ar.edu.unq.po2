package tp7.ej3;

public abstract class SolicitudDeCrédito {
	
	protected ClienteDeBanco cliente;
	protected int monto;
	protected int plazoEnMeses;
	protected float montoCuotaMensual;

	public ClienteDeBanco getCliente() {
		return cliente;
	}

	public int getMonto() {
		return monto;
	}
	
	public int getPlazoEnMeses() {
		return plazoEnMeses;
	}

	public float getMontoCuotaMensual() {
		return montoCuotaMensual;
	}
	
	public abstract boolean chequearSolicitud();
}
