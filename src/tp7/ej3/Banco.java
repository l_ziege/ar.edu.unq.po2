package tp7.ej3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Banco {
	
	private List<ClienteDeBanco> clientes;
	private List<SolicitudDeCrédito> solicitudesDeCrédito;

	public Banco(List<ClienteDeBanco> clientes) {
		super();
		this.clientes = clientes;
		this.solicitudesDeCrédito = new ArrayList<SolicitudDeCrédito>();
	}

	public Banco() {
		super();
		this.clientes = new ArrayList<ClienteDeBanco>();
		this.solicitudesDeCrédito = new ArrayList<SolicitudDeCrédito>();
	}

	public void agregarCliente(ClienteDeBanco cliente) {
		this.clientes.add(cliente);
	}
	
	public void agregarSolicitudDeCrédito(SolicitudDeCrédito sdc) {
		this.solicitudesDeCrédito.add(sdc);
	}

	public void registrarSolicitudDeCrédito(SolicitudDeCrédito sdc) {
		if (this.clientes.contains(sdc.getCliente())) {
			this.evaluarSolicitudDeCrédito(sdc);
		}
	}

	private void evaluarSolicitudDeCrédito(SolicitudDeCrédito sdc) {
		if (sdc.chequearSolicitud()) {
			sdc.getCliente().recibirMonto(sdc.getMonto());
		}
	}
	
	public int montoTotalADesembolsarPorSolicitudesDeCréditoAceptables() {
		List<SolicitudDeCrédito> solicitudesDeCréditoAceptables = this.solicitudesDeCrédito.
				stream().filter(sdc -> sdc.chequearSolicitud()).collect(Collectors.toList());
		int montoTotalADesembolsar = solicitudesDeCréditoAceptables.stream().
				mapToInt(sdc -> sdc.getMonto()).sum();
		return montoTotalADesembolsar;
	}
}
