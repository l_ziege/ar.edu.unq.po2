package tp7.ej1;

public interface IServidor {
	
	public void conectar(String nombreUsuario, String passusuario);
	
	public float tazaDeTransferencia();

	public void resetear();
	
	public void realizarBackUp();

}
