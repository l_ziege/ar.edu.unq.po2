package tp7.ej1;

public interface ClienteDeCorreo extends ClienteGeneral {
	
	public void borrarCorreo(Correo correo);
	
	public int contarBorrados();
	
	public int contarInbox();
	
	public void eliminarBorrado(Correo correo);
	
	public void recibirNuevos();
	
	public void enviarCorreo(String asunto, String destinatario, String cuerpo);
}
