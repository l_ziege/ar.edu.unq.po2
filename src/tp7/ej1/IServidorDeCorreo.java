package tp7.ej1;

import java.util.List;

public interface IServidorDeCorreo extends IServidor {

	public List<Correo> recibirNuevos(String user, String pass);

	public void enviar(Correo correo);
	
}