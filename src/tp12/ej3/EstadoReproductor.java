package tp12.ej3;

// PATRON STATE
// ROL: State

public abstract class EstadoReproductor {

	public abstract void play(ReproductorMP3 mp3);
	
	public abstract void pause(ReproductorMP3 mp3);
	
	public abstract void stop(ReproductorMP3 mp3);
}
