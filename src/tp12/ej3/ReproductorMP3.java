package tp12.ej3;

import java.util.List;

// PATRON STATE
// ROL: Context

public class ReproductorMP3 {
	
	private Pantalla pantalla;
	private List<Song> canciones;
	private EstadoReproductor estado;
	private boolean estáReproduciendo;

	public ReproductorMP3(Pantalla pantalla, List<Song> canciones) {
		super();
		this.pantalla = pantalla;
		this.canciones = canciones;
		this.estado = new SelecciónDeCanciones();
		this.estáReproduciendo = false;
	}

	public void play() {
		this.estado.play(this);
	}
	
	public void pause() {
		this.estado.pause(this);
	}
	
	public void stop() {
		this.estado.stop(this);
	}
	
	public void agregarCanción(Song nuevaCanción) {
		this.canciones.add(nuevaCanción);
	}

	boolean seEstáReproduciendoLaCanciónSeleccionada() {
		return this.estáReproduciendo;
	}

	Song canciónSeleccionada() {
		return this.canciones.get(0);
	}
	
	void pasarASiguienteCanción() {
		this.canciones.add(this.canciónSeleccionada());
		this.canciones.remove(0);
	}

	public void cambiarEstadoA(EstadoReproductor nuevoEstado) {
		this.estado = nuevoEstado;
	}
	
	public Pantalla getPantalla() {
		return this.pantalla;
	}
	
	public EstadoReproductor getEstado() {
		return this.estado;
	}
	
	public void setEstáReproduciendo(boolean nuevoValor) {
		this.estáReproduciendo = nuevoValor;
	}
	
}
