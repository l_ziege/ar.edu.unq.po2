package tp12.ej3;

// PATRON STATE
// ROL: ConcreteStateB

public class Reproduciendo extends EstadoReproductor {

	@Override
	public void play(ReproductorMP3 mp3) {
		mp3.getPantalla().mostrar("Error! El reproductor no está en modo de selección de canciones");
	}

	@Override
	public void pause(ReproductorMP3 mp3) {
		if (mp3.seEstáReproduciendoLaCanciónSeleccionada()) {
			mp3.canciónSeleccionada().pause();
			mp3.setEstáReproduciendo(false);
		} else {
			mp3.canciónSeleccionada().play();
			mp3.setEstáReproduciendo(true);
		}
	}

	@Override
	public void stop(ReproductorMP3 mp3) {
		mp3.canciónSeleccionada().stop();
		mp3.setEstáReproduciendo(false);
		mp3.pasarASiguienteCanción();
		mp3.cambiarEstadoA(new SelecciónDeCanciones());
	}
	
	@Override
	public String toString() {
		return "Estado del reproductor mp3: Reproduciendo";
	}

}
