package tp12.ej3;

// PATRON STATE
// ROL: ConcreteStateA

public class SelecciónDeCanciones extends EstadoReproductor {

	@Override
	public void play(ReproductorMP3 mp3) {
		mp3.canciónSeleccionada().play();
		mp3.setEstáReproduciendo(true);
		mp3.cambiarEstadoA(new Reproduciendo());
	}

	@Override
	public void pause(ReproductorMP3 mp3) {
		mp3.getPantalla().mostrar("Error! El reproductor no tiene una canción reproduciendo o en pausa");
	}

	@Override
	public void stop(ReproductorMP3 mp3) {
		// No hay canción que parar
	}
	
	@Override
	public String toString() {
		return "Estado del reproductor mp3: Selección de canciones";
	}

}
