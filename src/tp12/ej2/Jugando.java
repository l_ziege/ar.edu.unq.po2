package tp12.ej2;

// PATRON STATE
// ROL: ConcreteStateC

public class Jugando extends EstadoMaquina {

	private int cantJugadores;
	
	public Jugando(int jugadores) {
		super();
		this.cantJugadores = jugadores;
	}

	@Override
	public void presionarBotónDeInicio(MaquinaDeVideojuegos maquina) {
		// Se esperaba terminar el juego.
		// Vuelve al estado inicial.
		this.volverAEstadoInicial(maquina);
	}
	
	private void volverAEstadoInicial(MaquinaDeVideojuegos maquina) {
		maquina.setFichasIngresadas(0);
		maquina.cambiarEstadoA(new Inicial());
	}

	@Override
	public void introducirFicha(MaquinaDeVideojuegos maquina) {
		// La ranura está cerrada, es una operación imposible de realizar.
	}

	@Override
	public void terminarElJuego(MaquinaDeVideojuegos maquina) {
		this.volverAEstadoInicial(maquina);
	}
	
	@Override
	public String toString() {
		return "Estado de la maquina: Jugando con " + this.cantJugadores + " jugador/es";
	}

}
