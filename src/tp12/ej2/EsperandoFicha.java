package tp12.ej2;

// PATRON STATE
// ROL: ConcreteStateB

public class EsperandoFicha extends EstadoMaquina {

	@Override
	public void presionarBotónDeInicio(MaquinaDeVideojuegos maquina) {
		if (! maquina.hayCantidadDeFichasIngresadasNecesarias()) {
			maquina.getPantalla().mostrar("Ingresó una cantidad de fichas invalida (o no ingresó ninguna)");
			maquina.getRanura().cerrar();
			maquina.setFichasIngresadas(0);
			maquina.cambiarEstadoA(new Inicial());
		} else {
			maquina.getRanura().cerrar();
			this.comenzarJuego(maquina);
		}
	}

	private void comenzarJuego(MaquinaDeVideojuegos maquina) {
		if (maquina.hayUnaFichaIngresada()) {
			maquina.cambiarEstadoA(new Jugando(1));
		} else {
			maquina.cambiarEstadoA(new Jugando(2));
		}
	}

	@Override
	public void introducirFicha(MaquinaDeVideojuegos maquina) {
		maquina.ingresarFicha();
	}

	@Override
	public void terminarElJuego(MaquinaDeVideojuegos maquina) {
		// Aun no se comenzó ningún juego en la maquina
	}

	@Override
	public String toString() {
		return "Estado de la maquina: Esperando ficha";
	}
	
}
