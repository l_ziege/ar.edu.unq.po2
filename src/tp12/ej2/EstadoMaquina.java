package tp12.ej2;

// PATRON STATE
// ROL: State

public abstract class EstadoMaquina {

	public abstract void presionarBotónDeInicio(MaquinaDeVideojuegos maquina);

	public abstract void introducirFicha(MaquinaDeVideojuegos maquina);

	public abstract void terminarElJuego(MaquinaDeVideojuegos maquina);

}
