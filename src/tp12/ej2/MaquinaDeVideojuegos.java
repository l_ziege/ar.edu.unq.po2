package tp12.ej2;

// PATRON STATE
// ROL: Context

public class MaquinaDeVideojuegos {

	private Control control1;
	private Control control2;
	private Ranura ranura;
	private Pantalla pantalla;
	private int fichasIngresadas;
	private EstadoMaquina estado;
	
	public MaquinaDeVideojuegos(Control control1, Control control2, Ranura ranura, Pantalla pantalla) {
		super();
		this.control1 = control1;
		this.control2 = control2;
		this.ranura = ranura;
		this.pantalla = pantalla;
		this.fichasIngresadas = 0;
		this.estado = new Inicial();
	}
	
	public void presionarBotónDeInicio() {
		this.estado.presionarBotónDeInicio(this);
	}

	public void introducirFicha() {
		this.estado.introducirFicha(this);
	}
	
	public void terminarElJuego() {
		this.estado.terminarElJuego(this);
	}
	
	void ingresarFicha() {
		this.fichasIngresadas += 1;
	}

	public boolean hayCantidadDeFichasIngresadasNecesarias() {
		return this.fichasIngresadas == 1 || this.fichasIngresadas == 2;
	}

	public boolean hayUnaFichaIngresada() {
		return this.fichasIngresadas == 1;
	}
	
	public Pantalla getPantalla() {
		return this.pantalla;
	}

	public Ranura getRanura() {
		return this.ranura;
	}
	
	public EstadoMaquina getEstado() {
		return this.estado;
	}

	void cambiarEstadoA(EstadoMaquina nuevoEstado) {
		this.estado = nuevoEstado;
	}

	void setFichasIngresadas(int fichasIngresadas) {
		this.fichasIngresadas = fichasIngresadas;
	}
	
}
