package tp12.ej2;

// PATRON STATE
// ROL: ConcreteStateA

public class Inicial extends EstadoMaquina {

	@Override
	public void presionarBotónDeInicio(MaquinaDeVideojuegos maquina) {
		maquina.getPantalla().mostrar("Ingrese fichas para jugar");
		maquina.getRanura().abrir();
		maquina.cambiarEstadoA(new EsperandoFicha());
	}

	@Override
	public void introducirFicha(MaquinaDeVideojuegos maquina) {
		// La maquina aun no estaba encendida, no se le puede introducir fichas
	}

	@Override
	public void terminarElJuego(MaquinaDeVideojuegos maquina) {
		// Aun no se comenzó ningún juego en la maquina
	}
	
	@Override
	public String toString() {
		return "Estado de la maquina: Inicial";
	}

}
