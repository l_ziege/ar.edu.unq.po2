package tp12.ej1;

// PATRON STRATEGY
// ROL: Strategy

public interface EstrategiaEncriptado {

	public String encriptar(String texto);
	
	public String desencriptar(String texto);
}
