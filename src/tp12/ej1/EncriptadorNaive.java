package tp12.ej1;

// PATRON STRATEGY
// ROL: Context

public class EncriptadorNaive {
	
	private EstrategiaEncriptado estrategia;
	
	public EncriptadorNaive(EstrategiaEncriptado estrategia) {
		super();
		this.estrategia = estrategia;
	}

	public String encriptar(String texto) {
		return estrategia.encriptar(texto);
	}
	
	public String desencriptar(String texto) {
		return estrategia.desencriptar(texto);
	}
}
