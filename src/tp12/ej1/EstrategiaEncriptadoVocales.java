package tp12.ej1;

import java.util.Arrays;
import java.util.List;

// PATRON STRATEGY
// ROL: ConcreteStrategyA

public class EstrategiaEncriptadoVocales implements EstrategiaEncriptado {

	@Override
	public String encriptar(String texto) {
		String resultado = "";
		
		for(int i = 0; i < texto.length(); i++) {
			resultado += this.encriptarLetra(texto.substring(i, i+1));
		}
		
		return resultado;
	}

	private String encriptarLetra(String letra) {
		if(this.esVocal(letra)) {
			return this.siguienteVocal(letra);
		} else {
			return letra;
		}
	}

	private String siguienteVocal(String letra) {
		List<String> vocales = Arrays.asList("a","e","i","o","u");
		if (letra.equals("u")) {
			return "a";
		} else {
			return vocales.get(vocales.indexOf(letra) + 1);
		}
	}

	@Override
	public String desencriptar(String texto) {
		String resultado = "";
		
		for(int i = 0; i < texto.length(); i++) {
			resultado += this.desencriptarLetra(texto.substring(i, i+1));
		}
		
		return resultado;
	}
	
	private String desencriptarLetra(String letra) {
		if(this.esVocal(letra)) {
			return this.previoVocal(letra);
		} else {
			return letra;
		}
	}

	private String previoVocal(String letra) {
		List<String> vocales = Arrays.asList("a","e","i","o","u");
		if (letra.equals("a")) {
			return "u";
		} else {
			return vocales.get(vocales.indexOf(letra) - 1);
		}
	}

	private boolean esVocal(String letra) {
		return ((letra.equals("a"))
				||
				(letra.equals("e"))
				||
				(letra.equals("i"))
				||
				(letra.equals("o"))
				||
				(letra.equals("u")));
	}

}
