package tp12.ej1;

import java.util.Arrays;
import java.util.List;

// PATRON STRATEGY
// ROL: ConcreteStrategyB

public class EstrategiaEncriptadoNúmeros implements EstrategiaEncriptado {

	@Override
	public String encriptar(String texto) {
		String resultado = "";
		
		for(int i = 0; i < texto.length() - 1; i++) {
			resultado += this.encriptarLetra(texto.substring(i, i+1)) + ",";
		}
		
		return resultado += this.encriptarLetra(texto.substring(texto.length()-1));
	}

	private int encriptarLetra(String letra) {
		List<String> letras = Arrays.asList(" ","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
		return letras.indexOf(letra);
	}

	@Override
	public String desencriptar(String texto) {
		String resultado = "";
		String text = texto;
		while(text.contains(",")) {
			resultado += this.desencriptarLetra(text.substring(0, text.indexOf(",")));
			text = text.substring(text.indexOf(",") + 1);
		}
		return resultado += this.desencriptarLetra(text);
	}

	private String desencriptarLetra(String letra) {
		List<String> números = Arrays.asList("0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26");
		List<String> letras = Arrays.asList(" ","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
		return letras.get(números.indexOf(letra));
	}

}
