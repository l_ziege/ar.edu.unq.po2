package tp3.ej11;

import java.util.ArrayList;
import java.util.List;

public class EquipoDeTrabajo {

	private String nombre;
	private List<Persona> equipo;
	
	public EquipoDeTrabajo(String nombre) {
		super();
		this.nombre = nombre;
		this.equipo = new ArrayList<Persona>();
	}

	public String getNombre() {
		return nombre;
	}
	
	public int promedioEdadDelEquipo() {
		return this.edadTotalDelEquipo() / this.equipo.size();
	}

	private int edadTotalDelEquipo() {
		int edadTotal = 0;
		for (Persona p: equipo) {
			edadTotal += p.getEdad();
		}
		return edadTotal;
	}

	public void agregarAlEquipo(Persona p) {
		this.equipo.add(p);
	}
}
