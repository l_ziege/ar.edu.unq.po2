package tp3.ej2;

public class DesarmadorDeNumeros {

	public int elDeMayorCantDígitosPares(int[] númerosEnteros) {
		// TODOS LOS NÚMEROS DEL ARREGLO DEBEN TENER 5 DÍGITOS.
		// EL ARREGLO DEBE TENER AL MENOS 1 NÚMERO

		int numConMaxDigPar = númerosEnteros[0];

		for (int x = 1; x < númerosEnteros.length; x++) {
			numConMaxDigPar = elDeMayorCantDigitosPares(numConMaxDigPar, númerosEnteros[x]);
		}

		return numConMaxDigPar;
	}

	private int elDeMayorCantDigitosPares(int n1, int n2) {
		if (cantidadDeDígitosPares(n1) >= cantidadDeDígitosPares(n2)) {
			return n1;
		} else {
			return n2;
		}
	}

	private int cantidadDeDígitosPares(int n) {
		int cantDigPar = 0;
		cantDigPar += sumarSiEsPar(primerDígito(n));
		cantDigPar += sumarSiEsPar(segundoDígito(n));
		cantDigPar += sumarSiEsPar(tercerDígito(n));
		cantDigPar += sumarSiEsPar(cuartoDígito(n));
		cantDigPar += sumarSiEsPar(quintoDígito(n));
		return cantDigPar;
	}

	private int sumarSiEsPar(int n) {
		if ((n % 2) == 0) {
			return 1;
		} else {
			return 0;
		}
	}

	private int primerDígito(int n) {
		return (n / 10000);
	}

	private int segundoDígito(int n) {
		return ((n / 1000) % 10);
	}

	private int tercerDígito(int n) {
		return ((n / 100) % 10);
	}

	private int cuartoDígito(int n) {
		return ((n / 10) % 10);
	}

	private int quintoDígito(int n) {
		return (n % 10);
	}
}
