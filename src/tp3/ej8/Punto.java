package tp3.ej8;

public class Punto {

	private int x;
	private int y;
	
	public Punto(int posX, int posY) {
		super();
		this.x = posX;
		this.y = posY;
	}
	
	public Punto() {
		super();
		this.x = 0;
		this.y = 0;
	}
	
	public void moverPunto(int posX, int posY) {
		this.x = posX;
		this.y = posY;
	}
	
	public Punto sumarPuntos(Punto p2) {
		Punto pNuevo = new Punto(this.x + p2.getX(), this.y + p2.getY());
		return pNuevo;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
