package tp3.ej9;

import tp3.ej8.Punto;

public class Cuadrado {
	
	private Punto p1;
	private Punto p2;
	private Punto p3;
	private Punto p4;

	public Cuadrado(Punto puntoOrigen, int lado) {
		super();
		this.p1 = puntoOrigen;
		this.p2 = new Punto(puntoOrigen.getX() + lado, puntoOrigen.getY());
		this.p3 = new Punto(puntoOrigen.getX() + lado, puntoOrigen.getY() + lado);
		this.p4 = new Punto(puntoOrigen.getX(), puntoOrigen.getY() + lado);
	}
	
	public int �rea() {
		int lado = this.p2.getX() - this.p1.getX();
		return lado * lado;
	}
	
	public int per�metro() {
		int lado = this.p2.getX() - this.p1.getX();
		return lado * 4;
	}

	public Punto getP1() {
		return p1;
	}

	public Punto getP2() {
		return p2;
	}

	public Punto getP3() {
		return p3;
	}

	public Punto getP4() {
		return p4;
	}
}
