package tp3.ej9;

import tp3.ej8.Punto;

public class Rect�ngulo {
	
	private Punto p1;
	private Punto p2;
	private Punto p3;
	private Punto p4;

	public Rect�ngulo(Punto puntoOrigen, int ancho, int alto) {
		super();
		this.p1 = puntoOrigen;
		this.p2 = new Punto(puntoOrigen.getX() + ancho, puntoOrigen.getY());
		this.p3 = new Punto(puntoOrigen.getX() + ancho, puntoOrigen.getY() + alto);
		this.p4 = new Punto(puntoOrigen.getX(), puntoOrigen.getY() + alto);
	}
	
	public int �rea() {
		int b = this.p2.getX() - this.p1.getX();
		int h = this.p3.getY() - this.p2.getY();
		return b * h;
	}
	
	public int per�metro() {
		int b = this.p2.getX() - this.p1.getX();
		int h = this.p3.getY() - this.p2.getY();
		return (b * 2) + (h * 2);
	}
	
	public boolean esHorizontal() {
		int b = this.p2.getX() - this.p1.getX();
		int h = this.p3.getY() - this.p2.getY();
		return b > h;
	}
	
	public boolean esVertical() {
		int b = this.p2.getX() - this.p1.getX();
		int h = this.p3.getY() - this.p2.getY();
		return h > b;
	}

	public Punto getP1() {
		return p1;
	}

	public Punto getP2() {
		return p2;
	}

	public Punto getP3() {
		return p3;
	}

	public Punto getP4() {
		return p4;
	}
}
