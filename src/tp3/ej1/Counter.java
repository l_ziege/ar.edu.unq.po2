package tp3.ej1;

import java.util.ArrayList;
import java.util.List;

public class Counter {

	private List<Integer> numeros;

	public Counter(List<Integer> numeros) {
		super();
		this.numeros = numeros;
	}
	
	public Counter() {
		super();
		this.numeros = new ArrayList<Integer>();
	}

	public void addNumber(int n) {
		numeros.add(n);
	}

	public int getEvenOcurrences() {
		int evenOcurrences = 0;
		
		for (int num:this.numeros) {
			evenOcurrences += sumaUnoSiEsPar(num);
		}
		
		return evenOcurrences;
	}

	private int sumaUnoSiEsPar(int num) {
		if ((num % 2) == 0) {
			return 1;
		} else {
			return 0;
		}
	}

	public int getOddOcurrences() {
		int oddOcurrences = 0;
		
		for (int num:this.numeros) {
			oddOcurrences += sumaUnoSiEsImpar(num);
		}
		
		return oddOcurrences;
	}
	
	private int sumaUnoSiEsImpar(int num) {
		if ((num % 2) != 0) {
			return 1;
		} else {
			return 0;
		}
	}

	public int getCantMultiplosDe(int n) {
		int cantMultiplosDeN = 0;
		
		for (int num:this.numeros) {
			cantMultiplosDeN += sumaUnoSiEsMultiplo(num, n);
		}
		
		return cantMultiplosDeN;
	}

	private int sumaUnoSiEsMultiplo(int num, int n) {
		if ((num % n) == 0) {
			return 1;
		} else {
			return 0;
		}
	}
}
