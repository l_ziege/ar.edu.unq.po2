package tp3.ej6;

import java.util.List;

public class Multioperador {

	public int sumarEnteros(List<Integer> enteros) {
		int suma = 0;
		
		for (Integer entero:enteros) {
			suma += entero;
		}
		
		return suma;
	}
	
	public int restarEnteros(List<Integer> enteros) {
		int resta = 0;
		
		for (Integer entero:enteros) {
			resta -= entero;
		}
		
		return resta;
	}
	
	public int multiplicarEnteros(List<Integer> enteros) {
		int multiplicacion = 1;
		
		for (Integer entero:enteros) {
			multiplicacion *= entero;
		}
		
		return multiplicacion;
	}
}
