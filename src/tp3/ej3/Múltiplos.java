package tp3.ej3;

public class Múltiplos {

	public int elMultiploEntre0Y1000MasAltoEntre(int n1, int n2) {
		int elMultiploMasAlto = -1;
		
		for (int x=1 ; x<=1000 ; x++) {
			elMultiploMasAlto = xSiEsMúltiploSimultaneoDe(elMultiploMasAlto, x, n1, n2);
		}
		
		return elMultiploMasAlto;
	}

	private int xSiEsMúltiploSimultaneoDe(int a, int x, int n1, int n2) {
		if (esMúltiploDe(x, n1) && esMúltiploDe(x, n2)) {
			return x;
		} else {
			return a;
		}
	}

	private boolean esMúltiploDe(int x, int n) {
		return ((x % n) == 0);
	}
}
