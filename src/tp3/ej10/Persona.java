package tp3.ej10;

import java.util.Date;

public class Persona {
	
	private String nombre;
	private Date fechaNac;
	private int edad;
	
	public Persona(String nombre, Date fechaDeNacimiento) {
		super();
		this.nombre = nombre;
		this.fechaNac = fechaDeNacimiento;
		this.edad = this.calcularEdad(fechaDeNacimiento);
	}

	private int calcularEdad(Date fechaDeNacimiento) {
		Date diaDeHoy = new Date(2021, 8, 11);
		int edad = diaDeHoy.getYear() - fechaDeNacimiento.getYear();
		if ( (esAnteriorMes(fechaDeNacimiento.getMonth(), diaDeHoy.getMonth())) || 
				((mismoMes(fechaDeNacimiento.getMonth(), diaDeHoy.getMonth())) && 
						esDiaAnteriorOIgual(fechaDeNacimiento.getDay(), diaDeHoy.getDay()))) {
			return edad;
		} else {
			return edad - 1;
		}
	}
	
	private boolean esAnteriorMes(int month1, int month2) {
		return month1 < month2;
	}
	
	private boolean mismoMes(int month1, int month2) {
		return month1 == month2;
	}
	
	private boolean esDiaAnteriorOIgual(int day1, int day2) {
		return day1 <= day2;
	}

	public boolean menorQue(Persona persona) {
		return this.edad < persona.getEdad();
	}

	public String getNombre() {
		return nombre;
	}

	public Date getFechaNac() {
		return fechaNac;
	}

	public int getEdad() {
		return edad;
	}
}
