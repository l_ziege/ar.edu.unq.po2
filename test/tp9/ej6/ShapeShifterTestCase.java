package tp9.ej6;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ShapeShifterTestCase {
	
	private ShapeShifterHoja elemento1;
	private ShapeShifterHoja elemento2;
	private ShapeShifterHoja elemento3;
	private ShapeShifterHoja elemento5;
	private ShapeShifterHoja elemento6;
	private ShapeShifter ssc;
	private ShapeShifter ssd;
	private ShapeShifter sse;
	private ShapeShifter ssf;
	private ShapeShifter ssg;

	@BeforeEach
	public void setUp() throws Exception {
		elemento1 = new ShapeShifterHoja(1);
		elemento2 = new ShapeShifterHoja(2);
		elemento3 = new ShapeShifterHoja(3);
		elemento5 = new ShapeShifterHoja(5);
		elemento6 = new ShapeShifterHoja(6);
		ssc = new ShapeShifter(Arrays.asList(elemento1, elemento2));
		ssd = new ShapeShifter(Arrays.asList(elemento3, ssc));
		sse = new ShapeShifter(Arrays.asList(elemento5, elemento6));
		ssf = new ShapeShifter(Arrays.asList(ssd, sse));
		ssg = new ShapeShifter(Arrays.asList(elemento3, elemento1, elemento2, elemento5, elemento6));
	}

	@Test
	public void testLaProfundidadMáximaDelElemento1Es0() {
		assertEquals(0, elemento1.deepest());
	}
	
	@Test
	public void testLaProfundidadMáximaDelShapeShifterCEs1() {
		assertEquals(1, ssc.deepest());
	}
	
	@Test
	public void testLaProfundidadMáximaDelShapeShifterFEs3() {
		assertEquals(3, ssf.deepest());
	}
	
	@Test
	public void testLosValoresDelElemento1SonEl1() {
		assertEquals(Arrays.asList(1), elemento1.values());
	}
	
	@Test
	public void testLosValoresDelShapeShifterDSonEl3El1yEl2() {
		assertEquals(Arrays.asList(3,1,2), ssd.values());
	}
	
	@Test
	public void testElElemento1CompuestoConElElemento2EsIgualAlShapeShifterC() {
		assertEquals(ssc.toString(), elemento1.compose(elemento2).toString());
	}
	
	@Test
	public void testElElemento3CompuestoConElShapeShifterCEsIgualAlShapeShifterD() {
		assertEquals(ssd.toString(), elemento3.compose(ssc).toString());
	}
	
	@Test
	public void testElShapeShifterDCompuestoConElShapeShifterEEsIgualAlShapeShifterF() {
		assertEquals(ssf.toString(), ssd.compose(sse).toString());
	}
	
	@Test
	public void testElElemento1AchatadoEsIgualAlElemento1() {
		assertEquals(elemento1, elemento1.flat());
	}
	
	@Test
	public void testElShapeShifterFAchatadoEsIgualAlShapeShifterG() {
		assertEquals(ssg.toString(), ssf.flat().toString());
	}

}
