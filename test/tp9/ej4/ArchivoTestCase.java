package tp9.ej4;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ArchivoTestCase {
	
	private Archivo txt;
	private Date fechaCreacionTxt;

	@BeforeEach
	public void setUp() throws Exception {
		fechaCreacionTxt = new Date(2021, 8, 20);
		txt = new Archivo("nuevo.txt", 500, fechaCreacionTxt);
	}

	@Test
	public void testElNombreDelArchivoTxtEsNuevoTxt() {
		assertEquals("nuevo.txt", txt.getNombre());
	}
	
	@Test
	public void testElTamañoDelArchivoTxtEs500() {
		assertEquals(500, txt.totalSize());
	}
	
	@Test
	public void testLaUltimaFechaDeModificaciónDelArchivoTxtEs20DeSeptiembreDe2021() {
		assertEquals(fechaCreacionTxt, txt.getFecha());
	}

}
