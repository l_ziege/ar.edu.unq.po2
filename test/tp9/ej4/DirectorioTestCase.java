package tp9.ej4;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DirectorioTestCase {
	
	private Directorio carpeta;
	private Archivo pdf;
	private Directorio obj;
	private Archivo tarea;
	private Archivo resumen;

	@BeforeEach
	public void setUp() throws Exception {
		carpeta = new Directorio("carpeta", new Date(2021, 5, 15));
		pdf = new Archivo("tp.pdf", 1200, new Date(2021, 5, 18));
		obj = new Directorio("objetos2", new Date(2021, 7, 25));
		tarea = new Archivo("tarea.docx", 2500, new Date(2021, 6, 5));
		resumen = new Archivo("resumen.txt", 300, new Date(2021, 4, 20));
	}

	@Test
	public void testUnaCarpetaRecienCreadaTieneTamaño0() {
		assertEquals(0, carpeta.totalSize());
	}
	
	@Test
	public void testUnaCarpetaConUnArchivoPdfTieneUnTamañoDe1200() {
		carpeta.agregarElementoFS(pdf);
		assertEquals(1200, carpeta.totalSize());
	}
	
	@Test
	public void testElElementoMasAntiguoEnUnaCarpetaConUnPdfYUnTxtEsElArchivoTxt() {
		carpeta.agregarElementoFS(pdf);
		carpeta.agregarElementoFS(resumen);
		assertEquals(resumen, carpeta.oldestElement());
	}
	
	@Test
	public void testElElementoMasNuevoEnUnaCarpetaObjConUnDocxEsLaCarpeta() {
		obj.agregarElementoFS(tarea);
		assertEquals(obj, obj.lastModified());
	}
	
	public static void main(String[] args) {
		Directorio carpeta = new Directorio("carpeta", new Date(2021, 5, 15));
		Archivo pdf = new Archivo("tp.pdf", 1200, new Date(2021, 5, 18));
		Directorio obj = new Directorio("objetos2", new Date(2021, 7, 25));
		Archivo tarea = new Archivo("tarea.docx", 2500, new Date(2021, 6, 5));
		Archivo resumen = new Archivo("resumen.txt", 300, new Date(2021, 4, 20));
		obj.agregarElementoFS(tarea);
		obj.agregarElementoFS(pdf);
		carpeta.agregarElementoFS(resumen);
		carpeta.agregarElementoFS(obj);
		carpeta.printStructure();
		obj.printStructure();
	}

}
