package tp8.ej3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PokerStatusTestCase {
	
	private PokerStatus poker;
	private Carta cuatroC;
	private Carta sieteC;
	private Carta ochoC;
	private Carta doceC;
	private Carta unoC;
	private Carta ochoT;
	private Carta ochoD;
	private Carta ochoP;

	@BeforeEach
	public void setUp() throws Exception {
		poker = new PokerStatus();
		cuatroC = new Carta(4, "Corazones");
		sieteC = new Carta(7, "Corazones");
		ochoC = new Carta(8, "Corazones");
		doceC = new Carta(12, "Corazones");
		unoC = new Carta(1, "Corazones");
		ochoT = new Carta(8, "Tr�boles");
		ochoD = new Carta(8, "Diamantes");
		ochoP = new Carta(8, "Picas");
	}

	@Test
	public void testElJugadorTienePoquer() {
		assertEquals("Poquer", poker.verificar(ochoT, ochoD, ochoC, cuatroC, ochoP));
	}
	
	@Test
	public void testElJugadorTieneColor() {
		assertEquals("Color", poker.verificar(unoC, sieteC, ochoC, cuatroC, doceC));
	}
	
	@Test
	public void testElJugadorTieneTrio() {
		assertEquals("Trio", poker.verificar(ochoD, sieteC, ochoP, cuatroC, ochoT));
	}
	
	@Test
	public void testElJugadorNoTieneNingunaJugada() {
		assertEquals("Nada", poker.verificar(ochoD, ochoP, doceC, unoC, sieteC));
	}

}
