package tp8.ej3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CartaTestCase {
	
	private Carta carta1;
	private Carta carta2;
	private Carta carta3;

	@BeforeEach
	public void setUp() throws Exception {
		carta1 = new Carta(4, "Picas");
		carta2 = new Carta(8, "Tréboles");
		carta3 = new Carta(5, "Tréboles");
	}

	@Test
	public void testElValorDeLaCarta4DePicasEs4() {
		assertEquals(4, carta1.getValor());
	}
	
	@Test
	public void testElPaloDeLaCarta4DePicasEsPicas() {
		assertEquals("Picas", carta1.getPalo());
	}
	
	@Test
	public void testElValorDeLaCarta8DeTrébolesEsSuperiorAlDe4DePicas() {
		assertTrue(carta2.esSuperiorA(carta1));
	}
	
	@Test
	public void testElValorDeLaCarta4DePicasNoEsSuperiorAlDe5DeTréboles() {
		assertFalse(carta1.esSuperiorA(carta3));
	}
	
	@Test
	public void testElPaloDeLaCarta8DeTrébolesEsElMismoQueElDe5DeTréboles() {
		assertTrue(carta2.esMismoPaloQue(carta3));
	}

	@Test
	public void testElPaloDeLaCarta4DePicasNoEsElMismoQueElDe8DeTréboles() {
		assertFalse(carta1.esMismoPaloQue(carta2));
	}
}
