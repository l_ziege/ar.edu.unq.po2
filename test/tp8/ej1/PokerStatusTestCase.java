package tp8.ej1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PokerStatusTestCase {
	
	private PokerStatus poker;

	@BeforeEach
	public void setUp() throws Exception {
		poker = new PokerStatus();
	}

	@Test
	public void testElJugadorTienePoker() {
		assertTrue(poker.verificar("3P", "3C", "8T", "3D", "3T"));
	}
	
	@Test
	public void testElJugadorNoTienePoker() {
		assertFalse(poker.verificar("8T", "5C", "JD", "10T", "KP"));
	}

}
