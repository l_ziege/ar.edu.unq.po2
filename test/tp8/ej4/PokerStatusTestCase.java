package tp8.ej4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import tp8.ej3.Carta;
import tp8.ej3.PokerStatus;

public class PokerStatusTestCase {

	private PokerStatus poker;

	@BeforeEach
	public void setUp() throws Exception {
		poker = new PokerStatus();
	}

	@Test
	public void testElJugadorTienePoquer() {
		Carta ochoT = Mockito.mock(Carta.class);
		Mockito.when(ochoT.getValor()).thenReturn(8);
		
		Carta ochoD = Mockito.mock(Carta.class);
		Mockito.when(ochoD.getValor()).thenReturn(8);
		
		Carta ochoC = Mockito.mock(Carta.class);
		Mockito.when(ochoC.getValor()).thenReturn(8);
		
		Carta cuatroC = Mockito.mock(Carta.class);
		Mockito.when(cuatroC.getValor()).thenReturn(4);
		
		Carta ochoP = Mockito.mock(Carta.class);
		Mockito.when(ochoP.getValor()).thenReturn(8);
		
		assertEquals("Poquer", poker.verificar(ochoT, ochoD, ochoC, cuatroC, ochoP));
		
		Mockito.verify(ochoT, Mockito.times(2)).getValor();
		Mockito.verify(ochoD, Mockito.times(2)).getValor();
		Mockito.verify(ochoC, Mockito.times(2)).getValor();
		Mockito.verify(cuatroC, Mockito.times(1)).getValor();
		Mockito.verify(ochoP, Mockito.times(1)).getValor();
	}
	
	@Test
	public void testElJugadorTieneColor() {
		Carta unoC = Mockito.mock(Carta.class);
		Mockito.when(unoC.getValor()).thenReturn(1);
		
		Carta sieteC = Mockito.mock(Carta.class);
		Mockito.when(sieteC.getValor()).thenReturn(7);
		
		Carta ochoC = Mockito.mock(Carta.class);
		Mockito.when(ochoC.getValor()).thenReturn(8);
		
		Carta cuatroC = Mockito.mock(Carta.class);
		Mockito.when(cuatroC.getValor()).thenReturn(4);
		
		Carta doceC = Mockito.mock(Carta.class);
		Mockito.when(doceC.getValor()).thenReturn(12);
		
		Mockito.when(unoC.esMismoPaloQue(sieteC)).thenReturn(true);
		Mockito.when(sieteC.esMismoPaloQue(ochoC)).thenReturn(true);
		Mockito.when(ochoC.esMismoPaloQue(cuatroC)).thenReturn(true);
		Mockito.when(cuatroC.esMismoPaloQue(doceC)).thenReturn(true);
		
		assertEquals("Color", poker.verificar(unoC, sieteC, ochoC, cuatroC, doceC));
		
		Mockito.verify(unoC, Mockito.times(4)).getValor();
		Mockito.verify(sieteC, Mockito.times(4)).getValor();
		Mockito.verify(ochoC, Mockito.times(4)).getValor();
		Mockito.verify(cuatroC, Mockito.times(4)).getValor();
		Mockito.verify(doceC, Mockito.times(4)).getValor();
		
		Mockito.verify(unoC, Mockito.times(1)).esMismoPaloQue(sieteC);
		Mockito.verify(sieteC, Mockito.times(1)).esMismoPaloQue(ochoC);
		Mockito.verify(ochoC, Mockito.times(1)).esMismoPaloQue(cuatroC);
		Mockito.verify(cuatroC, Mockito.times(1)).esMismoPaloQue(doceC);
		Mockito.verify(doceC, Mockito.never()).esMismoPaloQue(unoC);
	}
	
	@Test
	public void testElJugadorTieneTrio() {
		Carta ochoD = Mockito.mock(Carta.class);
		Mockito.when(ochoD.getValor()).thenReturn(8);
		
		Carta sieteC = Mockito.mock(Carta.class);
		Mockito.when(sieteC.getValor()).thenReturn(7);
		
		Carta ochoP = Mockito.mock(Carta.class);
		Mockito.when(ochoP.getValor()).thenReturn(8);
		
		Carta cuatroC = Mockito.mock(Carta.class);
		Mockito.when(cuatroC.getValor()).thenReturn(4);
		
		Carta ochoT = Mockito.mock(Carta.class);
		Mockito.when(ochoT.getValor()).thenReturn(8);
		
		Mockito.when(ochoD.esMismoPaloQue(sieteC)).thenReturn(false);
		Mockito.when(sieteC.esMismoPaloQue(ochoP)).thenReturn(false);
		Mockito.when(ochoP.esMismoPaloQue(cuatroC)).thenReturn(false);
		Mockito.when(cuatroC.esMismoPaloQue(ochoT)).thenReturn(false);
		
		assertEquals("Trio", poker.verificar(ochoD, sieteC, ochoP, cuatroC, ochoT));
		
		Mockito.verify(ochoD, Mockito.times(9)).getValor();
		Mockito.verify(sieteC, Mockito.times(7)).getValor();
		Mockito.verify(ochoP, Mockito.times(7)).getValor();
		Mockito.verify(cuatroC, Mockito.times(6)).getValor();
		Mockito.verify(ochoT, Mockito.times(6)).getValor();
		
		Mockito.verify(ochoD, Mockito.times(1)).esMismoPaloQue(sieteC);
		Mockito.verify(sieteC, Mockito.never()).esMismoPaloQue(ochoP);
		Mockito.verify(ochoP, Mockito.never()).esMismoPaloQue(cuatroC);
		Mockito.verify(cuatroC, Mockito.never()).esMismoPaloQue(ochoT);
		Mockito.verify(ochoT, Mockito.never()).esMismoPaloQue(ochoD);
	}
	
	@Test
	public void testElJugadorNoTieneNingunaJugada() {
		Carta ochoD = Mockito.mock(Carta.class);
		Mockito.when(ochoD.getValor()).thenReturn(8);
		
		Carta ochoP = Mockito.mock(Carta.class);
		Mockito.when(ochoP.getValor()).thenReturn(8);
		
		Carta doceC = Mockito.mock(Carta.class);
		Mockito.when(doceC.getValor()).thenReturn(12);
		
		Carta unoC = Mockito.mock(Carta.class);
		Mockito.when(unoC.getValor()).thenReturn(1);
		
		Carta sieteC = Mockito.mock(Carta.class);
		Mockito.when(sieteC.getValor()).thenReturn(7);
		
		Mockito.when(ochoD.esMismoPaloQue(ochoP)).thenReturn(false);
		Mockito.when(ochoP.esMismoPaloQue(doceC)).thenReturn(false);
		Mockito.when(doceC.esMismoPaloQue(unoC)).thenReturn(true);
		Mockito.when(unoC.esMismoPaloQue(sieteC)).thenReturn(true);
		
		assertEquals("Nada", poker.verificar(ochoD, ochoP, doceC, unoC, sieteC));
		
		Mockito.verify(ochoD, Mockito.times(10)).getValor();
		Mockito.verify(ochoP, Mockito.times(10)).getValor();
		Mockito.verify(doceC, Mockito.times(10)).getValor();
		Mockito.verify(unoC, Mockito.times(10)).getValor();
		Mockito.verify(sieteC, Mockito.times(10)).getValor();
		
		Mockito.verify(ochoD, Mockito.times(1)).esMismoPaloQue(ochoP);
		Mockito.verify(ochoP, Mockito.never()).esMismoPaloQue(doceC);
		Mockito.verify(doceC, Mockito.never()).esMismoPaloQue(unoC);
		Mockito.verify(unoC, Mockito.never()).esMismoPaloQue(sieteC);
		Mockito.verify(sieteC, Mockito.never()).esMismoPaloQue(ochoD);
	}

}
