package tp8.ej5;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JugadaTestCase {

	private Jugada jugadaPoquer1;
	private Jugada jugadaPoquer2;
	private Jugada jugadaColor1;
	private Jugada jugadaColor2;
	private Jugada jugadaTrio1;
	private Jugada jugadaTrio2;
	private Jugada jugadaNada1;
	private Jugada jugadaNada2;
	private Carta sieteD;
	private Carta sieteT;
	private Carta sieteC;
	private Carta sieteP;
	private Carta asD;
	private Carta asT;
	private Carta asC;
	private Carta asP;
	private Carta cincoC;
	private Carta kC;
	private Carta jC;
	private Carta dosT;
	private Carta nueveT;
	private Carta qT;
	
	@BeforeEach
	public void setUp() throws Exception {
		sieteD = new Carta(7, "Diamantes");
		sieteT = new Carta(7, "Tr�boles");
		sieteC = new Carta(7, "Corazones");
		sieteP = new Carta(7, "Picas");
		asD = new Carta(14, "Diamantes");
		asT = new Carta(14, "Tr�boles");
		asC = new Carta(14, "Corazones");
		asP = new Carta(14, "Picas");
		cincoC = new Carta(5, "Corazones");
		kC = new Carta(13, "Corazones");
		jC = new Carta(11, "Corazones");
		dosT = new Carta(2, "Tr�boles");
		nueveT = new Carta(9, "Tr�boles");
		qT = new Carta(12, "Tr�boles");
		jugadaPoquer1 = new Jugada(Arrays.asList(sieteD, sieteT, sieteC, sieteP), "Poquer");
		jugadaPoquer2 = new Jugada(Arrays.asList(asD, asT, asC, asP), "Poquer");
		jugadaColor1 = new Jugada(Arrays.asList(cincoC, kC, jC, sieteC, asC), "Color");
		jugadaColor2 = new Jugada(Arrays.asList(dosT, nueveT, qT, sieteT, asT), "Color");
		jugadaTrio1 = new Jugada(Arrays.asList(sieteT, sieteC, sieteP), "Trio");
		jugadaTrio2 = new Jugada(Arrays.asList(asD, asP, asC), "Trio");
		jugadaNada1 = new Jugada(Arrays.asList(asC, kC, dosT, nueveT, sieteP), "Nada");
		jugadaNada2 = new Jugada(Arrays.asList(qT, jC, dosT, sieteC, sieteP), "Nada");
	}

	@Test
	public void testLaJugadaDePoquerLeGanaALaDeColor() {
		assertTrue(jugadaPoquer1.leGanaA(jugadaColor2));
	}
	
	@Test
	public void testLaJugadaDePoquerLeGanaALaDeTrio() {
		assertTrue(jugadaPoquer1.leGanaA(jugadaTrio2));
	}
	
	@Test
	public void testLaJugadaDePoquerLeGanaALaDeNada() {
		assertTrue(jugadaPoquer2.leGanaA(jugadaNada1));
	}
	
	@Test
	public void testLaJugadaDePoquer1NoLeGanaALaDePoquer2() {
		assertFalse(jugadaPoquer1.leGanaA(jugadaPoquer2));
	}
	
	@Test
	public void testLaJugadaDePoquer2LeGanaALaDePoquer1() {
		assertTrue(jugadaPoquer2.leGanaA(jugadaPoquer1));
	}
	
	@Test
	public void testLaJugadaDeColorNoLeGanaALaDePoquer() {
		assertFalse(jugadaColor1.leGanaA(jugadaPoquer2));
	}
	
	@Test
	public void testLaJugadaDeColorLeGanaALaDeTrio() {
		assertTrue(jugadaColor2.leGanaA(jugadaTrio1));
	}
	
	@Test
	public void testLaJugadaDeColorLeGanaALaDeNada() {
		assertTrue(jugadaColor2.leGanaA(jugadaNada1));
	}
	
	@Test
	public void testLaJugadaDeColor2NoLeGanaALaDeColor1() {
		assertFalse(jugadaColor2.leGanaA(jugadaColor1));
	}
	
	@Test
	public void testLaJugadaDeColor1LeGanaALaDeColor2() {
		assertTrue(jugadaColor1.leGanaA(jugadaColor2));
	}
	
	@Test
	public void testLaJugadaDeTrioNoLeGanaALaDePoquer() {
		assertFalse(jugadaTrio1.leGanaA(jugadaPoquer1));
	}
	
	@Test
	public void testLaJugadaDeTrioNoLeGanaALaDeColor() {
		assertFalse(jugadaTrio1.leGanaA(jugadaColor1));
	}
	
	@Test
	public void testLaJugadaDeTrioLeGanaALaDeNada() {
		assertTrue(jugadaTrio1.leGanaA(jugadaNada1));
	}
	
	@Test
	public void testLaJugadaDeTrio1NoLeGanaALaDeTrio2() {
		assertFalse(jugadaTrio1.leGanaA(jugadaTrio2));
	}
	
	@Test
	public void testLaJugadaDeTrio2LeGanaALaDeTrio1() {
		assertTrue(jugadaTrio2.leGanaA(jugadaTrio1));
	}
	
	@Test
	public void testLaJugadaDeNadaNoLeGanaALaDePoquer() {
		assertFalse(jugadaNada1.leGanaA(jugadaPoquer2));
	}
	
	@Test
	public void testLaJugadaDeNadaNoLeGanaALaDeColor() {
		assertFalse(jugadaNada1.leGanaA(jugadaColor2));
	}
	
	@Test
	public void testLaJugadaDeNadaNoLeGanaALaDeTrio() {
		assertFalse(jugadaNada1.leGanaA(jugadaTrio2));
	}
	
	@Test
	public void testLaJugadaDeNada1LeGanaALaDeNada2() {
		assertTrue(jugadaNada1.leGanaA(jugadaNada2));
	}
	
	@Test
	public void testLaJugadaDeNada2NoLeGanaALaDeNada1() {
		assertFalse(jugadaNada2.leGanaA(jugadaNada1));
	}

}
