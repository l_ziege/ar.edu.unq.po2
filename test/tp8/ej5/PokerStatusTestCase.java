package tp8.ej5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PokerStatusTestCase {
	
	private PokerStatus poker;
	private Carta cuatroC;
	private Carta sieteC;
	private Carta ochoC;
	private Carta qC;
	private Carta asC;
	private Carta ochoT;
	private Carta ochoD;
	private Carta ochoP;

	@BeforeEach
	public void setUp() throws Exception {
		poker = new PokerStatus();
		cuatroC = new Carta(4, "Corazones");
		sieteC = new Carta(7, "Corazones");
		ochoC = new Carta(8, "Corazones");
		qC = new Carta(12, "Corazones");
		asC = new Carta(14, "Corazones");
		ochoT = new Carta(8, "Tr�boles");
		ochoD = new Carta(8, "Diamantes");
		ochoP = new Carta(8, "Picas");
	}

	@Test
	public void testElJugadorTienePoquer() {
		assertEquals("Poquer", poker.verificar(ochoT, ochoD, ochoC, cuatroC, ochoP).getTipoDeJugada());
	}
	
	@Test
	public void testElJugadorTieneColor() {
		assertEquals("Color", poker.verificar(asC, sieteC, ochoC, cuatroC, qC).getTipoDeJugada());
	}
	
	@Test
	public void testElJugadorTieneTrio() {
		assertEquals("Trio", poker.verificar(ochoD, sieteC, ochoP, cuatroC, ochoT).getTipoDeJugada());
	}
	
	@Test
	public void testElJugadorNoTieneNingunaJugada() {
		assertEquals("Nada", poker.verificar(ochoD, ochoP, qC, asC, sieteC).getTipoDeJugada());
	}

}
