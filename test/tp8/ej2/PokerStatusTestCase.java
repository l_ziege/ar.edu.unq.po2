package tp8.ej2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PokerStatusTestCase {
	
	private PokerStatus poker;

	@BeforeEach
	public void setUp() throws Exception {
		poker = new PokerStatus();
	}

	@Test
	public void testElJugadorTienePoquer() {
		assertEquals("Poquer", poker.verificar("3P", "3C", "8T", "3D", "3T"));
	}
	
	@Test
	public void testElJugadorTieneColor() {
		assertEquals("Color", poker.verificar("5P", "1P", "8P", "3P", "3P"));
	}
	
	@Test
	public void testElJugadorTieneTrio() {
		assertEquals("Trio", poker.verificar("10P", "3C", "8T", "3D", "3T"));
	}
	
	@Test
	public void testElJugadorNoTieneNingunaJugada() {
		assertEquals("Nada", poker.verificar("JP", "QD", "8T", "3D", "7T"));
	}

}
