package tp3.ej4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StringTestCase {

	private String a;
	private String s;
	private String t;
	
	@BeforeEach
	public void setUp() throws Exception {
		a = "abc";
		s = a;
	}
	
	/* Indique que valores retornan las siguientes expresiones o, si dan error, por qu� se producen:
	- s.length(); 							Tama�o del String s (int)
	- t.length();							ERROR a un String vacio sin inicializar no puedo pedirle su tama�o.
	- 1 + a;								Agrega el 1 adelante del String, quedando como resultado "1abc". (String)
	- a.toUpperCase();						Retorna el "abc" en may�sculas. (String)
	- "Libertad".indexOf("r");				Retorna la posici�n de char �r�. (int)
	- "Universidad".lastIndexOf('i');		Retorna la posici�n del �ltimo char �i�. (int)
	- "Quilmes".substring(2,4);				Retorna el String contenido entre las posiciones 2 y 4-1. (String)
	- (a.length() + a).startsWith("a");		Pregunta si el String dado comienza con el char �a�. (boolean)
	- s == a;								Compara si el String s y el String a son iguales. (boolean)
	- a.substring(1,3).equals("bc")			Compara el String contenido entre las posiciones 1 y 3-1 del String a con el String "bc". (boolean)
	*/

	@Test
	public void testPrueba1() {
		assertEquals(3, s.length());
	}
	
	@Test
	public void testPrueba2() {
		//assertEquals(3, t.length());
	}
	
	@Test
	public void testPrueba3() {
		assertEquals("1abc", 1 + a);
	}
	
	@Test
	public void testPrueba4() {
		assertEquals("ABC", a.toUpperCase());
	}
	
	@Test
	public void testPrueba5() {
		assertEquals(4, "Libertad".indexOf("r"));
	}
	
	@Test
	public void testPrueba6() {
		assertEquals(7, "Universidad".lastIndexOf('i'));
	}
	
	@Test
	public void testPrueba7() {
		assertEquals("il", "Quilmes".substring(2,4));
	}
	
	@Test
	public void testPrueba8() {
		assertFalse((a.length() + a).startsWith("a"));
	}
	
	@Test
	public void testPrueba9() {
		assertTrue(s == a);
	}
	
	@Test
	public void testPrueba10() {
		assertTrue(a.substring(1,3).equals("bc"));
	}
}
