package tp3.ej9;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import tp3.ej8.Punto;

public class CuadradoTestCase {

	private Cuadrado cuadrado;
	private Punto puntoCuadrado;
	
	@BeforeEach
	public void setUp() throws Exception {
		puntoCuadrado = new Punto(-2,-1);
		cuadrado = new Cuadrado(puntoCuadrado, 6);
	}

	@Test
	public void testCuadradoCreadoApropiadamente() {
		assertTrue(cuadrado.getP1().getX() == cuadrado.getP4().getX());
		assertTrue(cuadrado.getP2().getX() == cuadrado.getP3().getX());
		assertTrue(cuadrado.getP1().getY() == cuadrado.getP2().getY());
		assertTrue(cuadrado.getP3().getY() == cuadrado.getP4().getY());
	}
	
	@Test
	public void testÁreaDelCuadrado() {
		assertEquals(36, cuadrado.área());
	}
	
	@Test
	public void testPerímetroDelCuadrado() {
		assertEquals(24, cuadrado.perímetro());
	}
}
