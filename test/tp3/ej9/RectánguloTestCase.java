package tp3.ej9;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import tp3.ej8.Punto;

public class RectánguloTestCase {

	private Rectángulo rectangulo;
	private Punto puntoRectangulo;
	
	@BeforeEach
	public void setUp() throws Exception {
		puntoRectangulo = new Punto(5,9);
		rectangulo = new Rectángulo(puntoRectangulo, 7, 5);
	}

	@Test
	public void testRectánguloCreadoApropiadamente() {
		assertTrue(rectangulo.getP1().getX() == rectangulo.getP4().getX());
		assertTrue(rectangulo.getP2().getX() == rectangulo.getP3().getX());
		assertTrue(rectangulo.getP1().getY() == rectangulo.getP2().getY());
		assertTrue(rectangulo.getP3().getY() == rectangulo.getP4().getY());
	}
	
	@Test
	public void testÁreaDelRectángulo() {
		assertEquals(35, rectangulo.área());
	}
	
	@Test
	public void testPerímetroDelRectángulo() {
		assertEquals(24, rectangulo.perímetro());
	}
	
	@Test
	public void testElRectánguloEsHorizontal() {
		assertTrue(rectangulo.esHorizontal());
	}
	
	@Test
	public void testElRectánguloEsVertical() {
		assertFalse(rectangulo.esVertical());
	}
}
