package tp3.ej11;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EquipoDeTrabajoTestCase {

	private EquipoDeTrabajo equipoV;
	private Persona lucas;
	private Persona milena;
	private Persona chula;
	private Persona mica;
	private Persona mauro;
	
	@BeforeEach
	public void setUp() throws Exception {
		equipoV = new EquipoDeTrabajo("Vittar");
		lucas = new Persona("Lucas", "Ziegemann", 24);
		milena = new Persona("Milena", "Quevedo", 24);
		chula = new Persona("Daiana", "Vazquez", 31);
		mica = new Persona("Mica", "Suarez", 22);
		mauro = new Persona("Mauro", "Ayala", 27);
	}

	@Test
	public void testElNombreDelEquipoEsVittar() {
		assertEquals("Vittar", equipoV.getNombre());
	}
	
	@Test
	public void testElPromedioDeEdadDelEquipoEs25() {
		equipoV.agregarAlEquipo(lucas);
		equipoV.agregarAlEquipo(milena);
		equipoV.agregarAlEquipo(chula);
		equipoV.agregarAlEquipo(mica);
		equipoV.agregarAlEquipo(mauro);
		assertEquals(25, equipoV.promedioEdadDelEquipo());
	}
}
