package tp3.ej6;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MultioperadorTestCase {

	private Multioperador multi;
	private List<Integer> enteros;
	
	@BeforeEach
	public void setUp() throws Exception {
		multi = new Multioperador();
		enteros = new ArrayList<Integer>();
		enteros.add(8);
		enteros.add(7);
		enteros.add(-18);
		enteros.add(15);
		enteros.add(3);
		enteros.add(-4);
		enteros.add(-10);
		enteros.add(20);
	}

	@Test
	public void testSumarEnteros() {
		assertEquals(21, multi.sumarEnteros(enteros));
	}
	
	@Test
	public void testRestarEnteros() {
		assertEquals(-21, multi.restarEnteros(enteros));
	}
	
	@Test
	public void testMultiplicarEnteros() {
		assertEquals(-36288000, multi.multiplicarEnteros(enteros));
	}
}
