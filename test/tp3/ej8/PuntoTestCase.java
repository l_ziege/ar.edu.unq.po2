package tp3.ej8;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PuntoTestCase {

	private Punto puntoOrigen;
	private Punto p1;
	private Punto p2;
	
	@BeforeEach
	public void setUp() throws Exception {
		puntoOrigen = new Punto();
		p1 = new Punto(5,8);
		p2 = new Punto(-3, 7);
	}

	@Test
	public void testPuntoConValoresXY() {
		assertEquals(5, p1.getX());
		assertEquals(8, p1.getY());
	}
	
	@Test
	public void testPuntoSinValoresXY() {
		assertEquals(0, puntoOrigen.getX());
		assertEquals(0, puntoOrigen.getY());
	}
	
	@Test
	public void testMoverPuntoAPosicion4x3y() {
		assertEquals(5, p1.getX());
		assertEquals(8, p1.getY());
		p1.moverPunto(4, 3);
		assertEquals(4, p1.getX());
		assertEquals(3, p1.getY());
	}
	
	@Test
	public void testNuevoPuntoSumandoDosPuntos() {
		Punto pNuevo = p1.sumarPuntos(p2);
		assertEquals(2, pNuevo.getX());
		assertEquals(15, pNuevo.getY());
	}
}
