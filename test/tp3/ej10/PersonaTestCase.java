package tp3.ej10;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PersonaTestCase {
	
	private Persona lucas;
	private Persona estela;
	private Date fechaNacLucas;
	private Date fechaNacEstela;

	@BeforeEach
	public void setUp() throws Exception {
		fechaNacLucas = new Date(1997, 3, 6);
		fechaNacEstela = new Date(1965, 8, 15);
		lucas = new Persona("Lucas", fechaNacLucas);
		estela = new Persona("Estela", fechaNacEstela);
	}

	@Test
	public void testElNombreDeLucasEsLucas() {
		assertEquals("Lucas", lucas.getNombre());
	}

	@Test
	public void testLaFechaDeNacimientoDeLucasEs6DeAbrilDe1997() {
		assertEquals(fechaNacLucas, lucas.getFechaNac());
	}
	
	@Test
	public void testLaEdadDeLucasEs24() {
		assertEquals(24, lucas.getEdad());
	}
	
	@Test
	public void testLaEdadDeEstelaEs55() {
		assertEquals(55, estela.getEdad());
	}
	
	@Test
	public void testLucasEsMenorQueEstela() {
		assertTrue(lucas.menorQue(estela));
	}
	
	@Test
	public void testEstelaNoEsMenorQueLucas() {
		assertFalse(estela.menorQue(lucas));
	}
}
