package tp3.ej2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DesarmadorDeNumerosTestCase {

	private DesarmadorDeNumeros ddn;
	
	@BeforeEach
	public void setUp() throws Exception {
		//int númerosEnteros[] = { 48523, 00000, 12111, 55555, 84845, 66999 };
		ddn = new DesarmadorDeNumeros();
	}

	@Test
	public void testElDeMayorCantidadDeDígitosPares() {
		int númerosEnteros[] = { 48523, 53353, 12111, 55555, 84845, 66999, 22885 };
		assertEquals(84845, ddn.elDeMayorCantDígitosPares(númerosEnteros));
	}

}
