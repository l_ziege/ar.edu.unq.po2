package tp3.ej3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MúltiplosTestCase {

	private Múltiplos multiplos;
	
	@BeforeEach
	public void setUp() throws Exception {
		multiplos = new Múltiplos();
	}

	@Test
	public void testElMúltiploMásAltoEntre0y1000De5y7() {
		assertEquals(980, multiplos.elMultiploEntre0Y1000MasAltoEntre(5,7));
	}
	
	@Test
	public void testElMúltiploMásAltoEntre0y1000De15y700() {
		assertEquals(-1, multiplos.elMultiploEntre0Y1000MasAltoEntre(15,700));
	}

}
