package tp6.ej1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ColorLesionesDermatolˇgicasTestCase {
	
	private ColorLesionesDermatolˇgicas colorRojo;
	private ColorLesionesDermatolˇgicas colorGris;
	private ColorLesionesDermatolˇgicas colorAmarillo;
	private ColorLesionesDermatolˇgicas colorMiel;

	@BeforeEach
	public void setUp() throws Exception {
		colorRojo = ColorLesionesDermatolˇgicas.ROJO;
		colorGris = ColorLesionesDermatolˇgicas.GRIS;
		colorAmarillo = ColorLesionesDermatolˇgicas.AMARILLO;
		colorMiel = ColorLesionesDermatolˇgicas.MIEL;
	}

	@Test
	public void testDescripciˇnPredefinidaDeRojo() {
		assertEquals("Describe de la forma en que se debe describir el color Rojo.", colorRojo.descripciˇnPredefinida());
	}
	
	@Test
	public void testDescripciˇnPredefinidaDeGris() {
		assertEquals("Describe de la forma en que se debe describir el color Gris.", colorGris.descripciˇnPredefinida());
	}
	
	@Test
	public void testDescripciˇnPredefinidaDeAmarillo() {
		assertEquals("Describe de la forma en que se debe describir el color Amarillo.", colorAmarillo.descripciˇnPredefinida());
	}
	
	@Test
	public void testDescripciˇnPredefinidaDeMiel() {
		assertEquals("Describe de la forma en que se debe describir el color Miel.", colorMiel.descripciˇnPredefinida());
	}
	
	@Test
	public void testElNivelDeRiesgoDelColorRojoEs1() {
		assertTrue(colorRojo.nivelDeRiesgo() == 1);
	}
	
	@Test
	public void testElNivelDeRiesgoDelColorGrisEs2() {
		assertTrue(colorGris.nivelDeRiesgo() == 2);
	}
	
	@Test
	public void testElNivelDeRiesgoDelColorAmarilloEs3() {
		assertTrue(colorAmarillo.nivelDeRiesgo() == 3);
	}
	
	@Test
	public void testElNivelDeRiesgoDelColorMielEs4() {
		assertTrue(colorMiel.nivelDeRiesgo() == 4);
	}
	
	@Test
	public void testElPrˇximoColorDeRojoEsGris() {
		assertEquals(ColorLesionesDermatolˇgicas.GRIS, colorRojo.prˇximoColorEnProcesoMadurativo());
	}
	
	@Test
	public void testElPrˇximoColorDeGrisEsAmarillo() {
		assertEquals(ColorLesionesDermatolˇgicas.AMARILLO, colorGris.prˇximoColorEnProcesoMadurativo());
	}
	
	@Test
	public void testElPrˇximoColorDeAmarilloEsMiel() {
		assertEquals(ColorLesionesDermatolˇgicas.MIEL, colorAmarillo.prˇximoColorEnProcesoMadurativo());
	}
	
	@Test
	public void testElPrˇximoColorDeMielEsRojo() {
		assertEquals(ColorLesionesDermatolˇgicas.ROJO, colorMiel.prˇximoColorEnProcesoMadurativo());
	}
}
