package tp6.ej2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SecretariaDeDeportesTestCase {
	
	private SecretariaDeDeportes sdd;
	private ActividadSemanal a1;
	private ActividadSemanal a2;
	private ActividadSemanal a3;
	private ActividadSemanal a4;
	private ActividadSemanal a5;
	private ActividadSemanal a6;
	private ActividadSemanal a7;
	private ActividadSemanal a8;
	private ActividadSemanal a9;
	private ActividadSemanal a10;
	

	@BeforeEach
	public void setUp() throws Exception {
		a1 = new ActividadSemanal(DiaDeLaSemana.LUNES, 1500, 120, Deporte.FUTBOL);
		a2 = new ActividadSemanal(DiaDeLaSemana.SABADO, 2200, 180, Deporte.FUTBOL);
		a3 = new ActividadSemanal(DiaDeLaSemana.DOMINGO, 1800, 60, Deporte.RUNNING);
		a4 = new ActividadSemanal(DiaDeLaSemana.JUEVES, 1700, 60, Deporte.RUNNING);
		a5 = new ActividadSemanal(DiaDeLaSemana.MARTES, 2000, 60, Deporte.BASKET);
		a6 = new ActividadSemanal(DiaDeLaSemana.SABADO, 2100, 120, Deporte.BASKET);
		a7 = new ActividadSemanal(DiaDeLaSemana.VIERNES, 800, 60, Deporte.JABALINA);
		a8 = new ActividadSemanal(DiaDeLaSemana.MIERCOLES, 700, 60, Deporte.JABALINA);
		a9 = new ActividadSemanal(DiaDeLaSemana.DOMINGO, 1500, 240, Deporte.TENNIS);
		a10 = new ActividadSemanal(DiaDeLaSemana.MARTES, 1200, 180, Deporte.TENNIS);
		List<ActividadSemanal> actividades = Arrays.asList(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10);
		sdd = new SecretariaDeDeportes(actividades);
	}

	@Test
	public void testLaSecretariaDeDeportesTiene2ActividadesDeFutbol() {
		assertEquals(2, sdd.actividadesDeFutbol().size());
	}
	
	@Test
	public void testLaSecretariaDeDeportesTiene4ActividadesDeComplejidad2() {
		assertTrue(4 == sdd.actividadesDeComplejidad(2).size());
	}
	
	@Test
	public void testLaCantidadDeHorasTotalesDeLasActividadesDeLaSecretariaDeDeportesEs16() {
		assertTrue(19 == sdd.cantHorasTotalesDeActividadesSemanales());
	}

	@Test
	public void testLaActividadDeTennisDeMenorCostoEnLaSecretariaDeDeportesEsA10() {
		assertEquals(a10, sdd.laDeMenorCostoDeUnDeporte(Deporte.TENNIS));
	}
	
	@Test
	public void testLaActividadDeFutbolDeMenorCostoEnLaSecretariaDeDeportesEsA1() {
		assertEquals(a1, sdd.laDeMenorCostoDeUnDeporte(Deporte.FUTBOL));
	}
	
	@Test
	public void testLaActividadDeRunningDeMenorCostoEnLaSecretariaDeDeportesEsA3() {
		assertEquals(a3, sdd.laDeMenorCostoDeUnDeporte(Deporte.RUNNING));
	}
	
	@Test
	public void testLaActividadDeJabalinaDeMenorCostoEnLaSecretariaDeDeportesEsA8() {
		assertEquals(a8, sdd.laDeMenorCostoDeUnDeporte(Deporte.JABALINA));
	}
	
	@Test
	public void testLaActividadDeBasquetDeMenorCostoEnLaSecretariaDeDeportesEsA5() {
		assertEquals(a5, sdd.laDeMenorCostoDeUnDeporte(Deporte.BASKET));
	}
	
	@Test
	public void testLasActividadesDeMenorCostoEnLaSecretariaDeDeportesSonA1A3A5A8yA10() {
		assertEquals(a1, sdd.laDeMenorCostoDeCadaDeporte().get(Deporte.FUTBOL));
		assertEquals(a3, sdd.laDeMenorCostoDeCadaDeporte().get(Deporte.RUNNING));
		assertEquals(a5, sdd.laDeMenorCostoDeCadaDeporte().get(Deporte.BASKET));
		assertEquals(a8, sdd.laDeMenorCostoDeCadaDeporte().get(Deporte.JABALINA));
		assertEquals(a10, sdd.laDeMenorCostoDeCadaDeporte().get(Deporte.TENNIS));
	}
	
	public static void main(String args[]) {
		SecretariaDeDeportes sdd = new SecretariaDeDeportes();
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.LUNES, 1500, 120, Deporte.FUTBOL));
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.SABADO, 2200, 180, Deporte.FUTBOL));
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.DOMINGO, 1800, 60, Deporte.RUNNING));
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.JUEVES, 1700, 60, Deporte.RUNNING));
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.MARTES, 2000, 60, Deporte.BASKET));
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.SABADO, 2100, 120, Deporte.BASKET));
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.VIERNES, 800, 60, Deporte.JABALINA));
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.MIERCOLES, 700, 60, Deporte.JABALINA));
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.DOMINGO, 1500, 240, Deporte.TENNIS));
		sdd.agregarActividadSemanal(new ActividadSemanal(DiaDeLaSemana.MARTES, 1200, 180, Deporte.TENNIS));
		sdd.imprimirTodasLasActividades();
	}
}
