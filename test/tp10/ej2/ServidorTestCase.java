package tp10.ej2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ServidorTestCase {
	
	private Servidor veinte;
	private AppDeResultadosDePartidos app;

	@BeforeEach
	public void setUp() throws Exception {
		veinte = new Servidor(Arrays.asList(Deporte.TENIS));
		app = new AppDeResultadosDePartidos();
	}

	@Test
	public void testElServidorVeinteRecibeElResultadoDeUnNuevoPartidoDeTenisRegistradoEnLaApp() {
		app.registrarServidorObservador(veinte);
		
		ResultadoDePartido resultadoTenis = new ResultadoDePartido("3-2", Arrays.asList("Carlos Perez", "Mario Baron"), Deporte.TENIS);
		app.ingresarResultadoDePartido(resultadoTenis);
		
		assertEquals(1, veinte.getResultados().size());
	}
	
	@Test
	public void testElServidorVeinteNoRecibeElResultadoDeUnNuevoPartidoDePingPongRegistradoEnLaApp() {
		app.registrarServidorObservador(veinte);
		
		ResultadoDePartido resultadoPingPong = new ResultadoDePartido("5-1", Arrays.asList("Ariel Puchetta", "Rodrigo Tapari"), Deporte.PINGPONG);
		app.ingresarResultadoDePartido(resultadoPingPong);
		
		assertEquals(0, veinte.getResultados().size());
	}

}
