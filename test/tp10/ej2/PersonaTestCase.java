package tp10.ej2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PersonaTestCase {
	
	private Persona lucas;
	private Móvil móvil;
	private AppMóvil appMóvil;
	private Servidor veinte;
	private AppDeResultadosDePartidos appResultados;

	@BeforeEach
	public void setUp() throws Exception {
		appResultados = new AppDeResultadosDePartidos();
		veinte = new Servidor(Arrays.asList(Deporte.TENIS, Deporte.BASQUET));
		appMóvil = new AppMóvil(veinte, Deporte.BASQUET, "Ariel Puchetta");
		móvil = new Móvil(appMóvil);
		lucas = new Persona("Lucas", móvil);
	}

	@Test
	public void testLucasQuiereEnviarUnMensajeALaAppYRecibeUnaListaDeResultadosVacíaDebidoAQueNoHayActualización() {
		assertTrue(lucas.pedirResultadosDeInteres().isEmpty());
	}
	
	@Test
	public void testLucasQuiereEnviarUnMensajeALaAppLuegoDeRegistrarseElResultadoDeUnPartidoDeTenisYRecibeUnaListaDeResultadosVacíaDebidoAQueNoHayActualización() {
		appResultados.registrarServidorObservador(veinte);
		
		ResultadoDePartido resultadoTenis = new ResultadoDePartido("3-2", Arrays.asList("Carlos Perez", "Mario Baron"), Deporte.TENIS);
		appResultados.ingresarResultadoDePartido(resultadoTenis);
		
		assertTrue(lucas.pedirResultadosDeInteres().isEmpty());
	}
	
	@Test
	public void testLucasEnviaUnMensajeALaAppYRecibeUnResultadoDeUnPartidoLuegoDeRegistrarseUnPartidoDeTenisConArielPuchetta() {
		appResultados.registrarServidorObservador(veinte);
		
		ResultadoDePartido resultadoTenis = new ResultadoDePartido("5-1", Arrays.asList("Ariel Puchetta", "Pablo Lescano"), Deporte.TENIS);
		appResultados.ingresarResultadoDePartido(resultadoTenis);
		
		assertEquals(1, lucas.pedirResultadosDeInteres().size());
	}
	
	@Test
	public void testLucasEnviaUnMensajeALaAppYNoRecibeNadaLuegoDeHaberEnviadoElMensajeAnteriormenteHabiendoUnaActualización() {
		appResultados.registrarServidorObservador(veinte);
		
		ResultadoDePartido resultadoTenis = new ResultadoDePartido("5-1", Arrays.asList("Ariel Puchetta", "Pablo Lescano"), Deporte.TENIS);
		appResultados.ingresarResultadoDePartido(resultadoTenis);
		
		assertEquals(1, lucas.pedirResultadosDeInteres().size());
		assertEquals(0, lucas.pedirResultadosDeInteres().size());
	}

}
