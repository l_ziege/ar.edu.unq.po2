package tp10.ej2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AppDeResultadosDePartidosTestCase {

	private AppDeResultadosDePartidos app;
	
	@BeforeEach
	public void setUp() throws Exception {
		app = new AppDeResultadosDePartidos();
	}

	@Test
	public void testSeRegistraElResultadoDeUnPartidoDeTenisEnLaApp() {
		ResultadoDePartido resultadoTenis = new ResultadoDePartido("3-2", Arrays.asList("Carlos Perez", "Mario Baron"), Deporte.TENIS);
		app.ingresarResultadoDePartido(resultadoTenis);
		assertEquals(1, app.getResultados().size());
	}
	
	@Test
	public void testSeRegistraElServidorVeinteEnLaApp() {
		Servidor veinte = new Servidor(Arrays.asList(Deporte.TENIS));
		app.registrarServidorObservador(veinte);
		assertEquals(1, app.getServidoresObservadores().size());
	}

}
