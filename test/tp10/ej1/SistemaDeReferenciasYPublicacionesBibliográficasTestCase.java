package tp10.ej1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SistemaDeReferenciasYPublicacionesBibliográficasTestCase {

	private SistemaDeReferenciasYPublicacionesBibliográficas sistema;
	private MiembroDelLaboratorio pepe;
	
	@BeforeEach
	public void setUp() throws Exception {
		sistema = new SistemaDeReferenciasYPublicacionesBibliográficas();
		pepe = new MiembroDelLaboratorio("Pepe");
	}

	@Test
	public void testAgregadoDeArticulo() {
		List<Autor> autores = Arrays.asList(new Autor("Carlos Pedro"), new Autor("Marcos Paz"));
		List<Filiación> filiaciones = Arrays.asList(new Filiación("Universidad Nacional de Quilmes"));
		List<String> palabrasClave = Arrays.asList("Polimorfismo", "Encapsulamiento", "Clases");
		sistema.cargarArtículoCientífico("Lenguaje Smalltalks", autores, filiaciones, "Trabajo de investigación", "Conferencia Smalltalks", palabrasClave);
		
		assertEquals(1, sistema.getArticulos().size());
	}

	@Test
	public void testElMiembroPepeRecibeLaNotificaciónDeUnaNuevaPublicaciónDeSuInterés() {
		pepe.agregarTemaDeInvestigación("Conferencia Smalltalks");
		sistema.registrarMiembroObservador(pepe);
		
		List<Autor> autores = Arrays.asList(new Autor("Carlos Pedro"), new Autor("Marcos Paz"));
		List<Filiación> filiaciones = Arrays.asList(new Filiación("Universidad Nacional de Quilmes"));
		List<String> palabrasClave = Arrays.asList("Polimorfismo", "Encapsulamiento", "Clases");
		sistema.cargarArtículoCientífico("Lenguaje Smalltalks", autores, filiaciones, "Trabajo de investigación", "Conferencia Smalltalks", palabrasClave);
		assertEquals(1, sistema.getArticulos().size());
	}
	
	@Test
	public void testElMiembroPepeRecibeUnaNotificaciónDeUnSistemaQueNoEsElEsperado() {
		pepe.agregarTemaDeInvestigación("Conferencia Smalltalks");
		sistema.registrarMiembroObservador(pepe);
		
		SistemaDeReferenciasYPublicacionesBibliográficas nuevoSistema = new SistemaDeReferenciasYPublicacionesBibliográficas();
		nuevoSistema.registrarMiembroObservador(pepe);
		
		List<Autor> autores = Arrays.asList(new Autor("Carlos Pedro"), new Autor("Marcos Paz"));
		List<Filiación> filiaciones = Arrays.asList(new Filiación("Universidad Nacional de Quilmes"));
		List<String> palabrasClave = Arrays.asList("Polimorfismo", "Encapsulamiento", "Clases");
		sistema.cargarArtículoCientífico("Lenguaje Smalltalks", autores, filiaciones, "Trabajo de investigación", "Conferencia Smalltalks", palabrasClave);
		assertEquals(1, sistema.getArticulos().size());
	}
	
}
