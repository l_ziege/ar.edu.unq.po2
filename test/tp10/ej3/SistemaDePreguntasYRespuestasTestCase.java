package tp10.ej3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SistemaDePreguntasYRespuestasTestCase {
	
	private SistemaDePreguntasYRespuestas sistema;
	private Pregunta pregunta1;
	private Pregunta pregunta2;
	private Pregunta pregunta3;
	private Pregunta pregunta4;
	private Pregunta pregunta5;
	private Respuesta respuesta1;
	private Respuesta respuesta2;
	private Respuesta respuesta3;
	private Respuesta respuesta4;
	private Respuesta respuesta5;
	private Jugador lucas;
	private Jugador chula;
	private Jugador mauro;
	private Jugador mica;
	private Jugador franco;
	private Jugador mike;

	@BeforeEach
	public void setUp() throws Exception {
		pregunta1 = new Pregunta(1, "�Cu�nto es 5 por 5?");
		pregunta2 = new Pregunta(2, "�C�mo se llama el papa de Bart en la serie Los Simpson?");
		pregunta3 = new Pregunta(3, "�C�mo se llama la ciudad donde se encuentra la Universidad Nacional de Quilmes?");
		pregunta4 = new Pregunta(4, "�Cu�ntos son los enanitos del cuento de Blancanieves?");
		pregunta5 = new Pregunta(5, "�C�mo se llama el protagonista de Digimon Adventure?");
		respuesta1 = new Respuesta(1, "25");
		respuesta2 = new Respuesta(2, "Homero");
		respuesta3 = new Respuesta(3, "Bernal");
		respuesta4 = new Respuesta(4, "7");
		respuesta5 = new Respuesta(5, "Tai Kamiya");
		sistema = new SistemaDePreguntasYRespuestas(Arrays.asList(pregunta1, pregunta2, pregunta3, pregunta4, pregunta5), Arrays.asList(respuesta1, respuesta2, respuesta3, respuesta4, respuesta5));
		lucas = new Jugador("Lucas");
		chula = new Jugador("Chula");
		mauro = new Jugador("Mauro");
		mica = new Jugador("Mica");
		franco = new Jugador("Franco");
		mike = new Jugador("Mike");
	}

	@Test
	public void testSeSuman2ParticipantesAlConcurso() {
		lucas.sumarseASiguientePartidaDelSistema(sistema);
		chula.sumarseASiguientePartidaDelSistema(sistema);
		
		assertEquals(2, sistema.getParticipantes().size());
	}

	@Test
	public void testSeIntentaAgregar6ParticipantesAlSistemaPeroEsRechazado() {
		lucas.sumarseASiguientePartidaDelSistema(sistema);
		chula.sumarseASiguientePartidaDelSistema(sistema);
		mauro.sumarseASiguientePartidaDelSistema(sistema);
		mica.sumarseASiguientePartidaDelSistema(sistema);
		franco.sumarseASiguientePartidaDelSistema(sistema);
		mike.sumarseASiguientePartidaDelSistema(sistema);
		
		assertEquals(5, sistema.getParticipantes().size());
	}
	
	@Test
	public void testElSistemaQuiereIniciarUnaPartidaSinTener5ParticipantesRegistrados() {
		sistema.iniciarPartida();
		
		assertEquals(0, sistema.getParticipantes().size());
	}
	
	@Test
	public void testSeIntentaAgregarUnParticipanteAlSistemaConUnaPartidaIniciadaYEsRechazado() {
		lucas.sumarseASiguientePartidaDelSistema(sistema);
		chula.sumarseASiguientePartidaDelSistema(sistema);
		mauro.sumarseASiguientePartidaDelSistema(sistema);
		mica.sumarseASiguientePartidaDelSistema(sistema);
		franco.sumarseASiguientePartidaDelSistema(sistema);
		
		sistema.iniciarPartida();
		
		mike.sumarseASiguientePartidaDelSistema(sistema);
		
		assertEquals(5, sistema.getParticipantes().size());
	}
	
	@Test
	public void testElSistemaIntentaIniciarUnaPartida2VecesYLaSegundaEsRechazada() {
		lucas.sumarseASiguientePartidaDelSistema(sistema);
		chula.sumarseASiguientePartidaDelSistema(sistema);
		mauro.sumarseASiguientePartidaDelSistema(sistema);
		mica.sumarseASiguientePartidaDelSistema(sistema);
		franco.sumarseASiguientePartidaDelSistema(sistema);
		
		sistema.iniciarPartida();
		sistema.iniciarPartida();
		
		assertEquals(5, sistema.getParticipantes().size());
	}
	
	@Test
	public void testElParticipanteMikeEnviaUnaRespuestaAlSistemaYEsRechazadaYaQueNoHayUnaPartidaIniciada() {
		lucas.sumarseASiguientePartidaDelSistema(sistema);
		chula.sumarseASiguientePartidaDelSistema(sistema);
		mauro.sumarseASiguientePartidaDelSistema(sistema);
		mica.sumarseASiguientePartidaDelSistema(sistema);
		mike.sumarseASiguientePartidaDelSistema(sistema);
		
		sistema.iniciarPartida();
		sistema.finalizarPartida();
		
		mike.enviarRespuesta(new Respuesta(3, "Bernal"));
		
		assertEquals(0, sistema.getParticipantes().size());
	}
	
	@Test
	public void testElSistemaIniciaUnaPartidaRecibeUnaRespuestaCorrectaDeLucasYLaNotifica() {
		lucas.sumarseASiguientePartidaDelSistema(sistema);
		chula.sumarseASiguientePartidaDelSistema(sistema);
		mauro.sumarseASiguientePartidaDelSistema(sistema);
		mica.sumarseASiguientePartidaDelSistema(sistema);
		franco.sumarseASiguientePartidaDelSistema(sistema);
		
		sistema.iniciarPartida();
		
		lucas.enviarRespuesta(new Respuesta(2, "Homero"));
		
		assertEquals(1, sistema.respuestasDelParticipante(lucas).size());
	}
	
	@Test
	public void testElSistemaRechazaUnaRespuestaRespondidaAnteriormentePorLaParticipanteChula() {
		lucas.sumarseASiguientePartidaDelSistema(sistema);
		chula.sumarseASiguientePartidaDelSistema(sistema);
		mauro.sumarseASiguientePartidaDelSistema(sistema);
		mica.sumarseASiguientePartidaDelSistema(sistema);
		franco.sumarseASiguientePartidaDelSistema(sistema);
		
		sistema.iniciarPartida();
		
		chula.enviarRespuesta(new Respuesta(4, "7"));
		chula.enviarRespuesta(new Respuesta(4, "7"));
		
		assertEquals(1, sistema.respuestasDelParticipante(chula).size());
	}
	
	@Test
	public void testElSistemaIniciaUnaPartidaRecibeUnaRespuestaIncorrectaDeFrancoYLaNotifica() {
		lucas.sumarseASiguientePartidaDelSistema(sistema);
		chula.sumarseASiguientePartidaDelSistema(sistema);
		mauro.sumarseASiguientePartidaDelSistema(sistema);
		mica.sumarseASiguientePartidaDelSistema(sistema);
		franco.sumarseASiguientePartidaDelSistema(sistema);
		
		sistema.iniciarPartida();
		
		franco.enviarRespuesta(new Respuesta(1, "50"));
		
		assertEquals(0, sistema.respuestasDelParticipante(franco).size());
	}
	
	@Test
	public void testElSistemaIniciaUnaPartidaRecibeLas5RespuestasCorrectasDeMauroNotificaElGanadorYFinalizaLaPartidaQuedandoSinParticipantes() {
		lucas.sumarseASiguientePartidaDelSistema(sistema);
		chula.sumarseASiguientePartidaDelSistema(sistema);
		mauro.sumarseASiguientePartidaDelSistema(sistema);
		mica.sumarseASiguientePartidaDelSistema(sistema);
		franco.sumarseASiguientePartidaDelSistema(sistema);
		
		sistema.iniciarPartida();
		
		mauro.enviarRespuesta(new Respuesta(2, "Homero"));
		mauro.enviarRespuesta(new Respuesta(3, "Bernal"));
		mauro.enviarRespuesta(new Respuesta(5, "Tai Kamiya"));
		mauro.enviarRespuesta(new Respuesta(4, "7"));
		mauro.enviarRespuesta(new Respuesta(1, "25"));
		
		assertEquals(0, sistema.getParticipantes().size());
	}
	
	@Test
	public void testElSistemaQuiereFinalizarUnaPartidaSinIniciarYEsRechazada() {
		sistema.finalizarPartida();
		
		assertEquals(0, sistema.getParticipantes().size());
	}
}
