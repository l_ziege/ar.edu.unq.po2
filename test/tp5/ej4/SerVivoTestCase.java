package tp5.ej4;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SerVivoTestCase {
	
	private Persona lucas;
	private Persona chula;
	private Mascota chimuela;
	private Mascota scrapy;
	private List<SerVivo> seresVivos;

	@BeforeEach
	public void setUp() throws Exception {
		lucas = new Persona("Lucas", new Date(1997, 3, 6));
		chula = new Persona("Chula", new Date(1990, 9, 4));
		chimuela = new Mascota("Chimuela", "Collie");
		scrapy = new Mascota("Scrapy", "Pequines");
	}

	@Test
	public void testElNombreDeLucasEsLucas() {
		assertEquals("Lucas", lucas.getName());
	}
	
	@Test
	public void testLaFechaDeNacimientoDeLucasEs6DeAbrilDe1997() {
		assertEquals(new Date(1997, 3, 6), lucas.getFechaNac());
	}
	
	@Test
	public void testLaEdadDeLucasEs24() {
		assertEquals(24, lucas.getEdad());
	}
	
	@Test
	public void testLaEdadDeChulaEs30() {
		assertEquals(30, chula.getEdad());
	}
	
	@Test
	public void testLucasEsMenorQueChula() {
		assertTrue(lucas.menorQue(chula));
	}
	
	@Test
	public void testChulaNoEsMenorQueLucas() {
		assertFalse(chula.menorQue(lucas));
	}
	
	@Test
	public void testElNombreDeChimuelaEsChimuela() {
		assertEquals("Chimuela", chimuela.getName());
	}
	
	@Test
	public void testLaRazaDeChimuelaEsCollie() {
		assertEquals("Collie", chimuela.getRaza());
	}
	
	public static void main(String[] args) {
		Persona lucas = new Persona("Lucas", new Date(1997, 3, 6));
		Persona chula = new Persona("Chula", new Date(1990, 9, 4));
		Mascota chimuela = new Mascota("Chimuela", "Collie");
		Mascota scrapy = new Mascota("Scrapy", "Pequines");
		List <SerVivo> seresVivos = Arrays.asList(lucas, chula, chimuela, scrapy);
		for (SerVivo s: seresVivos) {
			System.out.println(s.getName());
		}
	}
}
