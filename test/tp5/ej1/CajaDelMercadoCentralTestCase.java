package tp5.ej1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CajaDelMercadoCentralTestCase {

	private CajaDelMercadoCentral caja;
	private ProductoDeEmpresaTradicional yerba;
	private ProductoDeCooperativa azucar;
	
	@BeforeEach
	public void setUp() throws Exception {
		caja = new CajaDelMercadoCentral();
		yerba = new ProductoDeEmpresaTradicional(250, 2);
		azucar = new ProductoDeCooperativa(120, 5);
	}

	@Test
	public void testLaCajaRegistraUnaYerbaYElMontoAPagarEs250() {
		caja.registrarUnProducto(yerba);
		assertEquals(250, caja.informarMontoTotalAPagar());
	}

	@Test
	public void testLaCajaRegistraUnAzucarYElMontoAPagarEs108() {
		caja.registrarUnProducto(azucar);
		assertEquals(108, caja.informarMontoTotalAPagar());
	}
	
	@Test
	public void testLaCajaRegistra2YerbasYElMontoAPagarEs500() {
		caja.registrarUnProducto(yerba);
		caja.registrarUnProducto(yerba);
		assertEquals(500, caja.informarMontoTotalAPagar());
	}

	@Test
	public void testLaCajaRegistra3YerbasYElMontoAPagarEs500PorFaltaDeStock() {
		caja.registrarUnProducto(yerba);
		caja.registrarUnProducto(yerba);
		caja.registrarUnProducto(yerba);
		assertEquals(500, caja.informarMontoTotalAPagar());
	}
}
