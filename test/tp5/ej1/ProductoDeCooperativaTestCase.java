package tp5.ej1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductoDeCooperativaTestCase {

	private ProductoDeCooperativa azucar;
	
	@BeforeEach
	public void setUp() throws Exception {
		azucar = new ProductoDeCooperativa(120, 5);
	}

	@Test
	public void testStockDeAzucarEs5() {
		assertEquals(5, azucar.getStock());
	}
	
	@Test
	public void testStockDeAzucarEs4DespuesDeDecrementar() {
		assertEquals(5, azucar.getStock());
		azucar.decrementarStock();
		assertEquals(4, azucar.getStock());
	}

	@Test
	public void testStockDeAzucarEs0DespuesDeDecrementar8Veces() {
		assertEquals(5, azucar.getStock());
		azucar.decrementarStock();
		assertEquals(4, azucar.getStock());
		azucar.decrementarStock();
		azucar.decrementarStock();
		azucar.decrementarStock();
		azucar.decrementarStock();
		assertEquals(0, azucar.getStock());
		azucar.decrementarStock();
		assertEquals(0, azucar.getStock());
	}
	
	@Test
	public void testElPrecioDeAzucarEs108PorDescuentoDel10PorCiento() {
		assertEquals(108, azucar.consultarPrecio());
	}
	
	@Test
	public void testElPrecioDeAzucarCuandoNoHayStockEs0() {
		azucar.decrementarStock();
		azucar.decrementarStock();
		azucar.decrementarStock();
		azucar.decrementarStock();
		azucar.decrementarStock();
		assertEquals(0, azucar.consultarPrecio());
	}
}
