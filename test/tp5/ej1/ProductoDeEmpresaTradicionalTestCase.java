package tp5.ej1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductoDeEmpresaTradicionalTestCase {

	private ProductoDeEmpresaTradicional yerba;
	
	@BeforeEach
	public void setUp() throws Exception {
		yerba = new ProductoDeEmpresaTradicional(250, 2);
	}

	@Test
	public void testStockDeYerbaEs2() {
		assertEquals(2, yerba.getStock());
	}
	
	@Test
	public void testStockDeYerbaEs1DespuesDeDecrementar() {
		assertEquals(2, yerba.getStock());
		yerba.decrementarStock();
		assertEquals(1, yerba.getStock());
	}

	@Test
	public void testStockDeYerbaEs0DespuesDeDecrementar3Veces() {
		assertEquals(2, yerba.getStock());
		yerba.decrementarStock();
		assertEquals(1, yerba.getStock());
		yerba.decrementarStock();
		assertEquals(0, yerba.getStock());
		yerba.decrementarStock();
		assertEquals(0, yerba.getStock());
	}
	
	@Test
	public void testElPrecioDeYerbaEs250() {
		assertEquals(250, yerba.consultarPrecio());
	}
	
	@Test
	public void testElPrecioDeYerbaCuandoNoHayStockEs0() {
		yerba.decrementarStock();
		yerba.decrementarStock();
		assertEquals(0, yerba.consultarPrecio());
	}
}
