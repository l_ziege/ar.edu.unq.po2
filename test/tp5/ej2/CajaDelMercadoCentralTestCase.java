package tp5.ej2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CajaDelMercadoCentralTestCase {

	private CajaDelMercadoCentral caja;
	private ProductoDeEmpresaTradicional yerba;
	private ProductoDeCooperativa azucar;
	private FacturaDeImpuesto facturaI;
	private FacturaDeServicio facturaDeLuz;
	
	@BeforeEach
	public void setUp() throws Exception {
		caja = new CajaDelMercadoCentral();
		yerba = new ProductoDeEmpresaTradicional(250, 2);
		azucar = new ProductoDeCooperativa(120, 5);
		facturaI = new FacturaDeImpuesto(4300);
		facturaDeLuz = new FacturaDeServicio(12, 500);
	}

	@Test
	public void testLaCajaRegistraUnaYerbaYElMontoAPagarEs250() {
		caja.registrar(yerba);
		assertEquals(250, caja.informarMontoTotalAPagar());
	}

	@Test
	public void testLaCajaRegistraUnAzucarYElMontoAPagarEs108() {
		caja.registrar(azucar);
		assertEquals(108, caja.informarMontoTotalAPagar());
	}
	
	@Test
	public void testLaCajaRegistra2YerbasYElMontoAPagarEs500() {
		caja.registrar(yerba);
		caja.registrar(yerba);
		assertEquals(500, caja.informarMontoTotalAPagar());
	}

	@Test
	public void testLaCajaRegistra3YerbasYElMontoAPagarEs500PorFaltaDeStock() {
		caja.registrar(yerba);
		caja.registrar(yerba);
		caja.registrar(yerba);
		assertEquals(500, caja.informarMontoTotalAPagar());
	}
	
	@Test
	public void testLaCajaRegistra6AzucaresYElMontoAPagarEs540PorFaltaDeStock() {
		caja.registrar(azucar);
		caja.registrar(azucar);
		caja.registrar(azucar);
		caja.registrar(azucar);
		caja.registrar(azucar);
		caja.registrar(azucar);
		assertEquals(540, caja.informarMontoTotalAPagar());
	}
	
	@Test
	public void testLaCajaRegistra1FacturaDeImpuestoYElMontoAPagarEs4300() {
		caja.registrar(facturaI);
		assertEquals(4300, caja.informarMontoTotalAPagar());
	}
	
	@Test
	public void testLaCajaRegistra1FacturaDeLuzYElMontoAPagarEs6000() {
		caja.registrar(facturaDeLuz);
		assertEquals(6000, caja.informarMontoTotalAPagar());
	}
}
