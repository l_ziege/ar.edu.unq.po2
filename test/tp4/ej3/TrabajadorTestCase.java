package tp4.ej3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TrabajadorTestCase {
	
	private Trabajador trabajador;
	private Ingreso i1;
	private Ingreso i2;
	private Ingreso i3;
	private IngresoPorHorasExtras iphe1;

	@BeforeEach
	public void setUp() throws Exception {
		trabajador = new Trabajador();
		i1 = new Ingreso("Marzo", "Pago del mes", 20400.50);
		i2 = new Ingreso("Junio", "Pago del mes", 25000.75);
		i3 = new Ingreso("Octubre", "Pago del mes", 22125.50);
		iphe1 = new IngresoPorHorasExtras("Enero", "Horas extras del mes", 4000, 40);
	}

	@Test
	public void testTotalRecibido() {
		assertEquals(0, trabajador.getTotalPercibido());
		trabajador.registrarIngresoPercibido(i1); 
		trabajador.registrarIngresoPercibido(i2);
		trabajador.registrarIngresoPercibido(i3);
		trabajador.registrarIngresoPercibido(iphe1);
		assertEquals(71526.75, trabajador.getTotalPercibido());
	}
	
	@Test
	public void testMontoImponible() {
		assertEquals(0, trabajador.getMontoImponible());
		trabajador.registrarIngresoPercibido(i1); 
		trabajador.registrarIngresoPercibido(i2);
		trabajador.registrarIngresoPercibido(i3);
		trabajador.registrarIngresoPercibido(iphe1);
		assertEquals(67526.75, trabajador.getMontoImponible());
	}
	
	@Test
	public void testImpuestoAPagar() {
		assertEquals(0, trabajador.getImpuestoAPagar());
		trabajador.registrarIngresoPercibido(i1); 
		trabajador.registrarIngresoPercibido(i2);
		trabajador.registrarIngresoPercibido(i3);
		trabajador.registrarIngresoPercibido(iphe1);
		assertEquals(1350.535, trabajador.getImpuestoAPagar());
	}
}
