package tp4.ej2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductoPrimeraNecesidadTestCase {

	private ProductoPrimeraNecesidad leche;
	private ProductoPrimeraNecesidad azucar;
	private ProductoPrimeraNecesidad arroz;
	
	@BeforeEach
	public void setUp() {
		leche = new ProductoPrimeraNecesidad("Leche", 8d, false, 10);
		azucar = new ProductoPrimeraNecesidad("Azucar", 15d, true, 6);
		arroz = new ProductoPrimeraNecesidad("Arroz", 12d, false, 8);
	}
	
	@Test
	public void testCalcularPrecioLeche() {
		assertEquals(7.2d, leche.getPrecio());
	}
	
	@Test
	public void testCalcularPrecioAzucar() {
		assertEquals(14.1d, azucar.getPrecio());
	}
	
	@Test
	public void testCalcularPrecioArroz() {
		assertEquals(11.04d, arroz.getPrecio());
	}
}
