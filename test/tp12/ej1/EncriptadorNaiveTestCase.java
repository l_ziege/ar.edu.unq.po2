package tp12.ej1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class EncriptadorNaiveTestCase {
	
	private EncriptadorNaive encriptador;

	@Test
	public void testElEncriptadorRetornaMarcoileguAlEncriptarLasVocalesDeMurcielago() {
		encriptador = new EncriptadorNaive(new EstrategiaEncriptadoVocales());
		assertEquals("marcoilegu", encriptador.encriptar("murcielago"));
	}

	
	@Test
	public void testElEncriptadorRetornaMurcielagoAlDesencriptarLasVocalesDeMarcoilegu() {
		encriptador = new EncriptadorNaive(new EstrategiaEncriptadoVocales());
		assertEquals("murcielago", encriptador.desencriptar("marcoilegu"));
	}
	
	@Test
	public void testElEncriptadorRetorna495715AlEncriptarLaPalabraDiego() {
		encriptador = new EncriptadorNaive(new EstrategiaEncriptadoNúmeros());
		assertEquals("4,9,5,7,15", encriptador.encriptar("diego"));
	}
	
	@Test
	public void testElEncriptadorRetornaDiegoAlDesencriptarLaPalabra495715() {
		encriptador = new EncriptadorNaive(new EstrategiaEncriptadoNúmeros());
		assertEquals("diego", encriptador.desencriptar("4,9,5,7,15"));
	}
}
