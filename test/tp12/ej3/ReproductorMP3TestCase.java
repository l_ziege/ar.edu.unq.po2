package tp12.ej3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ReproductorMP3TestCase {
	
	private ReproductorMP3 mp3;
	private Song cancion1;
	private Song cancion2;

	@BeforeEach
	public void setUp() throws Exception {
		cancion1 = new Song();
		cancion2 = new Song();
		List<Song> canciones = new ArrayList<Song>();
		canciones.add(cancion1);
		canciones.add(cancion2);
		mp3 = new ReproductorMP3(new Pantalla(), canciones);
	}

	@Test
	public void testElEstadoInicialDelReproductorDeCancionesEsSelecciónDeCanciones() {
		assertEquals("Estado del reproductor mp3: Selección de canciones", mp3.getEstado().toString());
	}
	
	@Test
	public void testElReproductorSigueEnEstadoSelecciónDeCancionesAlPresionarPauseSinSeleccionarUnaCanción() {
		mp3.pause();
		
		assertEquals("Estado del reproductor mp3: Selección de canciones", mp3.getEstado().toString());
	}
	
	@Test
	public void testElReproductorSigueEnEstadoSelecciónDeCancionesAlPresionarStopSinSeleccionarUnaCanción() {
		mp3.stop();
		
		assertEquals("Estado del reproductor mp3: Selección de canciones", mp3.getEstado().toString());
	}
	
	@Test
	public void testElReproductorRecienIniciadoNoEstáReproduciendoNingunaCanción() {
		assertFalse(mp3.seEstáReproduciendoLaCanciónSeleccionada());
	}
	
	@Test
	public void testElReproductorCambiaAEstadoReproduciendoAlPresionarPlayEstandoEnSelecciónDeCanciones() {
		mp3.play();
		
		assertEquals("Estado del reproductor mp3: Reproduciendo", mp3.getEstado().toString());
	}
	
	@Test
	public void testElReproductorEstáReproduciendoUnaCanciónLuegoDEPresionarPlayEstandoEnSelecciónDeCanciones() {
		mp3.play();
		
		assertTrue(mp3.seEstáReproduciendoLaCanciónSeleccionada());
	}

	@Test
	public void testElReproductorSigueEnEstadoReproduciendoAlPresionarPlayReproduciendoUnaCanción() {
		mp3.play();
		mp3.play();
		
		assertEquals("Estado del reproductor mp3: Reproduciendo", mp3.getEstado().toString());
	}
	
	@Test
	public void testElReproductorPausaLaReproducciónDeUnaCanciónLuegoDePresionarPauseReproduciendola() {
		mp3.play();
		mp3.pause();
		
		assertFalse(mp3.seEstáReproduciendoLaCanciónSeleccionada());
	}
	
	@Test
	public void testElReproductorReanudaLaReproducciónDeUnaCanciónLuegoDePresionar2VecesPauseReproduciendola() {
		mp3.play();
		mp3.pause();
		mp3.pause();
		
		assertTrue(mp3.seEstáReproduciendoLaCanciónSeleccionada());
	}
	
	@Test
	public void testElReproductorCambiaAEstadoSelecciónDeCancionesLuegoDePresionarStopAlEstarReproduciendoUnaCanción() {
		mp3.play();
		mp3.stop();
		
		assertEquals("Estado del reproductor mp3: Selección de canciones", mp3.getEstado().toString());
	}
	
	@Test
	public void testLaCanciónSeleccionadaAlIniciarElReproductorEsLaCanción1() {
		assertEquals(cancion1, mp3.canciónSeleccionada());
	}
	
	@Test
	public void testLaCanciónSeleccionadaEnElReproductorLuegoDePasarASiguienteCanciónEnLaListaEsLaCanción2() {
		mp3.pasarASiguienteCanción();
		
		assertEquals(cancion2, mp3.canciónSeleccionada());
	}
	
	@Test
	public void testLaCanciónSeleccionadaEnElReproductorLuegoDePasarASiguienteCanciónEnLaLista2VecesEsLaCanción1() {
		mp3.pasarASiguienteCanción();
		mp3.pasarASiguienteCanción();
		
		assertEquals(cancion1, mp3.canciónSeleccionada());
	}
}
