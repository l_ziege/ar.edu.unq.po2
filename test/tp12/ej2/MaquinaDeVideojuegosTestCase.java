package tp12.ej2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MaquinaDeVideojuegosTestCase {
	
	private MaquinaDeVideojuegos maquina;

	@BeforeEach
	public void setUp() throws Exception {
		maquina = new MaquinaDeVideojuegos(new Control(), new Control(), new Ranura(), new Pantalla());
	}

	@Test
	public void testLaMaquinaRecienEncendidaEstáEnEstadoInicial() {
		assertEquals("Estado de la maquina: Inicial", maquina.getEstado().toString());
	}
	
	@Test
	public void testLaMaquinaSigueEnEstadoInicialCuandoSeQuiereIntroducirUnaFichaConLaRanuraCerrada() {
		maquina.introducirFicha();
		
		assertEquals("Estado de la maquina: Inicial", maquina.getEstado().toString());
	}
	
	@Test
	public void testLaMaquinaSigueEnEstadoInicialCuandoSeQuiereTerminarUnJuegoSinIniciar() {
		maquina.terminarElJuego();
		
		assertEquals("Estado de la maquina: Inicial", maquina.getEstado().toString());
	}
	
	@Test
	public void testSePresionaElBotónDeInicioEnLaMaquinaYCambiaAEstadoEsperandoFicha() {
		maquina.presionarBotónDeInicio();
		
		assertEquals("Estado de la maquina: Esperando ficha", maquina.getEstado().toString());
	}
	
	@Test
	public void tesLaMaquinaSigueEnEstadoEsperandoFichaLuegoDePresionarElBotónDeInicioEIntroducirUnaFicha() {
		maquina.presionarBotónDeInicio();
		maquina.introducirFicha();
		
		assertEquals("Estado de la maquina: Esperando ficha", maquina.getEstado().toString());
	}
	
	@Test
	public void testLaMaquinaSigueEnEstadoEsperandoFichaLuegoDePresionarElBotónDeInicioEIntentarTerminarUnJuegoSinComenzar() {
		maquina.presionarBotónDeInicio();
		maquina.terminarElJuego();
		
		assertEquals("Estado de la maquina: Esperando ficha", maquina.getEstado().toString());
	}
	
	@Test
	public void testLaMaquinaVuelveAlEstadoInicialLuegoDePresionar2VecesElBotónDeInicioSinIngresarFichas() {
		maquina.presionarBotónDeInicio();
		maquina.presionarBotónDeInicio();
		
		assertEquals("Estado de la maquina: Inicial", maquina.getEstado().toString());
	}

	@Test
	public void testLaMaquinaVuelveAlEstadoInicialLuegoDePresionar2VecesElBotónDeInicioHabiendoIngresado3Fichas() {
		maquina.presionarBotónDeInicio();
		maquina.introducirFicha();
		maquina.introducirFicha();
		maquina.introducirFicha();
		maquina.presionarBotónDeInicio();
		
		assertEquals("Estado de la maquina: Inicial", maquina.getEstado().toString());
	}
	
	@Test
	public void testLaMaquinaCambiaAEstadoJugandoCon1JugadorLuegoDePresionar2VecesElBotónDeInicioHabiendoIngresado1Ficha() {
		maquina.presionarBotónDeInicio();
		maquina.introducirFicha();
		maquina.presionarBotónDeInicio();
		
		assertEquals("Estado de la maquina: Jugando con 1 jugador/es", maquina.getEstado().toString());
	}
	
	@Test
	public void testLaMaquinaCambiaAEstadoJugandoCon2JugadoresLuegoDePresionar2VecesElBotónDeInicioHabiendoIngresado2Fichas() {
		maquina.presionarBotónDeInicio();
		maquina.introducirFicha();
		maquina.introducirFicha();
		maquina.presionarBotónDeInicio();
		
		assertEquals("Estado de la maquina: Jugando con 2 jugador/es", maquina.getEstado().toString());
	}
	
	@Test
	public void testLaMaquinaSigueEnEstadoJugandoCuandoSeQuiereIntroducirUnaFichaEstandoLaRanuraCerrada() {
		maquina.presionarBotónDeInicio();
		maquina.introducirFicha();
		maquina.presionarBotónDeInicio();
		maquina.introducirFicha();
		
		assertEquals("Estado de la maquina: Jugando con 1 jugador/es", maquina.getEstado().toString());
	}
	
	@Test
	public void testLaMaquinaVuelveAlEstadoInicialCuandoSePresionaElBotónDeInicioMientrasSeEstaJugando() {
		maquina.presionarBotónDeInicio();
		maquina.introducirFicha();
		maquina.presionarBotónDeInicio();
		maquina.presionarBotónDeInicio();
		
		assertEquals("Estado de la maquina: Inicial", maquina.getEstado().toString());
	}
	
	@Test
	public void testLaMaquinaVuelveAlEstadoInicialLuegoDeTerminarElJuegoEnEstadoJugando() {
		maquina.presionarBotónDeInicio();
		maquina.introducirFicha();
		maquina.presionarBotónDeInicio();
		maquina.terminarElJuego();
		
		assertEquals("Estado de la maquina: Inicial", maquina.getEstado().toString());
	}

}
