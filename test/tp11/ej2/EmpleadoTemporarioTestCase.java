package tp11.ej2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EmpleadoTemporarioTestCase {
	
	private Empresa empresa;

	@BeforeEach
	public void setUp() throws Exception {
		empresa = new Empresa();
	}

	@Test
	public void testUnEmpleadoTemporarioConHijosRecibeUnaPagaDeSueldoDe2001() {
		EmpleadoTemporario empleado = new EmpleadoTemporario(240, 3, true);
		empresa.cargarEmpleado(empleado);
		empresa.pagarSueldos();
		assertEquals(2001, empleado.getDinero());
	}
	
	@Test
	public void testUnEmpleadoTemporarioCasadoRecibeUnaPagaDeSueldoDe1740() {
		EmpleadoTemporario empleado = new EmpleadoTemporario(180, 0, true);
		empresa.cargarEmpleado(empleado);
		empresa.pagarSueldos();
		assertEquals(1740, empleado.getDinero());
	}
	
	@Test
	public void testUnEmpleadoTemporarioSolteroSinHijosRecibeUnaPagaDe2175() {
		EmpleadoTemporario empleado = new EmpleadoTemporario(300, 0, false);
		empresa.cargarEmpleado(empleado);
		empresa.pagarSueldos();
		assertEquals(2175, empleado.getDinero());
	}

}
