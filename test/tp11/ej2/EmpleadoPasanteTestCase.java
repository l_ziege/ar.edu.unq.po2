package tp11.ej2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EmpleadoPasanteTestCase {
	
	private Empresa empresa;

	@BeforeEach
	public void setUp() throws Exception {
		empresa = new Empresa();
	}

	@Test
	public void testUnEmpleadoPasanteQueTrabaja210HorasAlMesRecibeUnaPagaDeSueldoDe7308() {
		EmpleadoPasante empleado = new EmpleadoPasante(210, 4, false);
		empresa.cargarEmpleado(empleado);
		empresa.pagarSueldos();
		assertEquals(7308, empleado.getDinero());
	}

}
