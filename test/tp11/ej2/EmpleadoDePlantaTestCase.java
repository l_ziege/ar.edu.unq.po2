package tp11.ej2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EmpleadoDePlantaTestCase {
	
	private Empresa empresa;

	@BeforeEach
	public void setUp() throws Exception {
		empresa = new Empresa();
	}

	@Test
	public void testUnEmpleadoDePlantaCon2HijosRecibeUnaPagaDeSueldoDe2871() {
		EmpleadoDePlanta empleado = new EmpleadoDePlanta(6, 2, true);
		empresa.cargarEmpleado(empleado);
		empresa.pagarSueldos();
		assertEquals(2871, empleado.getDinero());
	}
	
	@Test
	public void testUnEmpleadoDePlantaSinHijosRecibeUnaPagaDeSueldoDe2610() {
		EmpleadoDePlanta empleado = new EmpleadoDePlanta(8, 0, false);
		empresa.cargarEmpleado(empleado);
		empresa.pagarSueldos();
		assertEquals(2610, empleado.getDinero());
	}

}
