package tp11.ej3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PropiedadEnComunTestCase {
	
	private PropiedadEnComun filtro;

	@BeforeEach
	public void setUp() throws Exception {
		filtro = new PropiedadEnComun();
	}

	@Test
	public void testElFiltroPropiedadEnComunRetorna1PaginaAlRecibirLaPaginaLucasYUnaListaConLasPaginasMauroYMike() {
		WikipediaPage solano = new PaginaDeWikipedia("Solano", Arrays.asList(), new HashMap<String, WikipediaPage>());
		Map<String, WikipediaPage> infoboxLucas = new HashMap<String, WikipediaPage>();
		infoboxLucas.put("birth_place", solano);
		WikipediaPage lucas = new PaginaDeWikipedia("Lucas", Arrays.asList(), infoboxLucas);
		
		WikipediaPage quilmes = new PaginaDeWikipedia("Quilmes", Arrays.asList(), new HashMap<String, WikipediaPage>());
		Map<String, WikipediaPage> infoboxMauro = new HashMap<String, WikipediaPage>();
		infoboxMauro.put("birth_place", quilmes);
		WikipediaPage mauro = new PaginaDeWikipedia("Mauro", Arrays.asList(), infoboxMauro);
		
		WikipediaPage unq = new PaginaDeWikipedia("Universidad Nacional de Quilmes", Arrays.asList(), new HashMap<String, WikipediaPage>());
		Map<String, WikipediaPage> infoboxMike = new HashMap<String, WikipediaPage>();
		infoboxMike.put("university", unq);
		WikipediaPage mike = new PaginaDeWikipedia("Mike", Arrays.asList(), infoboxMike);
		
		assertEquals(1, filtro.getSimilarPages(lucas, Arrays.asList(mauro, mike)).size());
	}

}
