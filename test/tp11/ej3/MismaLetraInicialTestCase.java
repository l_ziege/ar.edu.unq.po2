package tp11.ej3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MismaLetraInicialTestCase {
	
	private MismaLetraInicial filtro;

	@BeforeEach
	public void setUp() throws Exception {
		filtro = new MismaLetraInicial();
	}

	@Test
	public void testElFiltroDeMismaLetraInicialRetorna2PaginasAlRecibirLaPaginaLaPlataYUnaListaConLasPaginasLucasArtLoboYBeto() {
		WikipediaPage laPlata = new PaginaDeWikipedia("La Plata", Arrays.asList(), new HashMap<String, WikipediaPage>());
		WikipediaPage lucasArt = new PaginaDeWikipedia("Lucas Art", Arrays.asList(), new HashMap<String, WikipediaPage>());
		WikipediaPage lobo = new PaginaDeWikipedia("Lobo", Arrays.asList(), new HashMap<String, WikipediaPage>());
		WikipediaPage beto = new PaginaDeWikipedia("Beto", Arrays.asList(), new HashMap<String, WikipediaPage>());
		assertEquals(2, filtro.getSimilarPages(laPlata, Arrays.asList(lucasArt, lobo, beto)).size());
	}
	
	@Test
	public void testElFiltroDeMismaLetraInicialRetorna2PaginasAlRecibirLaPaginaBernalYUnaListaConLasPaginasBernalQuilmesYBuenosAires() {
		WikipediaPage bernal = new PaginaDeWikipedia("Bernal", Arrays.asList(), new HashMap<String, WikipediaPage>());
		WikipediaPage quilmes = new PaginaDeWikipedia("Quilmes", Arrays.asList(), new HashMap<String, WikipediaPage>());
		WikipediaPage buenosAires = new PaginaDeWikipedia("Buenos Aires", Arrays.asList(), new HashMap<String, WikipediaPage>());
		assertEquals(2, filtro.getSimilarPages(bernal, Arrays.asList(bernal, quilmes, buenosAires)).size());
	}

}
