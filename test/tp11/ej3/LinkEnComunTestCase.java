package tp11.ej3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LinkEnComunTestCase {
	
	private LinkEnComun filtro;
	private WikipediaPage laPlata;
	private WikipediaPage gimnasiaYEsgrima;
	private WikipediaPage buenosAires;
	private WikipediaPage beto;

	@BeforeEach
	public void setUp() throws Exception {
		filtro = new LinkEnComun();
		laPlata = new PaginaDeWikipedia("La Plata", Arrays.asList(), new HashMap<String, WikipediaPage>());
		gimnasiaYEsgrima = new PaginaDeWikipedia("Gimnasia y Esgrima La Plata", Arrays.asList(laPlata), new HashMap<String, WikipediaPage>());
		buenosAires = new PaginaDeWikipedia("Buenos Aires", Arrays.asList(laPlata), new HashMap<String, WikipediaPage>());
		beto = new PaginaDeWikipedia("Beto", Arrays.asList(buenosAires), new HashMap<String, WikipediaPage>());
	}

	@Test
	public void testElFiltroDeLinkEnComunRetorna1PaginaSimilarAlRecibirLaPaginaDeGimnasiaYEsgrimaYUnaListaConLasPaginasBuenosAiresYBeto() {
		assertEquals(1, filtro.getSimilarPages(gimnasiaYEsgrima, Arrays.asList(buenosAires, beto)).size());
	}

}
